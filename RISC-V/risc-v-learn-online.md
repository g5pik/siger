# [RISC-V Learn Online](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4562959_link)
https://riscv.org/community/learn/risc-v-learn-online/

RISC-V在线学习在初学者，中级和高级级别提供在线学习。旨在增加整个行业在RISC-V上的工程专业知识和职业机会，并直接使社区受益，准备开始您的学习之旅！

 **查看我们的新课程！** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/095308_e206da5f_5631341.png "屏幕截图.png")

## [RISC-V简介（LFD110x）](https://www.edx.org/course/introduction-to-risc-v)
https://www.edx.org/course/introduction-to-risc-v

了解有关RISC-V的所有信息：了解如何使用开放规范以及制定规范的组织，并了解如何才能成为这个充满活力的社区的一部分。

本课程分为五章：

- 第1章：了解RISC-V，它将RISC-V作为一种技术，组织和社区进行介绍，以使您更好地了解RISC-V是什么。
- 第2章：RISC-V的故事，其中详细介绍了RISC-V的历史，RISC-V International，RISC-V治理的组织，工作组和委员会，以及所有这些人如何有效地沟通。
- 第3章：RISC-V社区，概述了RISC-V社区管理，扩展和改进RISC-V中的所有工件的过程，包括ISA和其他规范，工作策略，开发实践等。
- 第4章：开发RISC-V，它使用所有先前的信息来描述有关协作开发规范，软件，合规性测试和其他相关工件的硬性细节。
- 第5章：实践中的RISC-V，该动手章节将通过使用QEMU仿真器/虚拟器，使用SystemVerilog创建处理器以及在实际硬件上运行操作系统来了解RISC-V ISA的实际操作。

我们希望到本课程结束时，您将在与RISC-V有关的所有活动中都有扎实的基础。您将学习如何阅读和理解规范，并了解策划和扩展规范所涉及的过程。您将了解如何与RISC-V International和RISC-V社区有效地合作。您将了解去哪里获取更多信息。

![](https://images.gitee.com/uploads/images/2021/0322/095636_13db5d15_5631341.png "屏幕截图.png")

## [构建RISC-V CPU内核](https://www.edx.org/course/building-a-risc-v-cpu-core)（LFD111x）
https://www.edx.org/course/building-a-risc-v-cpu-core

使用现代的开放源代码电路设计工具，方法论和微体系结构创建RISC-V CPU，所有这些都可以从您的浏览器中进行。

这个微型车间是数字逻辑设计和基本CPU微体系结构的速成课程。使用Makerchip在线集成开发环境（IDE），您将实现从逻辑门到简单但完整的RISC-V CPU内核的所有功能。使用免费提供的在线工具进行开源开发所能做的事情会让您感到惊讶。您将走出逻辑设计职业的基本技能，并通过学习使用新兴的交易级Verilog语言扩展（即使您还不了解Verilog）将自己置于最前沿。

![](https://images.gitee.com/uploads/images/2021/0322/095938_b23637d8_5631341.png "屏幕截图.png")

## [RISC-V在线辅导](https://www.vicilogic.com/vicilearn/run_step/?c_id=36)
https://www.vicilogic.com/vicilearn/run_step/?c_id=36

### [RISC-V（RV32I）处理器体系结构和应用](https://www.vicilogic.com/register/)
https://www.vicilogic.com/register/

- 指导性的自定进度课程，包括处理器控制，沙箱和知识检查
- 使用远程硬件（Xilinx PYNQ FPGA模块的实时信号塔在右侧）。
- 每个指令的RISC-V处理器硬件操作演示
  - 在线RISC-V组装程序仿真（Venus）
  - 远程RISC-V硬件程序上载和执行（带有硬件调试）
- 使用Xilinx Vivado EDA工具捕获和仿真RV32I硬件描述语言模型
- 使用RISC-V汇编编程开发游戏应用程序
- 流水线处理器操作，危害检测和处理
- RISC-V的C编程简介

### [数字系统设计和FPGA原型：基础知识，HDL和EDA工具](https://www.vicilogic.com/register/)
https://www.vicilogic.com/register/

viciLogic在线培训课程与远程FPGA硬件交互，实时控制和观察数字逻辑信号行为。您的浏览器可以按照课程步骤控制远程硬件输入信号，探查内部信号状态，并提供带有动态叠加信号控件的交互式课程图。

该策略提供了丰富的视觉效果，交互式的边做边学的经验。请查看[介绍性视频](https://www.vicilogic.com/static/ext/RISCV/REV2021/presentation/video/RISCVOnlineTutor_vicilogic_FearghalMorgan_REV2021_26thFeb.html)和[幻灯片](https://www.vicilogic.com/static/ext/RISCV/REV2021/presentation/RISC-V%20Online%20Tutor%20%20vicilogic%20%20Fearghal%20Morgan%20%20REV2021%20Feb26_2021.pdf)以获取更多信息。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/100706_fa4a786d_5631341.png "屏幕截图.png")

## [Maven Silicon：RISC-V在线课程](https://elearn.maven-silicon.com/risc-v)
Maven Silicon提供了Sivakumar P R提供的五种按需在线课程。

- [RISC-V指令集体系结构](https://elearn.maven-silicon.com/riscv-isa-instruction-set-architecture)：本RISC-V ISA课程通过各种示例详细介绍RISC-V指令集体系结构和所有RV 32 I指令。
- [RISC-V R32I RTL设计](https://elearn.maven-silicon.com/riscv-rtl-design)：本RISC-V RTL设计课程介绍了完整的RTL设计过程，介绍了如何初步为J型指令创建基本体系结构并分阶段按比例放大以实现所有其他RV 32 I指令。 。
- [RISC-V ISA和RV32I RTL体系结构设计](https://elearn.maven-silicon.com/risc-v-isa-rv32i-rtl-architecture-design)：此RISC-V培训课程使用Digital Electronics对RTL设计进行广泛的培训，其中包括组合，顺序，FSM逻辑设计和存储器的概念。
- [使用Verilog HDL的RISC-V RV32I RTL设计](https://elearn.maven-silicon.com/risc-v-rv32i-rtl-design-using-verilog-hdl)：此RISC-V动手培训课程介绍RISC-V ISA，流水线RISC-V处理器RTL设计架构以及如何使用Verilog HDL实施RTL设计。
- [使用UVM进行RISC-V RV32I RTL验证](https://elearn.maven-silicon.com/risc-v-rv32i-rtl-verification-using-uvm)：此RISC-V动手培训课程介绍RISC-V ISA，流水线RISC-V处理器RTL设计架构以及如何使用UVM验证RISC-V Verilog RTL设计。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/101020_af082ff1_5631341.png "屏幕截图.png")

## VSD-RISCV：指令集体系结构（ISA），通过Udemy
第1a部分：本课程将从头开始大量讨论RISC-V ISA，还包括有关为何我们甚至需要计算机体系结构以及如何在计算机上运行实时日常应用程序的部分，并提供示例。

本课程的最终目的是帮助所有人建立可靠的规范，这是系统设计背后的第一个标准。在即将到来的课程中，这些规范将使用verilog / vhdl以RTL硬件描述语言进行编码，最后RTL将使用开源EDA工具链进行放置和路由。

本课程将带您逐步了解规格，从有符号/无符号整数表示开始，到带有一些非常酷的图像和示例的RV64IMFD指令集。诸如“ IMFD”之类的约定也将以独特的方式进行探索，这是前所未有的，并且与微处理器或微控制器相关的任何课程

第1b部分：本课程是我上一门课程的继续，该课程涉及RV64I整数指令。我们还查看了以RISC-V汇编语言编码的示例程序，并查看了RISC-V架构中存在的所有32个寄存器的内容。

在第1a部分中查看的所有概念均构成本课程的基础，并且预期观众将覆盖至少70％的第1a部分课程。本课程涉及RISC-V体系结构的乘法扩展（RV64M）和浮点扩展（RV64FD）的一些高级主题-在当今快速变化的计算世界中，这是一个重要的主题。

我们还探讨了有关硬件的一些事实，这是下一门课程（即将推出）的基础，在下一门课程中，我们将使用verilog编写RISC-V ISA。

更多信息：

- [第1a部分（Udemy）](https://www.udemy.com/course/vsd-riscv-instruction-set-architecture-isa-part-1a/)
- [第1b部分（乌迪米）](https://www.udemy.com/course/vsd-riscv-instruction-set-architecture-isa-part-1b/)