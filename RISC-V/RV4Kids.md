# RV4Kids

这个词是一个缩写，它是 RISC-V microarchitecture for Kids 的缩写。源自 RISC-V 国际社区的一篇博客。尽管本期 SIGer 的共同编审 RVWeekly 的主编孔令军先生一再提醒，这类孩子的消息多是 EDA 软件的软文，但是我还是要向大家推荐它，因为，我从来不这样看待这些 “孩子” ，在博文的结尾，博主是这样说的 “Please let them grow up at their own pace” 请让他们按照自己的节奏成长，这课程并不是针对初高中设计的，这只能说明工具软件越来越智能化，并易于驾驭，这对整个工业的发展是极为有利的。

我更愿意把这个词作为一个 “科普态度” 就如 SIGer 创刊的时候，我引用的 “拥抱未来” 的意思一样。随着工业的发展，设计驱动，为个体赋能是科技届的趋势，没有什么可以被奉若神坛一样不可企及，在学习面前，只有求知若渴的学子，没有年龄区分的孩童。随着时代的发展，终生学习让我这样的 “老IT” 回到起点，和我的学生一同学习开源，这本身就是一份值得夸赞的事情。所以，

> 我极力地推荐 RV4Kids 这个崭新的教育品牌，这是我和 RVWeekly 主编孔令军先生共同推荐的一个科普项目，我们邀请 RISC-V 的全体同仁和社区都展开双臂拥抱青少年，您只需要在您的实践项目（开源项目）中 @[RV4Kids](https://gitee.com/RV4Kids) 字样，尽您所能，在您开展的 RISC-V 相关的活动，课程，工作坊，等等可以吮吸 RISC-V 知识的场合，为青少年提供便利，允许他们参与，或安静地来，悄悄地走，或积极提问，求知若渴。谁能知道，不远的将来，他们中的某个人会成为您的同事，甚至战友。我要借用小孔老师的话 “ **身负强国使命，与一众小伙伴各种厮杀** ” 呢？

我还要分享一个故事，那就是引领这一波人工智能热潮，深度学习，机器博弈的里程碑 “AlphaGo” 的缔造者 

> 哈萨比斯, 小时候是要当棋王的，他去找卡斯帕罗夫踢馆，被老卡摸头杀，结果就横空出世了 alphago。这是详细故事《 [Better chess For Better life](BetterChess4BetterLife.md) 》时代杂志的专访，介绍了这位少年天才的成长。PS: 深蓝出世之前是有一种装置叫 chess computer 的，暂且按下不表。
> - 哈萨比斯是一名国际象棋天才，
> - 后来成为游戏设计师，
> - 再后来成为明星AI公司DeepMind的创始人。
> - 他的使命？
> - 用AI来解决我们生活中的一切问题。

## 向青少年科普 RISC-V 的相关素材：

- [RISC-V Microarchitecture for Kids??!! | Steve Hoover, Redwood EDA](risc-v-microarchitecture-for-kids-steve-hoover-redwood-eda.md)

最新一篇博文介绍了这个面向工程师的 RISC-V 芯片设计课程的两位小学员，分别是13岁和12岁。  
工作坊的主持对13岁男孩的评价和他的爸爸介绍自己的儿子的成长历程，说明了 RV4Kids 是适合孩子的节奏的，  
或者说孩子有能力以自己的节奏学习 RISC-V.

>  **老实说** ，我当时假设尼尔得到了很大的帮助，或者他实际上是一个顽皮的幽默感的大学生，或者他复制了自己的解决方案。但是我跟着他，甚至在我观看的时候甚至让他当场编写一些TL-Verilog逻辑。他自由地承认自己并不完全了解PC重定向之类的一切，他不得不在很大程度上依赖于我们提供的参考电路，但对我来说，很明显，他确实对RISC有很多了解。 -V和电路设计，并可自行在makerchip.com上开发基本电路。

>  **His father** , Calvin, was kind enough to speak with me as well about his son’s experience. “We didn’t know if he could complete the course, but we decided it wouldn’t hurt to give it a try. He has a real interest in hardware and software.” Niel tells me he enjoys coding, video games, and playing and composing on the piano. And he enjoys getting friends involved in his technical projects… when they are willing. “What kind of projects?” I asked. Arduino, Micro Bit, breadboards, drones, Linux, and more, it turns out, and you can even watch Niel’s training videos that he posts to help others with these technologies! Last year he was awarded first place for his “Smart Irrigation System” in the Young Innovator Design Challenge organized by NXP India.

- [13-Year-Old, Nicholas Sharkey, Creates a RISC-V Core](13-year-old-nicholas-sharkey-creates-a-risc-v-core.md)

这篇博文第一次介绍了这个能设计RISC-V芯片内核的孩子，也是 RV4Kids 的起点。

- [RISC-V based MYTH](riscv-MYTH-Workshop.md)

这是博主的详细介绍关于他的 RISC-V 核心设计入门（30小时能够掌握）

- [RISC-V Learn Online](risc-v-learn-online.md)

所有课程是基于一个在线的 RISC-V 课程，这些都可以在 [RISC-V.org](https://riscv.org) 找到。  
这也是 SIGer 编委可以 PR 给 RVWeekly 的主要内容源。

