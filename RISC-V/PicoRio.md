# [David Patterson](https://www.yuque.com/riscv/rvnews/gngb11)

- 相关书籍
- 相关机构
- 相关新闻
  - [2020.07.20 PicoRio 首个可运行Linux的全开源RISC-V微型电脑系统](https://www.yuque.com/riscv/rvnews/wa5pnu)

# [2020.07.20 PicoRio 首个可运行Linux的全开源RISC-V微型电脑系统](https://www.yuque.com/riscv/rvnews/wa5pnu)

## 新闻简评
## 新闻正文

2020年07月21日16:01  来源：[深圳商报](http://szsb.sznews.com/PC/content/202007/21/content_891279.html)

日前，由2017年图灵奖得主大卫·帕特森教授领衔的“RISC-V（第五代精简指令集）国际开源实验室”发布了全球首个可运行Linux的全开源RISC-V微型电脑系统PicoRio项目，用于构建更透明、低功耗、定制能力强的高效能边缘计算平台。PicoRio的发布，标志着2019年11月在深成立的“RISC-V国际开源实验室”进入了实际产出阶段。

作为对标树莓派（RaspberryPi）的新一代微型电脑主板，PicoRio的特点是芯片级的全开源设计、低功耗、体积小，可通过USB连接鼠标和键盘，具备普通个人电脑的大部分功能，可运行Linux操作系统浏览网页，使用Java、Python等高级语言进行编程。它也可以直接使用电池供电，连接各式传感器，以利于物联网等深度嵌入式应用。依托PicoRio开源的设计及文档，广大电子产品开发者、教育科研工作者、电子爱好者可以快速构建各种应用，带动RISC-V生态进一步发展，推动产业应用迅速推广。

据悉，PicoRio最大的特点是从CPU设计，到PCB电路板设计，再到操作系统核心软件全部开源，核心架构使用最新的开源RISC-V指令集技术。与树莓派相比，PicoRio更开源：将构建于开源的RISC-V之上，从CPU设计、SoC设计、开发版设计，到软件系统全部开源；更低功耗：树莓派从最初的RP0时的0.8瓦待机功耗， 发展到RP4的3.4瓦待机功耗，目前活跃态下功耗高达7.6瓦，而PicoRio的目标待机功耗将控制在0.05瓦特以下，工作功耗控制在0.5瓦特以下，功耗优势明显，利于物联网应用；更安全：树莓派基于ARM的Trustzone安全机制，而PicoRio基于更透明、更开放的RISC-V安全架构。

随着通讯、物联网等产业的兴起，需求日趋多样化，对迭代速度的要求也更高，目前常见的商业CPU的生态环境及商业拓展模式无法满足产业需求。在此背景下，RISC-V开源技术创始人大卫·帕特森教授于2019年11月以深圳为中心成立了“RISC-V国际开源实验室”，围绕RISC-V处理器领域最关键环节——软硬件生态体系的充实完善，进行世界领先的RISC-V开源指令集CPU研究开发。该实验室由清华大学和伯克利加州大学两所世界一流高校联合运行，是一家非盈利性开源研究机构。自成立以来，一大批拥有开源理念的国际化公司与高校先后加入了共同研发工作。此次发布的PicoRio项目，是实验室与合作伙伴一起推出的首款基于RISC-V处理器SoC的系统设计平台。

此外，记者获悉，“RISC-V国际开源实验室”依托清华－伯克利深圳学院的数据科学交叉学科项目，开展硕士研究生招生培养，预计五年内计划招生100名。（首席记者 吴吉）

"[深圳一实验室全球首发新成果](http://sz.people.com.cn/GB/n2/2020/0721/c202846-34172848.html) (From sz.people.com.cn 2020.07.21)"

## 相关新闻

- "[RISC-V国际开源实验室将依托清华-伯克利深圳学院建设](https://zhuanlan.zhihu.com/p/81273415) (From zhuanlan.zhihu.com 2019-09-05)"
- "[RISC-V国际开源实验室将依托清华-伯克利深圳学院建设](https://www.sohu.com/a/320720007_115479)  (From www.sohu.com 2019-06-15 09:49)"
  - 当地时间6月12日， 图灵奖得主、计算机体系结构领域享誉世界的科学家大卫·帕特森（David Patterson）在瑞士宣布，将依托清华-伯克利深圳学院（TBSI），建设RISC-V国际开源实验室（RISC-V International Open Source Laboratory），又称大卫·帕特森RIOS图灵奖实验室（以下简称“RIOS实验室”）。
  - RIOS实验室将瞄准世界CPU产业战略发展新方向和粤港澳大湾区产业创新需求，聚焦于RISC-V开源指令集CPU研究领域开展研究，建设以深圳为根节点的RISC-V全球创新网络。研究将极大地推动全球RISC-V技术的工业化进程和软硬件生态建设。实验室由大卫·帕特森担任主任，依托清华-伯克利深圳学院开展工作。RIOS在西班牙语中意为河流，代表着汇聚资源，形成聚力。将实验室命名为RIOS，也传达了大卫·帕特森对开源的支持与对合作前景的美好祝福。未来依托该实验室将开展硕士、博士培养项目，并以此作为清华大学深圳国际研究生院核心学科建设的一部分，在教师和研究人员的选聘方面建立配套措施。
  - 清华-伯克利深圳学院于2014年由清华大学与伯克利加州大学在深圳市政府的支持下联合建立，凭借两校的综合多学科优势和雄厚的工科基础，吸引世界一流的生源与顶尖的教授和研究者，致力于培养产业科学家和解决中国面临的世界级问题。
- "[RISC-V国际开源实验室揭牌 将于2020年招收硕士研究生](https://news.sina.com.cn/c/2019-11-13/doc-iihnzahi0566721.shtml) (From news.sina.com.cn 2019年11月13日)"
- "[希望RISC-V架构5年内成为专有架构的强大对手](https://www.cnbeta.com/articles/tech/969499.htm) (From www.cnbeta.com 2020年04月20日)"
  - 上月，RISC-V基金会首席执行官Calista Redmond（卡利斯塔·雷德蒙德）向全体会员发送的通知邮件确认，RISC-V 基金会的法律实体已经过渡到瑞士。
  - RIOS Lab（RISC-V国际开放源实验室）又称David Patterson RIOS 图灵奖实验室，总部位于深圳，David Patterson担任实验室主任，谭章熹担任联合主任，依托清华-伯克利深圳研究院（TBSI）开展工作。
- "[第一届中国RISC-V论坛开幕](https://www.tbsi.edu.cn/index.php?s=/cms/index/detail/id/1332.html)  (From www.tbsi.edu.cn 2019.11.13)"
  - 由帕特森RISC-V国际开源实验室（RISC-V International Open Source Laboratory）、RISC-V基金会中国委员会、中国开放指令生态（RISC-V）联盟主办，清华-伯克利深圳研究院承办的“第一届中国RISC-V论坛”在深圳大学城举行。
  - 主题报告聚焦“安全”（Security）、“深度学习”（Deep Learning）、“支持与验证”（Support and Verification）、“架构”（Architecture）、“系统软件和编译器”（System Software and Compilers）等热点领域，国内外相关领域学术界和产业界的专家学者作报告，就推动建设开放开源的RISC-V生态体系进行分享交流。专题研讨环节，受邀企业代表与嘉宾就“RISC-V在中国的风险投资”（Venture Capital and Investment for RISC-V in China）及“RISC-V的应用与生态系统”（RISC-V Applications and Ecosystem）两个论题展开讨论。嘉宾与听众进行了热烈的互动交流，共同探讨如何建设开放开源的RISC-V生态及在国内应用等问题。
- "[RISC-V国际开源实验室发布全球首个全开源可运行Linux的RISC-V平台](https://www.china-riscv.com/read.php?tid=543&fid=38)

# [PicoRio](https://www.yuque.com/riscv/rvnews/kzm01e)

David Patterson 领衔的 RIOS Lab 发布了全球首个可运行Linux的全开源RISC-V微型电脑系统PicoRio项目，用于构建更透明、低功耗、定制能力强的高效能边缘计算平台。 c

## 相关新闻

1. [2020.07.20 PicoRio 首个可运行Linux的全开源RISC-V微型电脑系统](https://www.yuque.com/riscv/rvnews/wa5pnu)
2. "[RISC-V火力全开！全开源PicoRio对标销量超3000万件的树莓派](https://www.leiphone.com/news/202008/PqRngTWMkihMRQff.html) (From www.leiphone.com 2020.08.28)"
3. "[挑战树莓派：RISC-V 微型计算机 PicoRio 发布](https://www.163.com/dy/article/FLIV5Q0V0511CUMI.html) (From 163.com 2020.09.03 转自 oschina.net)"
4. https://marijuanapy.com/raspi-competitor-with-risc-v-rios-picorio-in-development/
5. "[PicoRio Linux RISC-V SBC is an Open Source Alternative to Raspberry Pi Board](https://www.cnx-software.com/2020/09/04/picorio-linux-risc-v-sbc-is-an-open-source-alternative-to-raspberry-pi-board/) (From www.cnx-software.com 2020.09.04)"
6. "[PicoRio dev board PC with a RISC-V chip will be a low-cost, open source Raspberry Pi alternative](https://liliputing.com/2020/09/picorio-dev-board-pc-with-a-risc-v-chip-will-be-a-low-cost-open-source-raspberry-pi-alternative.html) (From liliputing.com 2020.09.04)"
7. [RISC-V Global Forum 2020](https://www.yuque.com/riscv/rvnews/dvlmhb#tiZfw)
8. [RISC-V 版树莓派 PicoRio 路线公布：年底发布 Beta 版](https://www.oschina.net/news/120866/risc-v-version-of-raspberry-pi-picorio-route-announced) (From oschina.net 2020-11-18) [eng from riscv.org](https://riscv.org/blog/2020/11/picorio-the-raspberry-pi-like-small-board-computer-for-risc-v/)
9. [全开源RISC-V开发板PicoRio发布](https://www.bilibili.com/read/cv7396323/) (From B站 2020-8-29)
10. 