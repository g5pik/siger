# [RISC-V Microarchitecture for Kids](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4561551_link)??!!

https://riscv.org/blog/2020/12/risc-v-microarchitecture-for-kids-steve-hoover-redwood-eda/

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/005325_c75e46c7_5631341.png "屏幕截图.png")

RISC-V Microarchitecture for Kids??!! | Steve Hoover, Redwood EDA

上个月，我很高兴分享有关[Nicholas Sharkey](https://riscv.org/blog/2020/11/13-year-old-nicholas-sharkey-creates-a-risc-v-core/)的博客文章，他是一位令人惊讶的13岁少年，他与研究生和专业人士一起参加了我的在线研讨会，并开发了自己的流水RISC-V CPU内核-这对于他来说是一项了不起的壮举八年级生！ 

好吧，现在，一个月后，我们再次提供了研讨会，并且…

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/005428_ad62be3b_5631341.png "屏幕截图.png")

当我发表有关尼古拉斯的文章时，我有点担心人们可能对研讨会有错误的认识。实际上，在最近的这次研讨会中，我们还有第二个12岁的孩子。kes！

这两个12岁的年轻人不像Nicholas那样活跃于聊天中，我认为他们会悄悄地离开，这是最好的。但尼尔在导师正式结束后不久，与导师核对了几次并留在原地：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/005444_1992b15a_5631341.png "屏幕截图.png")

惊人的！！！

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/005527_088b2bf2_5631341.png "屏幕截图.png")

（另一名12岁的男孩在他妈妈的帮助下确实编写了一个计算器电路，并很乐意将其余的保存下来，我认为这本身就是成功的。）

老实说，我当时假设尼尔得到了很大的帮助，或者他实际上是一个顽皮的幽默感的大学生，或者他复制了自己的解决方案。但是我跟着他，甚至在我观看的时候甚至让他当场编写一些TL-Verilog逻辑。他自由地承认自己并不完全了解PC重定向之类的一切，他不得不在很大程度上依赖于我们提供的参考电路，但对我来说，很明显，他确实对RISC有很多了解。 -V和电路设计，并可自行在[makerchip.com](https://www.makerchip.com/)上开发基本电路。

His father, Calvin, was kind enough to speak with me as well about his son’s experience. “We didn’t know if he could complete the course, but we decided it wouldn’t hurt to give it a try. He has a real interest in hardware and software.” Niel tells me he enjoys coding, video games, and playing and composing on the piano. And he enjoys getting friends involved in his technical projects… when they are willing. “What kind of projects?” I asked. Arduino, Micro Bit, breadboards, drones, Linux, and more, it turns out, and you can even watch [Niel’s training videos](https://vimeo.com/user108379095) that he posts to help others with these technologies! Last year he was awarded first place for his “Smart Irrigation System” in the Young Innovator Design Challenge organized by NXP India.

尼尔在整个研讨会中都坐在父亲旁边，而父亲则是作为NXP的软件工程师来做自己的工作，而他的堂兄则在旁边陪伴他们，使他们分心。但是Niel仍然专注，我很高兴地向大家报告说，自从研讨会以来，他和他们在一起的比赛时间令人振奋。

我期待Niel和Nicholas进入工作市场的那一天。请帮助我用喜欢和分享以及额外的冰淇淋来庆祝他们的成就。

以下是一些相关链接：

- [关于尼古拉斯的帖子](https://riscv.org/blog/2020/11/13-year-old-nicholas-sharkey-creates-a-risc-v-core/) 
- [Niel的培训视频](https://vimeo.com/user108379095)
- [神话工作坊](https://www.vlsisystemdesign.com/vsd-iat/)
- [Makerchip IDE](https://www.makerchip.com/)
- [TL-Verilog](https://www.redwoodeda.com/tl-verilog)


最后，我必须再次强调，为了我自己，认为[神话车间](https://www.vlsisystemdesign.com/vsd-iat/)是不是专为中等高中生。请让他们按照自己的步调成长，然后跟随我在linkedin上继续学习一些将来的课程，因为该技术将继续使电路设计更易于访问（并且对工业越来越强大）。

注意安全，

史蒂夫·胡佛（Steve Hoover），红木EDA