「大纲」

1. 中国传统文化中的数学：[圆周率计算 I](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652717481&idx=2&sn=ae6a912724665c33b09ea07cc69c59bd) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551753_link)」
2. 中国传统文化中的数学：[圆周率计算 II](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652718135&idx=2&sn=d84e20a508d1ef864c2ee04e732453e4) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551754_link)」
3. [直觉可靠吗？从三门问题到贝叶斯定理](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725202&idx=3&sn=ef1d71ee025dc9537cda8ed66fa5bc5d) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551755_link)」

[和乐数学II：趣味数学之编程示范](https://gitee.com/yuandj/siger/issues/I4SL0T)

- [Python练习：计算圆周率](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551064_link) 
- [如何利用Python计算圆周率？代码实例介绍！](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551061_link)
- [三门问题的Python代码模拟](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551059_link)
- [如何记忆圆周率-故事法](https://gitee.com/link?target=https%3A%2F%2Fwww.jianshu.com%2Fp%2Fb27a685186bf)

「彩蛋」[宇宙耍帅第一公式-欧拉公式](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551065_link)

<p><img src="https://images.gitee.com/uploads/images/2022/0204/023947_0b789b9a_5631341.jpeg" width="706px"></p>

特邀编审：[李健辉](http://www.lijianhui.net/)（[知乎](https://www.zhihu.com/people/lijianhui.net)数学问题优秀回答者）

> 又肝出一期，用上 #990000 久久红 的底色，以 [9999 位PI值](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551503_link)做素材，落上几行标题，用的是盐色，有种做年夜饭的感脚。这就是 SIGer 创刊一年后，形成的话风啦。虽未到达功成之日，但是每一期完成封面，都是一种满足感，我想这就是分享的乐趣了，更是学习本身带来的获得感。本期请到了一位知乎答者共同担任编审，除了他的专栏《[直观の数学](https://www.zhihu.com/column/c_1041689010053341184)》非常符合本期目标，他的分享之心，更是我希望同学们能够体会的，也是 SIGer 的真正缘起。:pray:

不能免俗，这期专题肯定要再一次地预报下 SIGer 最大的成果 石榴派 将在寒假结束正式交付一版全栈学习的成果，拥有自定义指令的 RISC-V 实验系统开发板，载有这一年来探索出的最北向应用 “吉祥数独” 的高产系统。它不只汇聚了 SIGer 的分享，还有社区（科普阵地最前沿）孩子们分享的祝福寄语，真正的万众一心，齐心合力的作品。只缘于这个 PI 字，它发心于去年 3月15日，同日诞生的两个主题，除了 PI，还有一个星辰大海的主角，后面也会提到。之后，三个月，石榴派就诞生了，期间共出了 16期 SIGer 期刊，成就了 [RV少年](https://gitee.com/RV4Kids) 的子品牌，SIGer 也风格化了，本期的 BANNER 也是这时期产生的风格，学习小组，成为了 兴趣小组的主旋律。

> 就是这个 PI，迷人的 “圆周率”，拥有宇宙般绚丽色彩的数字，串联起了所有人所有事，本期本应是《和乐数学II》的专题，结果一通学习，生长出了这许多增长点，我只好把雪藏了一年的主题词 PI 提前拿出来分享了。要不是无处不在的PI，怎么会 和乐的 [张老师推荐的3个趣味数学主题](https://gitee.com/yuandj/siger/issues/I4SL0T)，竟然有了2个是 PI 相关的呢？以 PI 为起点，已经不能是所见相同的英雄们的专利了，甚至不能成为探索数学世界的起点，可以上升到探求宇宙奥秘的高度啦。这也是 SIGer 一周年，将《[宇宙II](../%E7%AC%AC8%E6%9C%9F%20%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7II%20-%20%E5%AE%87%E5%AE%99.md)》定格在现在的首页的目的，SIGer 已经上升为 [SIGer 星云](../Tools/GitSTAR.md#gitstar-%E6%98%9F%E4%BA%91)，诞生 未来之星的阵地啦。这就是 PI 的高度。

数学可以描绘全世界，更能大到整个宇宙，这期专题备稿的几天里，也是吮吸知识的日子，[^LaTeX]书写的能守恒公式，就是一个收获，与和乐数学转载的两篇圆周率（PPT截图）不同，三门问题到贝叶斯定理用的是 [^LaTeX] 数学公式，本期备稿的 Issue 就会因为渲染 [^LaTeX] 变慢。起初只有贴了大量的 linux 打印信息才会，现在又多了一种。原本想用 [^LaTeX] 重写一遍 圆周率 的两篇，一个是100%转载和乐的编辑风格，再一个，这会是同学们锻炼的最好机会，还是尽快把这期的砖抛出来吧。而学习的意外收获，是 CMD 编辑器的作者 10年前的文章另人颇有触动，一个是 [Python 如何提升生产力](https://gitee.com/shiliupi/CMD/issues/I4SL1G#note_8550719_link)，另一个是 [Ubuntu 使用心得](https://gitee.com/shiliupi/CMD/issues/I4SL1G#note_8550977_link)，结合上 MD 编辑器的使命，通过分享尽快和世界上的同好结盟，让我对 Linux 社区有了更深的理解。开源世界，就是分享的世界，秉承着分享之心的人们的精神世界。:pray: SIGer 就是在召唤这样的年轻人一同加入，共同构建宇宙的未来。

> 现在要隆重推荐第一个向 SIGer pr 的 @mixshare 老师，结缘于他分享的 圆的分形，现在 SIGer 的一位编委正在准备分形几何的新专题。理想老师是一个涉猎丰厚的 终身学习者，也是深入社区一线的教育实践家，他认为数学能够激发孩子们的好奇心，求知欲。而和 PI 日主题同时上线的 [星辰大海专题](../%E7%AC%AC9%E6%9C%9F%EF%BC%9A%E6%88%91%E4%BB%AC%E7%9A%84%E6%9C%AA%E6%9D%A5%E6%98%AF%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7%EF%BC%81.md)，也是他的杰作。

最后就是 PI 的种子早就埋在了我的心里，早前就带者小袁挑战过记忆圆周率的游戏，围绕 PI 的题材更是丰富多彩的，用 Python 语言编写示范程序，也早就有之，本期的主要目标是开启同学们通过计算机绘图编程学习数学的方式。现在有了《[直观の数学](https://www.zhihu.com/column/c_1041689010053341184)》的答主为同学们撑腰，可以放手干起来了。在探索数学奥秘的同时，我们一定会有更多的收获。

> （剧透一下，我就是在找派的时候，发现了《[古人是如何找π](https://zhuanlan.zhihu.com/p/48307582)？》然后被《["上帝公式"(欧拉公式)真的神？](https://zhuanlan.zhihu.com/p/48392958)》吸引到了小李老师的专题的。:pray:，他两年后复燃的主题是 manimCE ，有一票数学爱好者在用它绘制世界，他们可是用 Python 沙箱在创作。SIGer 的一位编审，我们亲切地称他谢博，在他的开源学习方法论中，将开源作为青少年学习的第7级，也就是最高级，它能够让同学们的学习一步到位，触达世界的最前沿。这期数学主题就是例证，快快一起来学习吧！详见文末「笔记」）

  - 《[直观的数学 — 最难科普的自然科学！](https://gitee.com/yuandj/siger/issues/I4SLV9)》就是 @[leekunhwee](https://gitee.com/leekunhwee) 小李老师发出的最好邀约！我们下期见 :pray:

# [中国传统文化中的数学：圆周率计算 I](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652717481&idx=2&sn=ae6a912724665c33b09ea07cc69c59bd)
黄建国 和乐数学 2019-10-09 21:14

圆周率的计算中国古人一直领先，其中有很多算法的思想现在都有借鉴作用。

<p><img title="2" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030611_7c1b49fa_5631341.png"> <img title="3" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030635_e6103239_5631341.png"> <img title="4" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030645_a04bdbb0_5631341.png"> <img title="5" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030654_32f1a34c_5631341.png"> <img title="6" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030703_8931ade5_5631341.png"> <img title="7" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030710_a9666f07_5631341.png"> <img title="8" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030718_5584df22_5631341.png"> <img title="9" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030726_a4c8c893_5631341.png"> <img title="10" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030734_deda6628_5631341.png"> <img title="11" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030743_65d20fc0_5631341.png"> <img title="12" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030752_e5999ac2_5631341.png"> <img title="13" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030800_766de96d_5631341.png"> <img title="14" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030807_c4c0ac3a_5631341.png"> <img title="15" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030815_847a0404_5631341.png"> <img title="16" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/030825_1ef3dbd5_5631341.png"></p>

### 相关阅读 

- [中国传统文化中的数学算法：勾股定理 I](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652717297&idx=2&sn=d69d339e38cef9712e4b3667c22e8f07)
- [中国传统文化中的数学算法：勾股定理 II](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652717366&idx=2&sn=e468a6484ab7546d2d2e0757adf92e21)

本文转自：CAM传习录

# [中国传统文化中的数学：圆周率计算 II](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652718135&idx=2&sn=d84e20a508d1ef864c2ee04e732453e4)
黄建国 和乐数学 2019-10-31 14:25

古人计算圆周率时肯定是用到了外推法，可以极大地提高精度。

<p><img title="1" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031149_eaa71a84_5631341.png"> <img title="2" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031209_19c5a991_5631341.png"> <img title="3" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031218_cc4ddd5e_5631341.png"> <img title="4" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031227_d54d2bb4_5631341.png"> <img title="5" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031235_38f7fd6e_5631341.png"> <img title="6" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031243_6164e9a7_5631341.png"> <img title="7" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031251_ee7b632a_5631341.png"> <img title="8" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031259_52a087e7_5631341.png"> <img title="9" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031306_d61447f4_5631341.png"> <img title="10" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031312_270260b5_5631341.png"> <img title="11" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031320_d843d7ad_5631341.png"> <img title="12" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031328_0786e335_5631341.png"> <img title="13" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031335_5a8a4d82_5631341.png"> <img title="14" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031343_f84e3d2c_5631341.png"> <img title="15" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031350_c683a9ce_5631341.png"> <img title="16" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031402_9e4b283c_5631341.png"> <img title="17" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031411_aa228248_5631341.png"> <img title="18" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031419_a3726c2f_5631341.png"> <img title="19" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031427_48ed98c3_5631341.png"> <img title="20" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031436_c363fffb_5631341.png"> <img title="21" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031444_d1d4c1fa_5631341.png"> <img title="22" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031451_b4f128e8_5631341.png"> <img title="23" height="136px" src="https://images.gitee.com/uploads/images/2022/0204/031458_b707362a_5631341.png"></p>

本文转自：CAM传习录

# [直觉可靠吗？从三门问题到贝叶斯定理](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725202&idx=3&sn=ef1d71ee025dc9537cda8ed66fa5bc5d)

和乐数学 2021-02-20 18:04

编者荐语：

> 三门问题是概率论中的经典问题，贝叶斯定理则在现代统计、机器学习中发挥了越来越重要的作用。值得了解。

以下文章来源于返朴 ，作者张和持

> 导语：三门问题，也被称为蒙提霍尔问题，是一道著名的概率问题：一个游戏节目中共三扇门，一扇门后有汽车，另两门后只有山羊，你选择了一扇门但不打开，这时主持人会在另两门中打开一个后面是山羊的门，现在你换不换自己刚才选择的门？30年前这一问题被美国一知名杂志刊登后引发了热议，因为直觉告诉我们换不换都是一样的，但答题人选择换。数学爱好者、专业人士纷纷加入讨论，进行了一场旷日持久的论战，还发展出了诸多变种。现在让我们来回顾一下这道经典问题，来看看直觉到底哪出错了，信息又是如何影响结果的。

撰文 | 张和持

### 问题背景

在美国的一档节目Let's Make a Deal 中，主持人蒙提·霍尔设置了一项小游戏：

> 在你的面前有三扇门，其中一扇背后藏有一辆价值不菲的汽车；剩下的两扇背后则分别是两头山羊。你现在有机会选择一扇门，选好之后先不要打开，这时主持人会在另外两扇门中，开启一扇山羊门。现在你有两个选择，是应该维持原来的选择，还是转而选择另一扇没有开启的门？

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/031941_748bc533_5631341.png "屏幕截图.png")

三门问题示意图/图片来源：维基百科

这看起来似乎是一个没有深度的问题。可供选择的两扇门之间没有什么不同。不管选哪个，概率都应当是相同的才对。果真如此吗？

实际上这个问题有很古老的历史，可以追溯到法国数学家伯特兰（Joseph Bertand）的盒子悖论，后来著名的数学科普大师马丁·加德纳（Martin Gardner）也提出过与三门问题相似的囚徒问题。1975年美国统计学家塞尔文（Steve Selvin）根据电视节目改编提出了这一问题，投给了《美国统计学家》，这也是蒙提霍尔问题名称的由来。而真正引发讨论是到了1990年，一位读者向当时美国知名杂志《游行》（Parade）的专栏“交给玛丽莲”提出了关于这一问题的询问。

玛丽莲·莎凡特（Marilyn vos Savant）曾被吉尼斯世界纪录认定为世界上智商最高的人，后来成为这家杂志社的专栏作者专门回答各类问题。她给出的回答是：应该换门，而且换门后，开出汽车的概率将变为原来的两倍。

玛丽莲的吉尼斯记录颇受争议，她给出的这个答案也同样。人们纷纷向她写信，质疑她的结论。一时间，社会各界都在谈论这诡异的概率。来信表示反对的占了92%，其中有将近 1000 人拿过博士学位；65%来自大学，特别是数学等院系的信，都反对她的答案。蒙提·霍尔问题，或称三门问题，一下子成为了关注的焦点。90年代的十年间，40多种学术刊物发表了关于这一问题超过75篇论文。

反对并不是毫无根据。关于问题和答案的表述不甚严谨，表面上看，我们也看不出换不换门究竟有什么决定性的区别。玛丽莲为了说服反对者，专程组织了几次实验，其结果都证实了她的结论。

笔者听说这个问题，是在多年以前看的另一档美国电视节目 MythBusters ，中文译为 流言终结者 。两位主持人演示过的诸多实验令人印象深刻。这一次他们也同样忠实地再现了三扇门和山羊。最终他们的实验结果证实了玛丽莲的答案：不换门，概率 1/3 ；换门，概率 2/3 。

<p><img width="369px"  src="https://images.gitee.com/uploads/images/2022/0204/032103_30a96bf4_5631341.png"></p>

流言终结者节目

其实我们不一定非得大张旗鼓地搞些花里胡哨的东西，用电脑也能模拟，得出的答案没有不同。

<p><img width="369px"  src="https://images.gitee.com/uploads/images/2022/0204/032121_6d1f98fb_5631341.png"></p>

计算机模拟29次的结果/图片来源：维基百科

那么这样看来，玛丽莲的答案是对的了。现在的问题是，我们应该如何解释这样的结果？是我们的直觉究竟出了问题吗？接下来我们并不打算解释谁的观点为什么对，谁的观点又为什么错；我们细细来看问题的前因后果，把所有条件和结论整理清楚。

### 简单直观的图示解答

最不动脑的方法是把所有可能列出来，如下图所示，当玩家选择1号门的情况下所有的可能性。

<p><img width="369px"  src="https://images.gitee.com/uploads/images/2022/0204/032203_aa2953b1_5631341.png"></p>

玩家最初选择1号门时的所有可能/图片来源：维基百科

但这并不代表我们真正理解了问题所在。为了解答疑惑，我们先一步一步理清思路。首先，三门问题与两扇门二选一究竟有何不同？或者说，我们刚刚开始选定的这扇门究竟产生了什么样的影响？

第一次的选择，的确是随机的。如果这时候把门打开，那么有车的概率就是 1/3 。而剩下的两扇门中，必然有一扇山羊门。所以打开的山羊门不会影响这个 1/3 。那么这样说来，用总的 1 去减 1/3 ，就应该是剩下那扇门开出汽车的概率， 2/3 。这样的确说得过去：一开始概率分布是均匀的：


![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032252_57f5788a_5631341.png "屏幕截图.png")

图片来源：维基百科

打开山羊门之后， 2/3 的概率被“挤”到另一个门上了：


![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032307_9b67eec4_5631341.png "屏幕截图.png")

图片来源：维基百科

回到一开始的疑问：我们一开始的选择对结果产生了什么影响？

### 用条件概率来直接计算

三门问题中所提到的“我们刚开始选的门对剩下那个门的影响”，数学上我们称之为条件概率，用

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032541_4367d36d_5631341.png "2022-02-04-032509_92x35_scrot.png")

来表示事件  _B_  发生的情况下，事件  _A_  发生的概率。那么自然（其实这是条件概率定义），条件概率就等于  发生的概率除以  _A_  发生的概率：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032551_a6d4ea11_5631341.png "2022-02-04-032521_180x55_scrot.png")

其中分子表示  _A,B_  两个事件都发生的概率。在这个问题中，关键在于我们要求的是剩下的门开出汽车的概率。那  _A_  事件就是剩下那门开出汽车。 _B_  是什么呢？我们最初做的选择，有两个结果：一开始就选到车，我们记为  _B1_  ；一开始选到山羊，记为  _B2_  。这样，剩下的门开出汽车的概率，就应该考虑到两种情况的条件概率，分别乘上两个先发事件的概率（相当于权重），再加起来

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032808_2fe7eb75_5631341.png "2022-02-04-032737_325x26_scrot.png")

这个式子称为 **全概率公式** ，有了它我们可以具体计算。如果一开始就选到了车，那剩下的两个门里肯定没有车。所以  _P(A|B1) = 0_  ，那么贡献概率的就只有第二项了。如果一开始选到山羊，那剩下的门就一定是车，所以  _P(A|B2) = 1_  ，这样算出来

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/032933_ebd5526d_5631341.png "2022-02-04-032907_247x45_scrot.png")

也是非常自然的结果。第一次选出山羊的 2/3 概率全部贡献到剩下的门开出汽车的概率中了，所以换门概率更高也不足为奇。主持人开山羊门并不能改变一开始选到车的概率，但却改变了剩下的门开出车的概率：如果不开门，条件概率  _P(A|B2)_  本应该是 1/2 （毕竟剩下的两个门概率均等），而开门后，山羊门不可能有车，所以条件概率变成了 1 。或者用贝叶斯理论的话说：主持人开的那扇山羊门，为我们提供了关于剩下那个门的信息。


### 用贝叶斯公式来追本溯源

虽然问题就解决了，但还是不知道我们最开始错误的直觉来自哪里。我们来思考一下主持人“提供信息”的问题。一切的改变，都是因为主持人提供给我们的信息：主持人很明显是知道门背后都是些什么，才打开山羊门的。那要是主持人根本就不知道汽车在哪里，只是随手选择了一扇门，而这扇门恰好是山羊门的话，主持人岂不是就不能提供任何信息了？

为了验证这个想法，我们就假定主持人的开门行为完全是随机的。记他开出山羊门这个事件为  _C_  ，那么还是使用全概率公式

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/033208_7512abc7_5631341.png "2022-02-04-033149_320x107_scrot.png")

我们想要知道的，就是当事件  _C_  发生时，事件  _B1_  ，也就是最初的门开出汽车的概率。因为主持人自己都不知道汽车在哪，所以如果要做实验的话，样本里肯定存在主持人开出汽车的情况。而我们现在要算的条件概率  _P(B1|C)_  ，描述的就应该是在排出了这些样本后的实验结果。接下来我们就来计算

![输入图片说明](https://images.gitee.com/uploads/images/2022/0204/034109_fbf515c3_5631341.png "")

看！果不其然，概率是 1/2 。第二个等号其实也算是全概率公式。这个结果符合我们的直觉：如果主持人跟我们一样什么也不知道，那不管换不换门概率都是一样的。至此，我们已经完全搞清楚了这个问题。我们看到，一个看似毫无意义的行为，竟然能为决策者提供如此多的信息。那么在更为复杂的博弈中，找到对手留下的蛛丝马迹便尤为重要。

下面详细说一下这个算式。

这整个式子被称作贝叶斯公式，其中最神奇的地方，莫过于为了计算  _P(B1|C)_  ，竟然用到了  _P(C|B1)_  。贝叶斯的思想最初不被人接受。贝叶斯公式相当于是在知道了结果的情况下求原因。同时代的数学家们批判它为“玄学”，可在三门问题中，确实派上了用场。

为了理解这种思维，让我们假想一个工厂，有三台机器生产同一种零件，事件  _M,N,O_  分别表示一个零件是由某一台机器生产的，事件  _Q_  表示生产出次品。在生产中，每台机器产生次品的概率  _P(Q|M),P(Q|N),P(Q|O)_  通常是已知的，总的次品率  _P(Q)_  也是已知的。那么对于任意一个次品，它更有可能是哪台机器生产出来的呢？这种情况下，我们就需要计算 P(M|Q),P(N|Q),P(O|Q) ，而这用到的也是贝叶斯公式，读者朋友们感兴趣的话自己试试推导，跟我们上面的计算步骤差别不大。

这种由果溯因的思路一般称为贝叶斯推断。这种方法最早的特例是托马斯·贝叶斯证明的，后来拉普拉斯将其推广，并应用于天体力学、医疗统计学等方面面。在面对未知的自然界时，我们无法知晓其背后的法则，但能观察到现象，比如天体力学中，就是可观测天体的大小、数量与过去的轨迹；医疗统计学中，则是患者的症状、化验结果、CT数据等，有了这些数据，我们就可以猜测，是否存在一个我们尚未观测到的天体，或是患者是否得了某种病。猜测自然有可能是错的，现在我们有了贝叶斯推断，就能反过来计算我们所作假设成立的概率。

这一方法近几十年来更多应用于机器学习领域，或者更窄的概念：统计学习。任何包含了模式识别的算法，基本上都能用上贝叶斯推断，包括人脸识别，自动驾驶，他们会用到更加复杂的概念：当事先不存在假设时，就需要人为设定一项先验分布。不过总的来说，思想与最经典的贝叶斯公式一脉相承。如今，以此为基础的统计学仍在飞速发展。

「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T)」

# [和乐数学II：趣味数学之编程示范](https://gitee.com/yuandj/siger/issues/I4SL0T)

- 中国传统文化中的数学：[圆周率计算 I](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652717481&idx=2&sn=ae6a912724665c33b09ea07cc69c59bd)

  - [Python练习：计算圆周率](https://zhuanlan.zhihu.com/p/144944064) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551064_link)」
    
    > 圆周率的计算有两种方法，分别是数学公式法、工程上的蒙特卡罗法。

- 中国传统文化中的数学：[圆周率计算 II](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652718135&idx=2&sn=d84e20a508d1ef864c2ee04e732453e4)

  - [如何利用Python计算圆周率？代码实例介绍！](https://www.w3cschool.cn/article/33841075.html) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551061_link)」

    - [上帝公式：欧拉公式的宇宙学意义](https://zhuanlan.zhihu.com/p/90328091) - 知乎专栏 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551065_link)」
      > 这个公式关键的作用，是将正弦波统一成了简单的指数形式。 我们来看看图像上的涵义： 欧拉公式所描绘的，是一个随着时间变化，在复平面上做圆周运动的点，随着时间的改变，在时间轴上就成了一条螺旋线。 如果只看它的实数部分，也就是螺旋线在左侧的投影，就是一个最基础的余弦函数。 而右侧的投影则是一个正弦函数。 关于复数更深的理解，大家可以参考： 复数的物理意 …

      > 经常有理工科的学生为了跟妹子表现自己的学术功底，用这个公式来给妹子解释数学之美：”石榴姐你看，这个公式里既有自然底数e，自然数1和0，虚数i还有圆周率pi，它是这么简洁，这么美丽啊！“但是姑娘们心里往往只有一句话：”臭屁……“

    - 【林生·博物】[“上帝公式”欧拉公式](https://www.sohu.com/a/347273002_120066761) - sohu.com 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551070_link)」
      > Oct 15, 2019 · “上帝公式”——欧拉公式 “上帝创造的公式， 我们只能看它却不能完全理解它”。 导语 它是数学里最令人着迷的一个公式，它将数学里最重要的几个数学联系到了一起：两个超越数，欧拉数 e 与圆周率 π；两个单位，虚数单位 i 和自然数单位 1，以及数学里常见的 0。 我们虽然不敢肯定她是否为世界上“最伟大公式”，但是可以肯定的是，她是最美的公式之一。 展开全文 声 …

    - [“数学之王”欧拉：创造万能的上帝公式，还被怀疑是穿越者](https://new.qq.com/omn/20210521/20210521A0833W00.html)

      May 21, 2021 · 四、欧拉公式 除了上述我们提到的欧拉的事迹，他还在1748年发明了一个万能公式， 欧拉公式，也被成为上帝公式 。 为何这个公式被成为上帝公式呢？ 这个公式被成为最奇妙的

      ![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/032211_0fd982ba_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/032219_fe690cf2_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/032226_fe1c6a6e_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/032232_91758a4e_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/032239_336498ed_5631341.png "屏幕截图.png")

    - ["上帝公式"(欧拉公式)真的神到](https://zhuanlan.zhihu.com/p/48392958) ... - 知乎专栏 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551073_link)」

      - [古人是如何寻找到π的？](https://zhuanlan.zhihu.com/p/48307582)
        > 不妨先看看古人是如何确定π是一个常数，并通过迭代法最终求得该常数的近似值的。 π的历史简介 众所周知， 可以说，它是世界上最有名的无理常数了，代表的是一个圆的周长与直径之比或称为“圆周率”。公元前250年左…阅读全文​

        - [Python为什么取名为Python，很少人知道_Python学习笔记](https://blog.csdn.net/Pythonlaowan/article/details/100736796)

      - [3Blue1Brown 动画制作教程(10)](https://zhuanlan.zhihu.com/p/115474092)--三维空间及三维坐标系

        <p><img src="https://images.gitee.com/uploads/images/2022/0204/014050_89d8c2d6_5631341.png" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0203/034655_ebee0950_5631341.gif" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0204/014317_ba33684c_5631341.gif" height="69px"> <img src="https://images.gitee.com/uploads/images/2022/0204/014342_ec6dfd05_5631341.gif" height="69px"></p>

      - [新版动画引擎 manimCE 的快速安装与运行演示](https://zhuanlan.zhihu.com/p/447082417) （内有视频）

        时隔两年，我又来更新 manim 啦~

        我在 2019 年 6 月曾发布一篇名为 3Blue1Brown的动画引擎如何配置？ 的文章，在此之后的两年中，我收到了很多知友的评论与私信。有的问题我遇到过，我就会一一解答；有的问题没遇到过，实感抱歉无力，主要还是因为原始版本的 manim 实在是太难安装了，而且牵扯的软件太多，极易造成版本的不兼容，进而引发各式各样的问题。

        manim 的开发者们一直致力于精确动画引擎的升级与优化，在这两年中，manim 的 Github 项目发生了很大的变化，并衍生出了三个主要版本，分别是 manimCairo、manimGL、manimCE。

        - manimCairo：manim 的早期版本，也是本人以前所发教程对应的版本，该版本自 2020 年 11 月起已经停止了更新维护。
        - manimGL：该版本由 3B1B 作者 Grant Sanderson 主要负责维护。该版本由早期版本更新迭代而来，之所以这样命名，是因为该版本支持 OpenGL 的渲染。按照墨西哥知名 manim 教学 up 主 Theorem of Beethoven 的说法，该版本已近趋于稳定，但还尚未完成[1]。
        - manimCE：为本篇主要介绍的版本，该版本是由 2020 年中旬的一个 manim 分支演化而来，该分支后来社区化，被称为是 manim Community Edition ，缩写为 manimCE。现在该版本被一群开发者共同更新维护，原作者 Grant Sanderson 也在其中。这是 manim 当前的一个最为稳定的版本，相对于早期版本，其在语法结构上做了大量优化，并简化了安装步骤。由于参与者更多，所以其更新维护最为频繁，一些常见的 BUG 能在较短的时间内被解决，所以更加适合新手上手（注意：以往的教程并不适用于该版本）。

      - [如何优雅地使用 manimCE 进行动画制作？](https://zhuanlan.zhihu.com/p/450822295)

        - https://github.com/leekunhwee/manimce
        - https://gitee.com/shiliupi/manimce

      - [公众号：科研狗](https://images.gitee.com/uploads/images/2022/0203/104533_a62527d9_5631341.png) 

        - [我的博士毕业致辞](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651484526&idx=1&sn=845619180d8ec84d1399a73c323b6536)
        - [《Lemon》这首歌到底在讲什么？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483873&idx=1&sn=9402f12a7c6132c667d2ea446a181a11)
        - [五轴数控机床为什么是五轴联动而非六轴联动呢？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483289&idx=1&sn=668716c655237e365fb7c3c613ba1170)
        - [硕博学位论文 LaTeX 模板及其使用教程](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651484265&idx=1&sn=27907fa2675c36ef8f698d97d44a7b4c)
        - [什么是“奥利给不等式”？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483427&idx=1&sn=2ef92fbe951d7d5d219dc931330d368c)
        - [嘿，文青！你真的“文艺”吗？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483913&idx=1&sn=3a6d809efbf589d9156b8955f1594c22)

      - https://github.com/leekunhwee/

        - Jianhui Li （leekunhwee）
        - 【公众号】: 科研狗 【知乎ID】: 李狗嗨
        - 24 followers · 17 following
        - Xi'an Jiaotong University
        - Xi'an, Shaanxi, China 
        - leekunhwee@gmail.com 

      - http://www.lijianhui.net <img src="https://images.gitee.com/uploads/images/2022/0203/110255_181d181f_5631341.png" height="19px">

        Welcome! I’m Alex Lee. My Chinese name is Jianhui Li. Welcome to my blog!

- [直觉可靠吗？从三门问题到贝叶斯定理](http://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725202&idx=3&sn=ef1d71ee025dc9537cda8ed66fa5bc5d)

  - [直觉可靠吗？从三门问题到贝叶斯定理 MarkDown 样本](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8550370_link)

  - [Markdown 数学公式](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8550373_link) 

    - [Markdown 公式指导手册.html](https://gitee.com/ityangzhiwen/markdown_mathematical_formula/blob/master/Markdown%20%E5%85%AC%E5%BC%8F%E6%8C%87%E5%AF%BC%E6%89%8B%E5%86%8C.html)
    - [Markdown 公式指导手册.md](https://gitee.com/ityangzhiwen/markdown_mathematical_formula/blob/master/Markdown%20%E5%85%AC%E5%BC%8F%E6%8C%87%E5%AF%BC%E6%89%8B%E5%86%8C.md) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8550374_link)」「[转载](https://gitee.com/shiliupi/CMD/blob/master/Markdown%20%E5%85%AC%E5%BC%8F%E6%8C%87%E5%AF%BC%E6%89%8B%E5%86%8C.md)」
    - [web端数学公式.html](https://gitee.com/ityangzhiwen/markdown_mathematical_formula/blob/master/web%E7%AB%AF%E6%95%B0%E5%AD%A6%E5%85%AC%E5%BC%8F.html)

  - [Markdown 示范 DOCs](https://gitee.com/shiliupi/CMD/tree/master/DOC)

    - Printable March 2022 Calendar.md

      <p><img src="https://www.typecalendar.com/wp-content/uploads/2020/03/March-2022-Calendar.jpg" height="99px"><img src="https://www.typecalendar.com/wp-content/uploads/2020/03/Calendar-March-2022.jpg" height="99px"><img src="https://www.typecalendar.com/wp-content/uploads/2020/03/March-2022-Printable-Calendar.jpg" height="99px"><img src="https://www.typecalendar.com/wp-content/uploads/2020/03/Blank-March-Calendar-2022.jpg" height="99px"></p>

    - 行列式(二)：余子式&代数余子式.md

  - [Cmd Md 编辑阅读器](https://gitee.com/shiliupi/CMD/tree/master/CMD)

    - Cmd Markdown 发布第十四次更新.md
    - Cmd Markdown 简明语法手册.md
    - 升级会员类型.md
    - 欢迎使用 Cmd Markdown 编辑阅读器.md

    书写一个质能守恒公式[^LaTeX]: $$E=mc^2$$

    - [张佳伟的博客](https://gitee.com/shiliupi/CMD/issues/I4SL1G#note_8550568_link)

      - About Me
        - 码农典范
        - 键盘狂热爱好者
        - 神经衰弱，强迫症患者
        - 和老婆，女儿，还有猫一起住在上海
        - 作业部落：[创始人](http://www.zybuluo.com/)
        - 新浪微博：[ghosert](http://www.weibo.com/ghosert)
        - 豆瓣社区：[ghosert](http://book.douban.com/people/ghosert/)

      - 2013
        - [Python 如何使基于 Java 的 StubHub 受益](http://ghosertblog.github.io/blog/2013/07/21/stubhub-blog-chinese/) JUL 21 「[笔记](https://gitee.com/shiliupi/CMD/issues/I4SL1G#note_8550719_link)」
posted in English, Python, StubHub, Work
        - [How does Python benefit the Java based StubHub](http://ghosertblog.github.io/blog/2013/07/17/stubhub-blog/) JUL 17
posted in English, Python, StubHub, Work
        - [Ubuntu桌面生存指南 (5) — Ubuntu常用效率软件简介](http://ghosertblog.github.io/blog/2013/01/07/ubuntu-efficient-software/) JAN 07
posted in Linux, Tools, Ubuntu

      - 2012

        - [Ubuntu桌面生存指南 (4) — Ubuntu系统备份恢复升级策略](http://ghosertblog.github.io/blog/2012/12/04/ubuntu-living-handbook-backup-restore/) DEC 04
posted in Linux, Tools, Ubuntu
        - [Ubuntu桌面生存指南 (3) — 构建Ubuntu系统基础设施](http://ghosertblog.github.io/blog/2012/10/30/ubuntu-living-handbook-install/) OCT 30
posted in Linux, Tools, Ubuntu
        - [Ubuntu桌面生存指南 (2) — Ubuntu桌面体验简介](http://ghosertblog.github.io/blog/2012/10/22/ubuntu-living-handbook-experience/) OCT 22
posted in Linux, Tools, Ubuntu
        - [Ubuntu桌面生存指南 (1) — 选择 Linux](http://ghosertblog.github.io/blog/2012/10/14/ubuntu-living-handbook/) OCT 14 「[笔记](https://gitee.com/shiliupi/CMD/issues/I4SL1G#note_8550977_link)」
posted in Linux, Tools, Ubuntu
        - [极客的博客](http://ghosertblog.github.io/blog/2012/04/20/geeks-blog/) APR 20 「[转载](https://gitee.com/shiliupi/CMD/blob/master/CMD/%E6%9E%81%E5%AE%A2%E7%9A%84%E5%8D%9A%E5%AE%A2.md)」
posted in Geek, Octopress      

  - [三门问题的Python代码模拟](https://blog.csdn.net/u011054333/article/details/84449503) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551059_link)」
  
- [PI DAY & 分形几何 - 宝藏老师 @理想觅学](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551456_link) 

  <p><img src="https://images.gitee.com/uploads/images/2022/0203/172134_25b18560_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/173725_3e8fba6c_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/173739_50bd5d0c_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/173800_98f87614_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/173838_ebd9baa6_5631341.jpeg" height="99px"></p>

  - 数学 | [40081787109376的平方](https://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168327&idx=1&sn=ce014953896055d3934ec56474053534) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551476_link)」

    之前介绍了几期有趣的数字，回顾请查看这里：

    - 数学 | [神奇的数字14459929](http://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168061&idx=1&sn=fb94627fdeae58393a14c1ff0e2f83d5)
    - 数学 | [数字陷阱：消失的998与98](http://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168239&idx=1&sn=0a129eddd371b256da1a07f77142720e)
    - 数学 | [千9丛中一个8](http://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168264&idx=1&sn=f347ac9b19aed672806da8a094542255)
    - 数学 | [巧合的幂之和](http://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168308&idx=2&sn=05f57fde06b36d80541eeb4dd10511fb)
    - 数学 | [sin1234567890°=?](http://mp.weixin.qq.com/s?__biz=MzA5ODc5MTU3Nw==&mid=2648168157&idx=1&sn=dcb3c3e1b099e615efce223c75ee4a68)

- [将圆周率精确到小数点后62.8万亿位！圆周率世界纪录或被刷新](https://mp.weixin.qq.com/s?__biz=MjM5NDA1Njg2MA==&mid=2652023989&idx=1&sn=6e1efc98f37ba1422492932898da74f8) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551488_link)」

  > 对圆周率π精确值的计算，可以作为高性能运算性能评估的基准。近日，来自瑞士格劳宾登应用科学大学数据分析、可视化和模拟中心（DAViS）的研究团队宣布，他们使用超级计算机，耗时108天零9小时，得到了π小数点后62.8万亿位的精确值。目前π精确值计算位数的吉尼斯世界纪录为50万亿位，由美国人蒂莫西·马利坎于2020年1月创造。该纪录的运算耗费时间超过8个月，而此次DAViS的运算速度是其3.5倍。接下来，DAViS需要将运算结果从十六进制转换成十进制，并使用特殊的算法检查其是否正确。若运算结果正确，则其将创下新的π计算位数吉尼斯世界纪录。

  - [宇宙最神奇的4组数字，两组可能隐藏宇宙真相，一组主宰着全世界！](https://new.qq.com/omn/20200322/20200322A0B0J100.html) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551499_link)」
    
    <p><img src="https://images.gitee.com/uploads/images/2022/0203/183444_a09b5cb2_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/183512_2885a2f9_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0203/183540_63a313d3_5631341.png" height="99px"></p>

    - 第一组数字：3.1415926......
    - 第三组数字：0.618
    - 第四组数字：142857

- [在线圆周率查询下载工具](https://uutool.cn/pi/) （PI 小数点后 9999 位）「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551503_link)」
  
  > 本期封面的底纹 :pray:

- [bing 圆周率 记忆方法](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551506_link)
  
  - [如何记忆圆周率-故事法 - 简书](https://www.jianshu.com/p/b27a685186bf)

    > 如何记忆圆周率-故事法 相信有很多人看过最强大脑，也特别惊奇于里面这些人的高超智商。但是这些绝大部分是有方法的。比如去年节目里来了个和"水哥"王昱珩比赛的日本老先生—原口证，他背诵圆周率达到了10万多位，非常厉害。

    当然记忆圆周率也有多种方法，比如通常的死记硬背，前面的故事法，还有定桩法。今天我就简要介绍下故事法。

    该方法有三个步骤。

    1. 对数字进行编码
    2. 链接编码，成为图像故事
    3. 利用图像故事回忆数字

# [Python练习：计算圆周率](https://zhuanlan.zhihu.com/p/144944064)

Cara
丽娃河畔的数据分析娃
​关注她

圆周率的计算有两种方法，分别是数学公式法、工程上的蒙特卡罗法。

### 1.公式法

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024353_b1b3b3fd_5631341.png "屏幕截图.png")

代码：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024409_b494d26c_5631341.png "屏幕截图.png")

运行效果：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024451_c2b7e3c8_5631341.png "屏幕截图.png")

注释：因为公式中k的上限是正无穷大，这里为了简便运算，取k=10000。

若增大k值，会发现pi的值更加精准，代价是程序运行时长更长。当我把N设为10000000后，电脑风扇开始呼呼呼了好久。



### 2.蒙特卡罗方法

核心思想：圆周率（Π）的值间接等于落入圆内的点的数量与正方形内总点数的比值。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024618_4219e7b4_5631341.png "屏幕截图.png")

为了便于计算，我们先计算1/4圆的pi值，再乘以4，得到整个圆的pi值。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024626_4f5ad1c8_5631341.png "屏幕截图.png")

1/4圆撒点模拟

代码如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024647_134d08d1_5631341.png "屏幕截图.png")

注释：

①dots是1/4正方形内总共撒的随机点数量。

②hits用于记录落在1/4圆形内的点的数量。

③random()函数用于生成0~1之间的随机数，这里是一个单位圆，正好~如果不设置种子，会自动根据操作系统当前的时间（精确到毫秒）作为种子。因此，随机生成的1000*1000个点一定是各不相同的，不会发生点重叠的情况。

④pow(x**2+y**2,0.5)用于计算随机点(x,y)到圆心O(0,0)的欧式距离。即

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024705_e169635f_5631341.png "屏幕截图.png")

⑤当距离≤1时，则说明该随机点位于1/4圆内，则将hits变量加1。

运行结果如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/024718_34ceebbb_5631341.png "屏幕截图.png")

可以看出，pi的值并不够准确，这一问题可以通过增大dots数量解决。

参考资料：北京理工大学-嵩天-《Python语言程序设计》

# [如何利用Python计算圆周率？代码实例介绍！](https://www.w3cschool.cn/article/33841075.html)
猿友 2021-07-17 09:41:04 浏览数 (527)

相信很多小伙伴小学时都有接触过圆周率，甚至有些小伙伴还能背出圆周率后好多位（虽然小编也不明白这有什么意义）。不过呢圆周率确实是一个神奇的数字，很多公式都涉及到了圆周率π（比如最出名的上帝公式——欧拉公式），那么这个圆周率是怎么算出来的呢？接下来小编同样一个python计算圆周率代码来介绍一下python怎么计算圆周率，同时可以借此机会学习一下圆周率算法。

## 一、圆周率的历史
### 1、中国

★ 魏晋时期，刘徽曾用使正多边形的边数逐渐增加去逼近圆周的方法 (即「割圆术」),求得π的近似值3.1416。

★ 汉朝时，张衡得出π的平方除以16等于5/8,即π等于10的开方(约为3.162)。虽然这个值不太准确,但它简单易理解,所以也在亚洲风行了一阵。

★ 王蕃(229-267)发现了另一个圆周率值,这就是3.156, 但没有人知道他是如何求出来的（ps. 没开源呗！）。

★ 公元5世纪,祖冲之和他的儿子以正24576边形,求出圆周率约为355/113,和真正的值相比,误差小于八亿分之一。这个纪录在一千年后才给打破。（ps. 在大部分人不知勾股定理年代，真牛！）

### 2、印度

★ 约在公元530年,数学大师阿耶波多利用384边形的周长,算出圆周率约为√9.8684。

★ 婆罗门笈多采用另一套方法,推论出圆周率等于10的平方根。（ps. 跟张衡大佬的结果一致，但过程不同）

### 3、欧洲

★ 斐波那契算出圆周率约为3.1418。

★ 韦达用阿基米德的方法,算出3.1415926535<π<3.1415926537。他是第一个以无限乘积叙述圆周率的人。

★ 鲁道夫万科伦以边数多过32000000000的多边形算出有35个小数位的圆周率。

★ 华理斯在1655年求出一道公式π/2=2×2×4×4×6×6×8×8...../3×3×5×5×7×7×9×9......

★ 欧拉发现的e的iπ次方加1等于0,成为证明π是超越数的重要依据。

## 二、用python计算圆周率π
### 【方法】蒙特卡洛法

【程序设计思路】使用python random库随机生成点，落在正方形内，计算正方形内的圆内落点与正方形内落点之比，近似为面积之比，随机数越随机，数量越大越准确。

【软件环境】python 3.6（本程序可兼容python 2.x）

### 【代码】


```
from random import random
from time import perf_counter
 
def calPI(N = 100):
    hits = 0
    start = perf_counter()
    for i in range(1, N*N+1):
        x, y = random(), random()
        dist = pow(x ** 2 + y ** 2, 0.5)
        if dist <= 1.0:
            hits += 1
    pi = (hits * 4) / (N * N)
    use_time = perf_counter() - start
    return pi, use_time
 
PI, use_time = calPI(10000)
print('use Monte Carlo method to calculate PI: {}'.format(PI))
print('use time: {} s'.format(use_time))
```

### 【结果展示】

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/023612_c631e969_5631341.png "运算结果")

震惊：10000次随机数，精确到3.1415了，把小编放在1000年前，可不得了
## 附：python输出指定精度的圆周率pi的值
首先像所有人都会的一样，本能地敲出


```
import math
val = math.pi
print(val)
```

这样就得到了pi的近似值3.141592653589793，要得到后面的小数，
不是直接可以简单粗暴的乘以10的指数


```
import math
val = math.pi * 100000000000000000
print(val)
```

但是当val的小数部分都变成整数141592653589793的时候，并不会如我们所想的那样露出后几位整数，而是直接变成科学计数法3.141592653589793e+24，所以在小数点移位之后为了看到整数部分，我们必须把float转换成int


```
import math

def get_pi_value(x):
  if(x>0):
   num = math.pow(10,x)
   val = int(math.pi * num)
   print(val)
  else:
   print('输入有误')
   
for i in range(10):
 get_pi_value(i * 10)
```

运行结果：

> 输入有误
> 31415926535
> 314159265358979334144
> 3141592653589793216413703340032
> 31415926535897931797658451191693855162368
> 314159265358979323748068948991981337089580185157632
> 3141592653589793042280431964658831312838665295201939643957248
> 31415926535897934343019391492015828684494553443559665723073458675384320
> 314159265358979299628295535813807516164434328768456060679773689288809487458631680
> 3141592653589793231804887682686061504016619085797532053907788745336000826072569315489480704

## 总结

到此这篇关于利用Python计算圆周率π的文章就介绍到这了,更多Python的有趣的学习内容请搜索W3Cschool以前的文章或继续浏览下面的相关文章。

# [三门问题的Python代码模拟](https://blog.csdn.net/u011054333/article/details/84449503)

乐百川  于 2018-11-24 16:55:18 发布  3134  收藏 16
分类专栏： python 文章标签： python
版权

三门问题是一个经典的概率问题，问题复制自百度百科：

> 参赛者会看见三扇关闭了的门，其中一扇的后面有一辆汽车，选中后面有车的那扇门可赢得该汽车，另外两扇门后面则各藏有一只山羊。当参赛者选定了一扇门，但未去开启它的时候，节目主持人开启剩下两扇门的其中一扇，露出其中一只山羊。主持人其后会问参赛者要不要换另一扇仍然关上的门。问题是：换另一扇门会否增加参赛者赢得汽车的机率？如果严格按照上述的条件，即主持人清楚地知道，自己打开的那扇门后是羊，那么答案是会。不换门的话，赢得汽车的几率是1/3。换门的话，赢得汽车的几率是2/3。

这个问题常见的误区在于以为换不换门概率一样。不过这里有一个假定需要特别注意，就是主持人知道哪扇门后面有奖，哪扇门后面没奖，并会帮助参赛者排除一个错误答案，这样一来答案就比较容易理解了。

当然，为了使问题更明确，人们还更清晰的规定了假设，同样摘自百度百科：

Mueser 和 Granberg 透过厘清细节，以及对主持人的行为加上明确的介定，提出了对这个问题的一种不含糊的陈述 [5]

* 现在有三扇门，只有一扇门有汽车，其余两扇门的都是山羊。
* 汽车事前是等可能地被放置于三扇门的其中一扇后面。
* 参赛者在三扇门中挑选一扇。他在挑选前并不知道任意一扇门后面是什麽。
* 主持人知道每扇门后面有什么。
* 如果参赛者挑了一扇有山羊的门，主持人必须挑另一扇有山羊的门。
* 如果参赛者挑了一扇有汽车的门，主持人等可能地在另外两扇有山羊的门中挑一扇门。
* 参赛者会被问是否保持他的原来选择，还是转而选择剩下的那一扇门.

将问题完全明确化之后，我们就可以用代码来模拟了。Python代码很简单：

# 三门问题代码模拟



```
def three_door_question(times: int):
    import random
    result_if_not_change = 0
    result_if_change = 0
    for i in range(0, times):
        doors = [1, 2, 3]  # 3是大奖
        random.shuffle(doors)
        first_choice = doors[random.randint(0, 2)]
        doors.remove(first_choice)
        # 如果大奖在剩下的里面，由主持人排除一个错误答案，剩下大奖
        if 3 in doors:
            doors = [3]
        # 如果大奖已经被选了，主持人随机排除剩下一个错误答案
        else:
            doors.remove(random.choice((1, 2)))
        if first_choice == 3:
            result_if_not_change = result_if_not_change + 1
        if doors[0] == 3:
            result_if_change = result_if_change + 1

    print(
        f'Total times:{times}, prob of not change is {result_if_not_change / times}, prob of change is {result_if_change / times}')


three_door_question(10000)
three_door_question(100000)
three_door_question(1000000)

```

# 运行结果


```
Total times:10000, prob of not change is 0.329, prob of change is 0.671
Total times:100000, prob of not change is 0.33306, prob of change is 0.66694
Total times:1000000, prob of not change is 0.333483, prob of change is 0.666517

```


可以看到，随着次数逐渐增加，换门的结果越来越趋向于2/3，确实验证了前面的结论。当然，这个代码写的比较简单，所以性能比较差，有兴趣的同学可以使用其他语言或者方法来实现。


[![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/022722_dcfdd9ba_5631341.png "屏幕截图.png")
](https://mp.weixin.qq.com/s?__biz=MzI2NjE0MTY0MA==&mid=2652725202&idx=3&sn=ef1d71ee025dc9537cda8ed66fa5bc5d)

计算机模拟29次的结果/图片来源：维基百科

# [如何记忆圆周率-故事法](https://www.jianshu.com/p/b27a685186bf) 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551506_link)」
  
（记忆教练赵俊祥）2018.01.14 18:47:07

相信有很多人看过最强大脑，也特别惊奇于里面这些人的高超智商。但是这些绝大部分是有方法的。比如去年节目里来了个和"水哥"王昱珩比赛的日本老先生—原口证，他背诵圆周率达到了10万多位，非常厉害。他使用的方法是编故事的方法。

当然记忆圆周率也有多种方法，比如通常的死记硬背，前面的故事法，还有定桩法。今天我就简要介绍下故事法。

该方法有三个步骤。

1. 对数字进行编码
2. 链接编码，成为图像故事
3. 利用图像故事回忆数字

比如针对小数点后16位，3.1415 9265 3589 7932

- 一：对数字进行编码

  编码方法有谐音、形象、含义等等。比如这里面的14钥匙、15鹦鹉、92球儿、65尿壶、35珊瑚、89芭蕉（香蕉）、79气球、32扇儿都是谐音。（形象：6是勺子就是形象，含义：24联想到24小时联想到闹钟。）这个步骤很关键，在确定自己的编码之后就要多练习，争取达到看到数字就能想到编码图的效果。

- 二：链接编码，成为图像故事

  这里有几个关键词，链接，图像。意思是说编码不要只是停留在表面上，而要深入进去，在脑海里想象出图像，才是重点。比如上面的16位圆周率，请跟着我的文字想象图像：你手里拿着一把金黄色的钥匙（14）戳一只绿色鹦鹉（15）的头，一不小心扎到了它的眼睛，眼睛里出来一个白色球儿（92），落到了你脚旁边的尿壶（65）里，里面的黄色尿溅到了周围的红色珊瑚（35）上，珊瑚有了肥料于是疯长，长出来一个大香蕉（89）和一个白色气球（79），你仔细看去会发现这个气球形状一直在扭曲，突然爆炸了，飞出来一把白色羽毛扇儿（32）。你可能会有疑问这个文字也太长了吧，对，确实挺长，但是如果在脑海中想图像了，你会发现会很容易记住这个过程。而且如果多练习的话，会非常快，就算是40位也只需要十几秒甚至几秒。

- 三：利用图像故事回忆数字

  这一步就是首先在脑海中回忆刚刚的图像过程，然后回忆到对应的图像编码时就转换成对应的数字就行。比如想到了钥匙去戳鹦鹉，就回忆出了1415，之后类似。

总体上就是这样。

# [上帝公式：欧拉公式的宇宙学意义](https://zhuanlan.zhihu.com/p/90328091)

泰文河
物理生物
​关注

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030647_a6e3adb8_5631341.png "屏幕截图.png")

傅里叶分析，最欣赏Heinrich 生娃学工打折腿的讲解，既是科学，又是艺术享受。

### 宇宙耍帅第一公式：欧拉公式

虚数i这个概念大家在高中就接触过，但那时我们只知道它是-1的平方根，可是它真正的意义是什么呢?

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030714_b2a112c6_5631341.png "屏幕截图.png")

这里有一条数轴，在数轴上有一个红色的线段，它的长度是1。当它乘以3的时候，它的长度发生了变化，变成了蓝色的线段，而当它乘以-1的时候，就变成了绿色的线段，或者说线段在数轴上围绕原点旋转了180度。



我们知道乘-1其实就是乘了两次 i使线段旋转了180度，那么乘一次 i 呢——答案很简单——旋转了90度。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030732_7db33d34_5631341.png "屏幕截图.png")

同时，我们获得了一个垂直的虚数轴。实数轴与虚数轴共同构成了一个复数的平面，也称复平面。这样我们就了解到，乘虚数i的一个功能——旋转。

现在，就有请宇宙第一耍帅公式欧拉公式隆重登场——

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030750_58d29f0f_5631341.png "屏幕截图.png")

这个公式在数学领域的意义要远大于傅里叶分析，但是乘它为宇宙第一耍帅公式是因为它的特殊形式——当x等于Pi的时候。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030803_a1c3b033_5631341.png "屏幕截图.png")

经常有理工科的学生为了跟妹子表现自己的学术功底，用这个公式来给妹子解释数学之美：”石榴姐你看，这个公式里既有自然底数e，自然数1和0，虚数i还有圆周率pi，它是这么简洁，这么美丽啊！“但是姑娘们心里往往只有一句话：”臭屌丝……“



这个公式关键的作用，是将正弦波统一成了简单的指数形式。我们来看看图像上的涵义：


![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030821_5a3dff24_5631341.png "屏幕截图.png")



欧拉公式所描绘的，是一个随着时间变化，在复平面上做圆周运动的点，随着时间的改变，在时间轴上就成了一条螺旋线。如果只看它的实数部分，也就是螺旋线在左侧的投影，就是一个最基础的余弦函数。而右侧的投影则是一个正弦函数。

关于复数更深的理解，大家可以参考：

[复数的物理意义是什么？](http://www.zhihu.com/question/23234701/answer/26017000)

这里不需要讲的太复杂，足够让大家理解后面的内容就可以了。

--------------------------------------------------------------------------

以上是Heinrich 生娃学工打折腿讲解。

关于复数，以及整个公式的宇宙学意义，Heinrich 生娃学工打折腿没有深入讲解，今天续上这一部分，算是对于欧拉公式的完整讲解。

现代物理学告诉我们，宏观宇宙处处是旋转的，微观世界也是如此，都带有圆周运动和自旋性，而欧拉公式描述的核心正是旋转与频率，因此，在物理学定量意义上讲，欧拉公式正好表述了宇宙的宏观和微观运动，称它是宇宙第一公式一点也不为过！

时空阶梯理论揭示，宇宙的根源是暗物质，暗物质极化产生收缩的物质世界和膨胀的暗能量世界。物质世界是粒子世界，速度小于光速，而暗能量世界是波动世界，速度大于光速。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030920_cc357fec_5631341.png "屏幕截图.png")

当洛伦兹因子中的速度v大于光速的时候，就出现了虚数，所以，虚数的宇宙学意义是超光速，也是暗能量的表达方式。暗能量在哪里？就在欧拉公式中。

至于暗物质的表达，这里需要一些说明，否则讲不清楚。

一般的数字，123456789，一般是表达实体物质的，一个苹果，两个苹果，.....,那么零代表什么都没有，其中，这里的零从时空阶梯理论看，是暗物质，并不是什么都没有。那么暗物质是什么？时空阶梯理论揭示，暗物质是能量场和气场，是场物质。能量场转化为气场，气场转化为能量场，两者相互转化，但是，这里没有物质粒子。当产生物质粒子的时候，就有暗能量的膨胀产生。所以，物质粒子和暗能量是成对出现的。所以，定义零是暗物质，只是从物质粒子的角度来规定的，而不是什么都没有。同样，从暗能量的角度也可以把暗物质定义为零，因为暗能量就是从零开始产生膨胀能量的。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/030942_2de32069_5631341.png "屏幕截图.png")

暗物质=物质+暗能量

所以，时空阶梯理论的核心内容，可以表示为：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031001_743772ce_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031024_5c07ba09_5631341.png "屏幕截图.png")

表示为没有极化的暗物质

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031034_baa15856_5631341.png "屏幕截图.png")

表示为暗物质极化产生的物质：小于光速

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031044_0653ae2d_5631341.png "屏幕截图.png")

表示为暗物质极化产生的暗能量：大于光速


时空阶梯理论认为，宇宙的根源是暗物质，暗物质极化产生收缩的物质世界和膨胀的暗能量。物质世界不断收缩，形成引力时空、弱力时空、电磁力时空和强力时空，就是不断收缩，不断压缩。这个理论不仅可以解释暗物质的星系自转曲线，能解释与暗能量有关的现象，而且能解释神秘的双缝实验。

对于J粒子的解释，自然是物质世界收缩的结果，而且物质世界的收缩是因为暗能量膨胀的结果，而现在证明宇宙在加速膨胀，所以，原子内的应该是不断收缩的，而不断收缩，就会产生更多的粒子。这里的1就代表粒子物质，假如还有更新的粒子被发现，对于时空阶梯理论来讲，并不新鲜，因为宇宙在加速膨胀，所以，原子就是在加速收缩，而收缩就会产生新的粒子。

但是，按照时空阶梯理论，宇宙膨胀和原子收缩都是有限度的，到了一定程度，宇宙会收缩，而原子会膨胀，到了那个时候，原子内的粒子是逐渐减少的。到了一定程度，代表1的粒子也会消失，等到所有的粒子都消失了 ，就是宇宙回到宇宙的根源：暗物质。而暗物质不稳定，在希格斯机制下，重新极化，又产生新的收缩的物质世界和膨胀的暗能量世界，这是一个无限循环的宇宙模式。

而欧拉公式正好可以表达这个循环：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031101_0f7195b4_5631341.png "屏幕截图.png")

### 总结如下：

1. 欧拉公式是宇宙的演化公式。

假如有人问，宇宙是如何演化的？答曰：欧拉公式。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0203/031138_11ee566e_5631341.png "屏幕截图.png")

看，多简单，多完美，多帅！宇宙第一帅，帅呆了吧！！！！！

2. 拉公式表述了宇宙的宏观和微观运动。

发布于 2019-11-05 23:46