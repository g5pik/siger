<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0307/104332_5105f1d8_5631341.png" title="死亡密码π|Pi电影.png"></p>

> 马科斯，我们可以玩玩吗？  
> 吉娜...  
> 322x491=?  
> 158102,right?  
> 对  
> ok 73/22=?  
> 3.3181818  
> 181818...

这是电影开场，介绍主角的方式，一位天才数学家在陪伴邻家女孩玩人脑和计算器的算数比赛...

> 12:45pm  
> 重述我的假设  
> 1. 数学是自然的语言
> 2. 我们周围的所有事情都可以用数字表述
> 3. 将任何数字系统图像化则可以发现其模式
> 
> 所以在自然界中到处存在模式

> 证明：
> - 疾病传染周期
> - 驯鹿数量的增减变化
> - 太阳黑子周期
> - 尼罗河的涨落

> 那么 Market 有模式吗?  
> 这个表示全球经济的数位系统是一个有机体  
> 一个包含无数双工作的双手  
> 无数思考的大脑  
> 有着巨大网络的喧嚣的生命体  
> 是一个自然的有机体

> 我有个假设  
> 整个 Market 的运行数字背后  
> 也一定有一个模式在运行  
> 就在我面前，存在一个 NUMBERs  
> 它是怎么运行的？  
> 按 return 键

这是电影旁白的叙事体，主人公的私人笔记，展示了一个天才的构想，

> 你真是发疯了，马科斯  
> 也可能是天才  
> 我必须找到那个数字

> 住口，你正在失去理智  
> 你必须放松一下  
> 听从自己的声音．  
> 你遇到了我也遇到的 电脑 BUG  
> 和一些虔诚的笨蛋．  
> 如果你想找 216  
> 你可以在任何地方找到  
> 从街的拐角，到你家门前的 216步  
> 你站在电梯的 216 秒  
> 当你的理智被迷惑的时候  
> 你会忽略所有其他的事情，而只看到你要看到的事情  
> 320, 450, 22等等  
> 你选择了 216 你就可以发现在自然界中到处都是  
> 但是，马科斯  
> 当你放弃了科学的严谨．  
> 你就不再是一名数学家了．  
> 你就变成一个用数字占卜的人

这是马科斯的导师，惊醒他的话，但这位天才却充耳不闻，现在看来他＂走火入魔＂啦．

> 随着电影序幕拉开，3.141592652631245342356795342354... 满屏幕的数字填充了字母 π 的背景，这就是海报的主元素．随着越来越多的数字淹没了 π 符号，我发现有些数字被扣掉了．这难道是导演特意为之的线索？我想我也已经被这深深的数字漩涡吸引啦，走火入魔，并不是身在其中的人愿意承认的，就好像主角马科斯．这是唯一一篇不需要叙事的期刊，他更像一个影迷对导演作品的膜拜，和所有粉丝一样，希望角色能够附体，成为一个虔诚的傻瓜．太多的线索契合当下的[数学](PI.md)主题． _数字系统图像化则可以发现其模式_ 不正式[直观的数学](最难科普的数学.md)的目标吗？我现在叫他[生动的数学](最难科普的数学.md)．而电影中的费式数列也才刚刚作为 Python 主题出现过．制作本期主题，我应用了大量的 Python Soup 脚本协助处理．片头大量的数学公式和几何数学图像中，我认出了傅式变换，伴随时间轴的螺旋前进．黄金分割的[维特鲁威人](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue2%20-%20Opreating%20System%20Shiliu%20Pi%2099%20was%20Released.md#2021-11-29)，是被现代人誉为达芬奇密码的矿世杰作，正式石榴派国际漂流的[封面](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue2%20-%20Opreating%20System%20Shiliu%20Pi%2099%20was%20Released.md#2021-11-29) ... 我们都身在这个数字漩涡中，必然成为这个漩涡的一部分，以这个漩涡的方式运行着，呈现着，唯有 GOD 的视角，才能清醒地看到．而人类自身正在无限逼近 GOD 的角色． [π](PI.md) 正在指引我沿着数学的方向，重新认识世界的过程．2.3026 就是其中一个，CUBISM 立体主义 是另一个．我庆幸身在其中
:pray: 下期我们将一起随 《[爱因斯坦与霍金：解锁宇宙](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068181_link)》 :pray:

- π|Pi电影

  > 别　名: Pi π 圆周率 数字漩涡 盛开的数字  
  > 地　区: 美国语　言: 英语  
  > 上映时间: 1998  
  > 简　介： 犹太天才数学家马克西·米利安·科恩（希恩·格莱特 Sean Gullette饰）痴迷沉醉于数字的世界，他认为一切自然界的事物都可以用数字解释。他潜心研究，试图透过数字推演出种种生命迹象背后的奥秘和规律。可越是逼近答案，他越是无法忍受致命的头痛。他发现了过去十年来股票市场的混乱波动，其实是由背后的一套数学模式在操纵。与此同时，他的研究引来了华尔街股票市场的炒家们和犹太神秘教派的注意。在介乎真实与虚拟的世界中，马克西正在走向疯狂。 1996年2月，阿伦诺夫斯基开始筹备这部成本只有六万美元的长片处女作。两年后，《死亡密码》以其新颖的创意和崭新的拍摄手法，一鸣惊人。先是斩获圣丹斯电影节的导演奖，翌年又摘得独立精神奖的最佳编剧处女作奖。2000年，他再次自编自导了《梦之安魂曲》，也赢得了不错的反响。2005年，他还入围“最需要关注的好莱坞100人”。

  - [影剧中的数理](https://space.bilibili.com/32012983/channel/seriesdetail?sid=1334873) [48: π|Pi (1998)](https://www.bilibili.com/video/BV1qU4y1p7Dq) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067977_link)］ ［[全集](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9057451_link)］
  - [学用数学](https://space.bilibili.com/32012983) [专栏文章](https://space.bilibili.com/32012983/article) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063825_link)］    
  - [Piπ|密码 观影笔记](https://zhuanlan.zhihu.com/p/105294387) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9051056_link)］
  - [GeoGebra是什么神仙软件啊！](https://zhuanlan.zhihu.com/p/142271439) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067494_link)］ [官网](https://www.geogebra.org/about) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067687_link)］ [许可证](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067857_link)

<p><img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041132_4a3ef4d2_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041200_0c0d7fdd_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041254_01f0bb8c_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041306_c20a6233_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041322_23897777_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041339_3ddb9e18_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041358_d687601c_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041422_89a2c55e_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041437_9a261a99_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041453_6f117132_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041511_3f90cc9c_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041529_a87245a4_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041543_80cf8c2e_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041600_36923225_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041613_b21b910f_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041631_8ce719bc_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041645_e2a83dfa_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041700_4089e3ac_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041716_c326b699_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041734_f7b263c1_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041749_01e160d2_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041804_f5b83880_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041818_476222e0_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041830_38507281_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041843_42d8d48e_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041858_1f54fa8e_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/041911_ae1aaa0f_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/043755_e6e661a4_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/043740_4331e0f2_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/043808_a8549928_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/045224_e0eef950_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/045240_399e3a36_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/051834_f296e4ea_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/051856_124b5816_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/051920_bfd7cd5f_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/051936_a7f70269_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/051949_97f0f431_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/052136_22105174_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/052003_5dabaf00_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/055701_c5892404_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/064914_ba910728_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/064929_8ffa2d1a_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/064943_f913d790_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/064959_a3f2863b_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/064959_a3f2863b_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/065026_9eabb02c_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/065039_8d2efe39_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/065052_6ec99ce7_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/065109_235b207f_5631341.png"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0308/065122_d2871769_5631341.png"></p>

# [影剧中的数理](https://space.bilibili.com/32012983/channel/seriesdetail?sid=1334873) ［[全集](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9057451_link)］

| img | title | info |
|---|---|---|
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/153748_6b14131a_5631341.jpeg" title="MM02 城市广场 Agora ：圆锥曲线与椭圆"> |MM02 城市广场 Agora ：圆锥曲线与椭圆<br><a target="_blank" href="https://www.bilibili.com/video/BV1gT4y1E7iZ">www.bilibili.com/video/BV1gT4y1E7iZ</a> | 02:30<br>163 <br>2020-7-9 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/153757_43482346_5631341.jpeg" title="05 超级30 Super 30 (2019) ：Super30 黄金比例"> |05 超级30 Super 30 (2019) ：Super30 黄金比例<br><a target="_blank" href="https://www.bilibili.com/video/BV1xQ4y1Z7xi">www.bilibili.com/video/BV1xQ4y1Z7xi</a> | 02:41<br>107 <br>2021-4-29 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/153805_07984a79_5631341.jpeg" title="13： 爱因斯坦与爱丁顿(2008)"> |13： 爱因斯坦与爱丁顿(2008)<br><a target="_blank" href="https://www.bilibili.com/video/BV19t4y1X7XH">www.bilibili.com/video/BV19t4y1X7XH</a> | 16:30<br>1461 <br>2020-6-21 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/153920_b34ffa0d_5631341.jpeg" title="15 ZERO 一获千金游戏Ep8： 十分逼近法"> |15 ZERO 一获千金游戏Ep8： 十分逼近法<br><a target="_blank" href="https://www.bilibili.com/video/BV1xz4y1X7gy">www.bilibili.com/video/BV1xz4y1X7gy</a> | 03:34<br>1142 <br>2020-7-5 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/153950_5e664240_5631341.jpeg" title="17 撲通撲通love-勾股定理"> |17 撲通撲通love-勾股定理<br><a target="_blank" href="https://www.bilibili.com/video/BV1rp4y1S7uH">www.bilibili.com/video/BV1rp4y1S7uH</a> | 02:33<br>308 <br>2020-7-18 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154000_714c58a7_5631341.jpeg" title="19 牛津兇殺案"> |19 牛津兇殺案<br><a target="_blank" href="https://www.bilibili.com/video/BV1Na4y1J7wQ">www.bilibili.com/video/BV1Na4y1J7wQ</a> | 24:29<br>118 <br>2020-8-2 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154008_8685ef50_5631341.jpeg" title="20 数学女孩的恋爱事件簿E2：条件机率"> |20 数学女孩的恋爱事件簿E2：条件机率<br><a target="_blank" href="https://www.bilibili.com/video/BV1BK411T71S">www.bilibili.com/video/BV1BK411T71S</a> | 02:39<br>322 <br>2020-8-10 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154017_29eb7fa9_5631341.jpeg" title="21: X+Y 愛的方程式"> |21: X+Y 愛的方程式<br><a target="_blank" href="https://www.bilibili.com/video/BV13Z4y1K7td">www.bilibili.com/video/BV13Z4y1K7td</a> | 22:46<br>642 <br>2020-8-17 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154027_e05ede5b_5631341.jpeg" title="22 雨人(1988)：自閉症、心算、算牌"> |22 雨人(1988)：自閉症、心算、算牌<br><a target="_blank" href="https://www.bilibili.com/video/BV1DK4y1Y7oh">www.bilibili.com/video/BV1DK4y1Y7oh</a> | 08:05<br>176 <br>2020-8-23 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154036_d0ad067f_5631341.jpeg" title="24 Inception ：Penrose 階梯v2"> |24 Inception ：Penrose 階梯v2<br><a target="_blank" href="https://www.bilibili.com/video/BV1K64y1F7Tu">www.bilibili.com/video/BV1K64y1F7Tu</a> | 02:55<br>57 <br>2020-9-7 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154047_c8229c2a_5631341.jpeg" title="28 Theory about Everything 愛的萬物論：數學與黑洞"> |28 Theory about Everything 愛的萬物論：數學與黑洞<br><a target="_blank" href="https://www.bilibili.com/video/BV1gV41127rY">www.bilibili.com/video/BV1gV41127rY</a> | 28:54<br>161 <br>2020-10-7 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154056_2d860717_5631341.jpeg" title="30 美丽心灵 A Beautiful Mind"> |30 美丽心灵 A Beautiful Mind<br><a target="_blank" href="https://www.bilibili.com/video/BV19f4y1B717">www.bilibili.com/video/BV19f4y1B717</a> | 37:17<br>186 <br>2020-10-24 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154105_61d6179b_5631341.jpeg" title="32 詐欺遊戲S2E2 17Poker 的几率分析"> |32 詐欺遊戲S2E2 17Poker 的几率分析<br><a target="_blank" href="https://www.bilibili.com/video/BV1Ti4y1L718">www.bilibili.com/video/BV1Ti4y1L718</a> | 13:53<br>206 <br>2020-11-4 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154115_424e7a03_5631341.jpeg" title="33 赌博默示录 剪辑"> |33 赌博默示录 剪辑<br><a target="_blank" href="https://www.bilibili.com/video/BV1gf4y1q7zb">www.bilibili.com/video/BV1gf4y1q7zb</a> | 08:14<br>159 <br>2020-11-10 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154124_8cb91c44_5631341.jpeg" title="34 UFO 幽浮檔案"> |34 UFO 幽浮檔案<br><a target="_blank" href="https://www.bilibili.com/video/BV1iV411a7JN">www.bilibili.com/video/BV1iV411a7JN</a> | 20:53<br>129 <br>2020-11-18 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154135_cbf929b1_5631341.jpeg" title="35 無間道莫斯密码"> |35 無間道莫斯密码<br><a target="_blank" href="https://www.bilibili.com/video/BV1zr4y1c73N">www.bilibili.com/video/BV1zr4y1c73N</a> | 07:19<br>520 <br>2020-11-25 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154145_0f8ed575_5631341.jpeg" title="36 后翼棄兵E2 ：二項式定理"> |36 后翼棄兵E2 ：二項式定理<br><a target="_blank" href="https://www.bilibili.com/video/BV1r5411n7KT">www.bilibili.com/video/BV1r5411n7KT</a> | 00:28<br>60 <br>2021-1-13 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154155_c886d92b_5631341.jpeg" title="37 銀河補習班 ：連通器原理"> |37 銀河補習班 ：連通器原理<br><a target="_blank" href="https://www.bilibili.com/video/BV1s54y1s7Ce">www.bilibili.com/video/BV1s54y1s7Ce</a> | 02:45<br>1540 <br>2021-1-13 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154207_7f7741c9_5631341.jpeg" title="38 玛蒂尔达 1996：多位數乘法"> |38 玛蒂尔达 1996：多位數乘法<br><a target="_blank" href="https://www.bilibili.com/video/BV16v411W7ST">www.bilibili.com/video/BV16v411W7ST</a> | 02:28<br>57 <br>2021-1-13 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154217_9d7fe215_5631341.jpeg" title="39 人不彪悍枉少年 2018：等比級數"> |39 人不彪悍枉少年 2018：等比級數<br><a target="_blank" href="https://www.bilibili.com/video/BV1W54y1s7HZ">www.bilibili.com/video/BV1W54y1s7HZ</a> | 01:31<br>163 <br>2021-1-13 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154228_bc6cb2cf_5631341.jpeg" title="40 社交網絡 Social Network 2010"> |40 社交網絡 Social Network 2010<br><a target="_blank" href="https://www.bilibili.com/video/BV1oy4y117qG">www.bilibili.com/video/BV1oy4y117qG</a> | 04:27<br>111 <br>2021-1-28 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154237_7a7011c7_5631341.jpeg" title="42 怦然心動：菱形面積公式"> |42 怦然心動：菱形面積公式<br><a target="_blank" href="https://www.bilibili.com/video/BV1uN411d7Fo">www.bilibili.com/video/BV1uN411d7Fo</a> | 01:18<br>75 <br>2021-2-13 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154247_cd2e5710_5631341.jpeg" title="43 温暖的抱抱：周长面积公式"> |43 温暖的抱抱：周长面积公式<br><a target="_blank" href="https://www.bilibili.com/video/BV1Lo4y197TP">www.bilibili.com/video/BV1Lo4y197TP</a> | 01:06<br>72 <br>2021-2-14 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154257_e1a65121_5631341.jpeg" title="44 少年谢尔顿S1E6：火箭回收的数学分析"> |44 少年谢尔顿S1E6：火箭回收的数学分析<br><a target="_blank" href="https://www.bilibili.com/video/BV1zi4y1N7HU">www.bilibili.com/video/BV1zi4y1N7HU</a> | 02:48<br>82 <br>2021-3-2 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154307_f7a8ec28_5631341.jpeg" title="45 模仿犯（泰剧 2020）：  圆周率"> |45 模仿犯（泰剧 2020）：  圆周率<br><a target="_blank" href="https://www.bilibili.com/video/BV1ab4y1X7Py">www.bilibili.com/video/BV1ab4y1X7Py</a> | 04:49<br>111 <br>2021-3-2 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154316_c5e952eb_5631341.jpeg" title="46 危机边缘 Fringe（2010）： 圆周率"> |46 危机边缘 Fringe（2010）： 圆周率<br><a target="_blank" href="https://www.bilibili.com/video/BV1NK4y1n7hr">www.bilibili.com/video/BV1NK4y1n7hr</a> | 07:38<br>62 <br>2021-3-9 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154325_27007d21_5631341.jpeg" title="48 死亡密码 Pi (1998)"> |48 死亡密码 Pi (1998)<br><a target="_blank" href="https://www.bilibili.com/video/BV1qU4y1p7Dq">www.bilibili.com/video/BV1qU4y1p7Dq</a> | 17:00<br>509 <br>2021-3-9 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154335_149434aa_5631341.jpeg" title="50 天地明察"> |50 天地明察<br><a target="_blank" href="https://www.bilibili.com/video/BV1v64y1U7i5">www.bilibili.com/video/BV1v64y1U7i5</a> | 03:53<br>120 <br>2021-4-25 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154344_d60f0c5e_5631341.jpeg" title="51 唐老鸭数学冒险之旅 (1959)"> |51 唐老鸭数学冒险之旅 (1959)<br><a target="_blank" href="https://www.bilibili.com/video/BV13Q4y1f7qt">www.bilibili.com/video/BV13Q4y1f7qt</a> | 08:23<br>209 <br>2021-4-29 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/154354_143667bb_5631341.jpeg" title="52 為人師表-stand and deliver (1988)"> |52 為人師表-stand and deliver (1988)<br><a target="_blank" href="https://www.bilibili.com/video/BV1R64y1y7jG">www.bilibili.com/video/BV1R64y1y7jG</a> | 17:42<br>3196 <br>2021-4-25 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/163243_19cc4854_5631341.jpeg" title="55 阿基米德大战 The Great War of Archimedes (2019) 混剪"> |55 阿基米德大战 The Great War of Archimedes (2019) 混剪<br><a target="_blank" href="https://www.bilibili.com/video/BV1XN411Z7Jw">www.bilibili.com/video/BV1XN411Z7Jw</a> | 20:40<br>174 <br>2021-5-24 |
| <img width="99px" src="https://images.gitee.com/uploads/images/2022/0307/163257_a1c6fc77_5631341.jpeg" title="数字追凶S1E6 03700-03800 费氏数列"> |数字追凶S1E6 03700-03800 费氏数列<br><a target="_blank" href="https://www.bilibili.com/video/BV1pz4y1f7L6">www.bilibili.com/video/BV1pz4y1f7L6</a> | 00:48<br>406 <br>2020-8-18 |

# [学用数学](https://space.bilibili.com/32012983) - [专栏文章](https://space.bilibili.com/32012983/article) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063825_link)］

1. <a title="这次的基础课要介绍 Geogebra 的运算区，这区域主要能作符号运算。本节只介绍最基本的操作，最后用解含参方程为例，来显示其符号运算的功" target="_blank" href="https://www.bilibili.com/read/cv15545887">S0A2 用运算区解含参方程</a> (19小时前)
1. <a title="从圆内发射一个入射角为 10 度的光线，经过几次后会反射为原出发点呢？如何用GGB来实践问题呢？在这个案例要带大家体验的就是问题拆解的方法" target="_blank" href="https://www.bilibili.com/read/cv15543645">S5R1 圆内的迭代反射</a> (22小时前)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15543546">S0G2 用GGB 实作圆面积的拼贴动画</a> (22小时前)
1. <a title="今天我们的解謎片段，取自《成就天才的妻子，~世界公认的数学家与妻子的爱》这部片的是描述日本数学家冈洁(1901--1978)，研究领域为多" target="_blank" href="https://www.bilibili.com/read/cv15542735">S5Q3 影剧中的数学：用复数作几何变换</a> (23小时前)
1. <a title="本节主要学习 Geogebra 的曲线区域上色几种方法，使用 Locus、不等式、与积分。问题拆解1. 绘制月牙形2. 对曲线区域上色(L" target="_blank" href="https://www.bilibili.com/read/cv15541859">S304 希波克拉底月牙形面积</a> (昨天)
1. <a title="这次要介绍的 GGB 学习资源是来自我的同乡台湾的官长寿老师。官老师早期接触 Geogebra 的时间应该也有 12 年以上了。在还没有 " target="_blank" href="https://www.bilibili.com/read/cv15524675">VG03 官长寿的GGB线上课件</a> (3-5)
1. <a title="这两年 Geogebra 的搜寻热度不断地在上升，很多想加入的新人也在问有哪些 Geogebra 学习资源呢？这时就可来看这份很完整的 G" target="_blank" href="https://www.bilibili.com/read/cv15524419">VG02 少数派Pi ：GGB 新手教程推荐</a> (3-5)
1. <a title="很多先接触几何画版再接触 GGB 的人都会抱怨 GGB 没有分页模式。但其实要实现分页模式不难，主要就是通过条件显示就可达成这样的效果。而" target="_blank" href="https://www.bilibili.com/read/cv15523811">S5Q2 GGB的分页模版：以余弦定理为例</a> (3-5)
1. <a title="用 Geogebra 来绘制函数很方便，直接输入观察就可。但分段函数要如何输入呢？其实也很简单就是通过 If 就可以实践了。课件效果建立三" target="_blank" href="https://www.bilibili.com/read/cv15523682">S0A1 Geogebra新手课：分段函数的绘制，以《一次函数方案选择》为例</a> (3-5)
1. <a title="几何证明难的往往不是逻辑推论。困难的是关键辅助线如何想到。在今天想与大家分享的GGB课件，就是以洋葱学院在平行四边形的综合大题为例。一同来" target="_blank" href="https://www.bilibili.com/read/cv15523523">VG01 用旋转、对称、平移来作几何题的辅助线（洋葱教研的 Geogebra 课件推荐）</a> (3-5)
1. <a title="本周的学用数学要来玩转时钟。作出可随时间转动的时钟，并可调整转速，观察数字时钟与指针时钟。在技巧上最主要是练习序列来绘制时钟，并且用滑动杆" target="_blank" href="https://www.bilibili.com/read/cv15511334">S5G3 用 GGB 作动态时钟</a> (3-4)
1. <a title="在先前 S4E05概率实验难演示？不存在的！ 这文章中，介绍了随机转盘的制作方式，在这篇文章将来演示如何取得转盘的结果，并绘制些统计图表来" target="_blank" href="https://www.bilibili.com/read/cv15498869">S4E19 概率实验难演示？(下)</a> (3-3)
1. <a title="在人教版五年级数学的学习中，学生第一次接触到“概率”，到初中九年级时，对机率有进一步认识，接触到实验机率模拟的概念。但是课堂上老师可以通过" target="_blank" href="https://www.bilibili.com/read/cv15498267">S405 概率实验难演示？(上）</a> (3-3)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15498067">S4E08 Penrose阶梯：哪里是最高点？</a> (3-3)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15498052">S4E10：Snakify Lesson2 - 0.1+0.2不等于0.3？快来一探究竟吧～</a> (3-3)
1. <a title="对于将军饮马的两折线最值问题，我们拓展到三折线，连接长方形内对角两点的最短距离，对于这个问题该如何解呢？让我们用 GGB 来探究吧。1 三" target="_blank" href="https://www.bilibili.com/read/cv15494923">S0G18 三折线的最短路径问题</a> (3-3)
1. <a title="在GGB中可以绘制函数曲线大家已经再熟悉不过了，那么数列的规律可以在GGB进行绘制和展示吗？今天我们就来以费氏数列为例，看在GGB中如何来" target="_blank" href="https://www.bilibili.com/read/cv15483764">S4E11 GGB 绘制数列曲线，看出数列走向</a> (3-2)
1. <a title="我们先看一下今天的效果图。猜猜看：右边的葱宝绕着左边的葱宝旋转半圈，葱宝头朝哪边呢？答案见①今天我们通过GGB来制作这样一个动图，还附属的" target="_blank" href="https://www.bilibili.com/read/cv15483745">S4E09：转动的圆，转出笛卡尔心形线</a> (3-2)
1. <a title="今天要介绍的这个是 2016 错觉艺术大奖的得奖作品  Ambiguous Cylinder Illusion 。图片来源于网络这样的错觉" target="_blank" href="https://www.bilibili.com/read/cv15483725">S4E13 是圆还是方？</a> (3-2)
1. <a title="趁着圣诞夜，本周任务就是要来绘制如下的圣诞树。在这篇文案，主要想与老师分享些，以「自我提问」来解决问题的方法。以这个圣诞树为例，第一部自我" target="_blank" href="https://www.bilibili.com/read/cv15483682">S4E14 圣诞树</a> (3-2)
1. <a title="日常教学中我们会遇到各种各样的函数或者是曲线，希望可以描绘在GGB中，能够帮助我们更好的教学但是每一种图象该如何绘制呢？又可以使用什么样的" target="_blank" href="https://www.bilibili.com/read/cv15483626">S4E12 画出任何你想要的数学图象，快来学习吧</a> (3-2)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15469243">S4E24 Python Snakify L4 for-loop回圈</a> (3-1)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15469234">S4E18 Python Snakify L3 条件判断</a> (3-1)
1. <a title="这一节接续上节 S4E15 Python 数列与Pi 的探究（上）的课程来作数列的探究。本节主要学会 for-loop 的回圈，并以计算以" target="_blank" href="https://www.bilibili.com/read/cv15465470">S4E21 Python 数列与Pi 的探究（下）</a> (3-1)
1. <a title="本节的 python + Math ，带大家来算算数列与用数列来探索 pi。在这单元将体现程式“重复运算”的特性，要使用数列就需要有抽象出" target="_blank" href="https://www.bilibili.com/read/cv15465465">S4E15 Python 数列与Pi 的探究（上）</a> (3-1)
1. <a title="第四季的第二个任务来了，这次主要复习第二季中的黄金螺线与向日葵。这个题目是当初我是在交互型网站 mathigon 看到这课件（可参考这文章" target="_blank" href="https://www.bilibili.com/read/cv15465483">S402 黄金螺线与向日葵：极坐标、序列、曲线</a> (3-1)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15455833">S403 如何让手拉手模型一目了然？</a> (2-28)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15455469">S4E07 你可以几次可以猜到答案</a> (2-28)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15455397">S4E06随机出练习，口算不再是难题！</a> (2-28)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv15455212">S4E04 Python 学习平台 Snakify</a> (2-28)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15455201">S4E01 Python入门：Kesci 平台与 Python 基本语法</a> (2-28)
1. <a title="本周的 GGB 任务要谈日剧 Liar Game S1E07 的一个双背面扑克牌的几率片段。这是部漫画改编的日剧，剧情十分烧脑，天真善良的" target="_blank" href="https://www.bilibili.com/read/cv15439332">S5Q4 诈欺游戏中双背面牌的几率模拟</a> (2-27)
1. <a title="最近这半年我做了大约100个洋葱视频的解题课模版，在这就为大家讲解如何用模版来作交互型的解题课件。以下是完成图，课件分为3部分，可参考下方" target="_blank" href="https://www.bilibili.com/read/cv15439302">S4E17 几何模版：倍长中线</a> (2-27)
1. <a title="在这节要呈现的就是毕氏螺线，主要的结构就是伸缩+旋转。但对于这个重复类似的结构，本教程将介绍 iterationList 来实现动态增加的" target="_blank" href="https://www.bilibili.com/read/cv15439167">S4E22 ItationList 与毕氏螺线</a> (2-27)
1. <a title="以前中学时也只知道 SSA 会对应到两个三角形，而在洋葱视频中对这部分做些深入的探究，其实大有学问。现在通过 Geogebra 的演示，让" target="_blank" href="https://www.bilibili.com/read/cv15439271">S4E16  SSA的探究</a> (2-27)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15439205">S4E23 勾股定理欧氏证明：单滑杆控制多次变形</a> (2-27)
1. <a title="从北京飞到纽约可不是要飞越太平洋，而是要经过北极圈。这可不是因为怕飞机掉到太平洋中，而是因为这是走球面上两点最近的的大圆路径。对于这类立体" target="_blank" href="https://www.bilibili.com/read/cv15429500">S5G4 球上的大圆路径</a> (2-26)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15428891">S4E20 艾宾浩斯错觉</a> (2-26)
1. <a title="PA + PB 的最小值这是经典的将军饮马问题。但除了关心最小值外，当动点在动时，其 PA+PB 的值是如何变化的呢？我们将用 GGB 的" target="_blank" href="https://www.bilibili.com/read/cv15428209">S5R3 动点与函数图像：以将军饮马为例</a> (2-26)
1. <a title="分析图形上面积与动点位置的函数图像关系是中考常见的题型。我喜欢这类问题，主要因这题目体现了函数中对变量的直观感受。那要如何用 GGB 来演" target="_blank" href="https://www.bilibili.com/read/cv15428199">S5R2 动点面积与函数图象</a> (2-26)
1. <a title="旋转变换是中考压轴题常见的题型。最经典是正三角形的情况。给定正三角形内一点 P，其中 P 到三顶点的距离是 3,4,5 。想求正三角形的边" target="_blank" href="https://www.bilibili.com/read/cv15425832">S5R4 旋转全等的探究</a> (2-26)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15413866">S5G1 圆面积的扇形分割</a> (2-25)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv15401493">S5G2 平面截圆锥</a> (2-24)
1. <a title="第五季将推出另一系列的挑战任务，主要是呈现案例，并说明指令。但细部的制作顺序就留给大家去探究。而这次要登场的也是配合 Pi Day 来做一" target="_blank" href="https://www.bilibili.com/read/cv15394977">S5Q1 圆内接与圆外切多边形</a> (2-23)
1. <a title="将一个定长线段放置在两个圆上，当圆杆动起来时，其中点的轨迹竟然会是个爱心！让我们一起探究这个连杆绘制爱心的图形吧.Part1 绘制两个圆说" target="_blank" href="https://www.bilibili.com/read/cv15317358">S13G4 连杆绘制爱心</a> (2-18)
1. <a title="在三角形內一點P如何將三角形分成等面積的三塊？又如何分割為面積比為 a,b,c 的三塊？這節將學習利用分點公式來探究三角形面積的分割。Pa" target="_blank" href="https://www.bilibili.com/read/cv15118623">S0G25 三角形的重心與面積分割</a> (2-4)
1. <a title="今天來介紹一個數字團康遊戲，這個遊戲我們將學習 GGB 的隨機出題功能，並認識 GGB 的運算區。Part1 構造隨機數說明：利用rand" target="_blank" href="https://www.bilibili.com/read/cv15051972">S12G8  Numdle 數字團康遊戲 談GGB運算區</a> (1-29)
1. <a title="核電廠的影響範圍有多遠？今天我們用GGB來度量。Part1調整比例尺說明：利用比例尺，把圖片位置設置為（0，0），（90.5，0）.操作：" target="_blank" href="https://www.bilibili.com/read/cv15032516">XY12 核電廠有多遠</a> (1-28)
1. <a title="1,1,2,3,5,8,13 稱為費氏數列，特性是每一項等於前兩項的和，對於這樣的遞回關係我們可以用試算表來做探究。Part1 等差等比數" target="_blank" href="https://www.bilibili.com/read/cv14927642">S0A6 用試算表探究費氏數列</a> (1-21)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv14807081">XY15 Ponzo Illusion 圖片版</a> (1-12)
1. <a title="對於固定周長之下的四邊形，正方形有最大面積。那如果不限定邊數，正3邊形、正4邊型、正6邊形哪個面積最大呢。這個就是有名的圈地問題。    " target="_blank" href="https://www.bilibili.com/read/cv14792861">S0G24 定周長的正多邊形面積探討</a> (1-11)
1. <a title="当一个长方形的周长为10，长、宽各为多少时，面积有最大值？Part1 繪制長方形觀察面積說明：構造點A、B、C，再利用對稱性D=B+C-A" target="_blank" href="https://www.bilibili.com/read/cv14785736">S0G23  定周長的長方形面積探討</a> (1-10)
1. <a title="平面上三角形的內角和是180度，這次我們就用三角形的平移與旋轉來驗證這個結果。Part1 平移說明：構造△ABC以及滑動條ta、tb，選擇" target="_blank" href="https://www.bilibili.com/read/cv14730795">S0G22  三角形內角和180°</a> (1-6)
1. <a title="冬至，是一年白天最短的一天。那為何會有白天黑夜消長的現象呢？在教材中有個靜態的太陽軌跡說明圖。這其實是個動態的 3D 變換，光靠想象像難度" target="_blank" href="https://www.bilibili.com/read/cv14722673">S8G2 日出方位與影長變化</a> (1-6)
1. <a title="如何找出四邊形內一點到四頂點的最短距離呢？ 關鍵就是兩點之間線段最短。 讓我們用 GGB 來探究這最值問題。Part1 一點到四點的距離和" target="_blank" href="https://www.bilibili.com/read/cv14609155">S0G20 一點到四點的最短距離和</a> (2021-12-28)
1. <a title="(3,8,7) ,(5,8,7) 是夾角為 60 度的三角形，而 (3,5,7) 是夾角 120 度的三角形。除了這組數，還有哪些60度的" target="_blank" href="https://www.bilibili.com/read/cv14577275">S12G6  整數邊的60°三角形</a> (2021-12-26)
1. <a title="小學學過三角形三內角和是 180°。但這是在平面上來看的觀點，如果考慮一個球面三角形時，這時三內角和就會超過 180°。這次就帶大家來探究" target="_blank" href="https://www.bilibili.com/read/cv14568703">S12G7 球面三角形內角和</a> (2021-12-25)
1. <a title="对于最短路径和问题中，有个经典的费马点问题。 要找三角形内一点 P 使其到三顶点的距离和为最小。 在寻找费马点的过程中，利用三角形三边往外" target="_blank" href="https://www.bilibili.com/read/cv14517256">S0G21 費馬點與拿破侖定理</a> (2021-12-21)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv14457911">S0G19 圓錐曲線與離心率</a> (2021-12-17)
1. <a title="這個箭頭錯覺 9 成以上的人會答錯，來測試你的空間方向感吧！在一個正反面都有個垂直箭頭的八邊形，翻面後箭頭朝向哪邊？除了觀察外，你覺得如何" target="_blank" href="https://www.bilibili.com/read/cv14383127">S12G5  箭頭錯覺</a> (2021-12-11)
1. <a title="在 A,B 兩點之間有條小河，該如何造一座橋 CD， 使得 AC+CD+DB 最短。這是升級版的將軍飲馬問題，方法是先平移，再連接，原理是" target="_blank" href="https://www.bilibili.com/read/cv14337983">S0G17 造橋選址問題</a> (2021-12-8)
1. <a title="想像一下，這個階梯顛倒後會如何？拉動滑動條觀察，原本朝上的綠色階梯，旋轉 180 度後，朝向哪邊呢？這次就來完成這個階梯顛倒錯覺。Part" target="_blank" href="https://www.bilibili.com/read/cv14327065">XY14  階梯顛倒錯覺</a> (2021-12-7)
1. <a title="S12G1 已完成蛇形序列，這節再來探究另一種環形序列，搭配旋轉與序列指令就可完成這個圖。要做這些序列圖，挑戰的不是指令而是對圖形的拆解。" target="_blank" href="https://www.bilibili.com/read/cv14303981">S12G2 環形序列</a> (2021-12-6)
1. <a title="序列是觀察數學規律，寫出通式的好練習。這一節就要通過 Geogebra 的雙層序列來寫出這蛇行序列。在這我們將搭配 Reverse 、If" target="_blank" href="https://www.bilibili.com/read/cv14290444">S12G1 蛇行序列</a> (2021-12-5)
1. <a title="已知一個三角形，如何找到其內接三角形中周長最小的三角形。這個三角形又叫施爾瓦茲三角形，這個問題可說是將軍飲馬問題的進階版。通過對稱將折線轉" target="_blank" href="https://www.bilibili.com/read/cv14258632">S0G16 施爾瓦茲三角形（最小內接周長三角形）</a> (2021-12-2)
1. <a title="先前已經用割補法來求平行四邊面積，利用 GGB 的平移操作。這次再用 GGB 來旋轉就可完成這梯形面積公式的推導。任務一 製作梯形說明：利" target="_blank" href="https://www.bilibili.com/read/cv14238268">S0G15 割補法求梯形面積</a> (2021-12-1)
1. <a title="這節通過圖片來認識圖片的定位與旋轉功能，使用這個雨後的雙重錯覺圖，當圖片改變方向後，圖片會從下雨前變為下雨後。旋轉圖片說明：構造圖片的中心" target="_blank" href="https://www.bilibili.com/read/cv14223195">XY13 顛倒圖像錯覺</a> (2021-11-30)
1. <a title="在正立方體內有八個點，選不共面的四個點就可以組成一個三角錐，其中有個方法可以選到六邊都等長的正三角錐。這個錐體的體積佔正六面體的幾倍呢？讓" target="_blank" href="https://www.bilibili.com/read/cv14107854">S0S6 正立方體內的正三角錐</a> (2021-11-22)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv14089877">S0S5 角錐體積公式</a> (2021-11-20)
1. <a title="平行四邊形的面積為底乘以高，它的證明就是利用到割補法來將此圖形拼貼為長方形。利用平移工具就可以實現此操作。任務一 構造平行四邊形說明：利用" target="_blank" href="https://www.bilibili.com/read/cv14008806">S0G14  割補法求平行四邊形面積</a> (2021-11-15)
1. <a title="帕斯卡三角在數學上有很多應用，在機率與多項式的展開都會用到，而其本身也藏著很多數學秘密，這次我們將用 GGB 來實作帕斯卡三角形揭開其中的" target="_blank" href="https://www.bilibili.com/read/cv13999144">S11G8 帕斯卡三角（楊輝三角）</a> (2021-11-14)
1. <a title="從月曆中選一個 4x4 的小方格，接著從中選四個數使得任兩個都不在同一直行或橫列。不論使用者如何選這四個數，我們都可以預測出這四個數字的總" target="_blank" href="https://www.bilibili.com/read/cv13805884">S0A8 月曆預言</a> (2021-10-31)
1. <a title="積分的思想就是將曲線下分割為多個長方形來求面積，在 GGB 中內建 RectangleSum 指令方便我們快速求得此面積，並且可視覺化呈現" target="_blank" href="https://www.bilibili.com/read/cv13718697">S0A7 黎曼和與積分</a> (2021-10-25)
1. <a title="由 40 個人繞成一圈，每間隔兩人淘汰一人，問最後一位被淘汰的為哪一位，這就是有名的約瑟夫斯問題。這次將介紹用 GGB 的列表功能來動態呈" target="_blank" href="https://www.bilibili.com/read/cv13629294">S11G4 約瑟夫斯問題 Josephus Problem</a> (2021-10-18)
1. <a title="Nim 是一个双人游戏，有三堆数 3,4,5 两人轮流从一堆中取任意多颗子，取到最后一个的人获胜。这次就用 GGB 来实作这个游戏介面，并" target="_blank" href="https://www.bilibili.com/read/cv13622447">S11G5 Nim遊戲與二進位</a> (2021-10-17)
1. <a title="在一个任务我们学会将 10 进位转换为 d 进位，但要实践 d 进位转换为 10 进位的挑战就更多了。我们需要使用更多列表的相关指令来协助" target="_blank" href="https://www.bilibili.com/read/cv13429207">S11G3B d進位轉換為10進位</a> (2021-10-3)
1. <a title="電腦系統採用的是 2 進位或 16 進位制，使用 16 進位制時，還會用到 ABCDEF 來表示一個數字。先前我們已經做出了 10 進位轉" target="_blank" href="https://www.bilibili.com/read/cv13424997">S11G3A  十進位轉d進位</a> (2021-10-2)
1. <a title="你想一個1~31 的數字，再挑出包含這些數字的卡片，就可快速猜出你所想的數字。這個數學小魔術的製作原理主要是二進位的應用，通過列表的計算就" target="_blank" href="https://www.bilibili.com/read/cv13415279">S11G2 二進位猜生日卡片</a> (2021-10-1)
1. <a title="平方數有很多規律。例如： 45,55,65 的平方為 2025,3025,4225 ，其中 20=4x5, 30=5x6, 42=6x7 " target="_blank" href="https://www.bilibili.com/read/cv13346402">S11G1 炫彩99平方表</a> (2021-9-26)
1. <a title="達文西最著名的壁畫《最後的晚餐》重現聖經故事耶穌被出賣的前一晚，這幅壁畫也利用透視法的概念來將這三維空間呈現在平面上，而畫面中的消失點就是" target="_blank" href="https://www.bilibili.com/read/cv13248495">XY11 透視法與名畫：達文西《最後的晚餐》</a> (2021-9-19)
1. <a title="在平面上放置幾個中心縮放的長方形，這時就出現立體效果，此時放入兩張大小一樣的圖片不在同一位置，就覺得這兩張圖片大小不一樣，這就是所謂的 P" target="_blank" href="https://www.bilibili.com/read/cv13070062">S10G8B  Ponzo Illusion 庞佐错觉</a> (2021-9-7)
1. <a title="將兩條等長的線段擺在無限延伸的鐵軌上 ，這時發覺兩線段長度不一樣了，這種利用透視產生的錯覺稱為 Ponzo 錯覺。任務一 縮小的平行線段說" target="_blank" href="https://www.bilibili.com/read/cv13070033">S10G8A  Ponzo Illusion 庞佐错觉</a> (2021-9-7)
1. <a title="圓周率為圓周長與直徑的比值，早期對於圓周率的計算就是利用割圓術，在圓的內外做正多邊形，利用多邊形的周長來估計圓周長。今天就用 GGB 展示" target="_blank" href="https://www.bilibili.com/read/cv12807486">S0G14 圓內接多邊形逼近圓周率（基礎版）</a> (2021-8-22)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv12663646">XY10 籃框有多高</a> (2021-8-14)
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv12590543"><b>学用数学 Geogebra 教程汇整 (Lv7)</b></a> (2021-8-10) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201713_ae1e55f6_5631341.png">
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv12548553">S0S4 平面截圓錐</a> (2021-8-8)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv12541583">S0G13 橢圓上一點到兩焦點距離和</a> (2021-8-7)
1. <a title="今天通過拉婓爾的名畫雅典學院來認識 Geogebra ZoomIn 這個指令，讓我們通過滑動條來快速切換畫面的位置.   任務一 設置畫面" target="_blank" href="https://www.bilibili.com/read/cv12490974">S10G6 雅典學院與畫面的縮放</a> (2021-8-4)
1. <a title="本週要探索 Geogebra 的3D功能，構建立體圖形，談到立體圖形就要談柱體與錐體，其中很著名的錐體就是金字塔。讓我們用 Google " target="_blank" href="https://www.bilibili.com/read/cv12468579">S0S3 金字塔與圓周率</a> (2021-8-3)
1. <a title="讓一條線段繞著一個軸轉形成旋轉體，線段平行轉形成軸圓柱、線段頂點位在軸上形成圓錐。若線段傾斜再旋轉就形成單葉雙曲面。很多建築物都有旋轉體的" target="_blank" href="https://www.bilibili.com/read/cv12396582">S0S2 從旋轉體談圓錐、圓柱與單葉雙曲面</a> (2021-7-30)
1. <a title="本週用GGB 認識名畫，欣賞達文西的名作，認識黃金矩形。試著在圖片上建另一個長寬比為 1.618 的長方形來比對一下畫中蒙娜麗莎有哪些符合" target="_blank" href="https://www.bilibili.com/read/cv12389030">XY08 達文西與黃金矩形</a> (2021-7-29)
1. <a title="点击进入查看全文" target="_blank" href="https://www.bilibili.com/read/cv12080307">S10G4 毕达哥拉斯的瓷砖定理</a> (2021-7-9)
1. <a title="我們小時候學的九九乘法表, 印度對於 19x19 內的乘法有個速算法, 例如 12x13= 100+(2+3)x10 + 2x3 = 15" target="_blank" href="https://www.bilibili.com/read/cv11997647">S10G3 印度19×19算法</a> (2021-7-4)
1. <a title="圖中的兩張桌子『看起來』形狀不一樣，但其實是全等的！就讓我們用 GGB 讓圖形動起來檢驗看看吧！ 這個錯覺叫做謝帊德錯覺，主要其實是因為大" target="_blank" href="https://www.bilibili.com/read/cv11892142">S0G11 Shepard 桌子錯覺</a> (2021-6-26)
1. <a title="学用数学推广 GGB 学习已进入第 10 季，累计近 5000 人次的打卡。今年暑假进一步的推广GGB ，希望更多的师生可以共同参与，特此" target="_blank" href="https://www.bilibili.com/read/cv11842056">师生组团打卡 GGB  瓜分奖金 1024</a> (2021-6-23)
1. <a title="618 是黃金比例日，趁此來做一個與黃金比例有關的案例，縮放的正五邊形。 在這案例里我們先做出一層的縮放，接著利用試算表的公式複製，就可快" target="_blank" href="https://www.bilibili.com/read/cv11813586">S10G2 缩放的正五边形</a> (2021-6-21)
1. <a title="在電腦上將物件上色，最常用的就是 RGB 代碼。本節將完成一個三個色環顯示光的三原色混搭的結果。任務一：利用不等式構造圓的內部說明：構造點" target="_blank" href="https://www.bilibili.com/read/cv11698641">S10G1 光的三原色RGB色盤</a> (2021-6-13)
1. <a title="最近與小朋友介紹世界名人，提到高斯是德國人、牛頓是英國人、阿基米德來自於希臘...。提到這些國家的時候，就順便帶學生認識歐洲地圖。但為了讓" target="_blank" href="https://www.bilibili.com/read/cv11511553">S9G8 隨機出題：歐洲地圖</a> (2021-5-31)
1. <a title="根号2的近似值 1.414... 要求得值可近似值可用十分逼近法，但这个计算量偏大，这次就用 Geogebra 来自动执行根号2的估算过程" target="_blank" href="https://www.bilibili.com/read/cv11434306">S9G7 平方根的估算</a> (2021-5-25)
1. <a title="向量是 Geogebra 處理平面幾何變換的基礎數學概念，將向量搭配數值滑桿，就可以做到圖形動態變化的效果。在這節將利用高中平面向量的線性" target="_blank" href="https://www.bilibili.com/read/cv11330754">S9G6 向量的線性組合</a> (2021-5-18)
1. <a title="學用數學將推出新一系列 GS 課程  ：用 Geogebra 來分析解題思路。很多數學幾何題，要理解解析不難，困難的是該如何想到解題方法？" target="_blank" href="https://www.bilibili.com/read/cv11213206">GS01 用 GGB 探究科学班甄选试题：南一中101填充4</a> (2021-5-9)
1. <a title="談到黃金比例，常會提到黃金螺線，並說鸚鵡螺就符合黃金螺線。這次就讓我們用 Geogebra 來驗證看看是否符合黃金螺線。為了繪製黃金螺線，" target="_blank" href="https://www.bilibili.com/read/cv11139187">S9G5 黄金螺线与极坐标方程</a> (2021-5-4)
1. <a title="談到數學與藝術就要來談黃金矩形，這次使用 GGB 的正多邊形與圓弧工具來繪黃金矩形與黃金螺線、並利用數值滑桿來讓圖形能依次出現。同時分享一" target="_blank" href="https://www.bilibili.com/read/cv11025400">S9G4 黃金矩形與黃金比例</a> (2021-4-25)
1. <a title="將正五邊形的對角線相連會連出四種線段長度，將線段由短到長排列，可發覺兩個小的線段和等於大的線段，而其相近的兩個線段長，其長度之比約為 0." target="_blank" href="https://www.bilibili.com/read/cv10969657">S0G10 正五邊形與黃金比例</a> (2021-4-21)
1. <a title="在中小學數學課中，除了代數與幾何，機率統計也是個重要的環節。GGB 內建些統計指令，讓我們可一鍵生成統計圖表。這次以 200 內的因數個數" target="_blank" href="https://www.bilibili.com/read/cv10852939">S0D1 用表格區繪製統計圖：以因數個數為例</a> (2021-4-14)
1. <a title="这次以小学的进退位计数器为例，来展示用脚本按钮来控制进位器的数珠的变化。主要使用 SetValue 与 If 来控制算珠的加减与进退位。除" target="_blank" href="https://www.bilibili.com/read/cv10812152">S9G3 小学进退位计数器</a> (2021-4-12)
1. <a title="這個無窮根式是先前加入一個 manim 群所遇到的入群問題。當時用 Python 寫了個逼近式，但一直不了解這個題目的設計原理。後來再做台" target="_blank" href="https://www.bilibili.com/read/cv10630877">S9G2 用Geogebra 动态展示拉马努金的迭代根式</a> (2021-4-5)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv10577800">XY06 公園有多大？</a> (2021-4-1)
1. <a title="給定三個點如何找出到三頂點等距的點？這個點其實就是所謂的外心，這次將示範用 Geogebra 的工具來為演示找出外接圓的圓心，並認識外心一" target="_blank" href="https://www.bilibili.com/read/cv10470091">S0G9 三角形外心與外接圓的探究</a> (2021-3-25)
1. <a title="这一年在推广 Geogebra 最常被问的问题之一就是 Geogebra v.s. 几何画版该选哪一个？我在2020/7  S6A1 Ge" target="_blank" href="https://www.bilibili.com/read/cv10429120">Geogebra V.S. 几何画版 要选哪一个？</a> (2021-3-23)
1. <a title="新手該如何學 Geogebra ？今天帶大家認識 Geogebra 官網上的學習資源，並介紹學用數學這兩年 100 多個案例的視頻與搭配的" target="_blank" href="https://www.bilibili.com/read/cv10388136">VG09 Geogebra 新手必備學習資源</a> (2021-3-20)
1. <a title="Geogebra 官網有很豐富的學習資源，可觀摩學習很多作品。在官網上創作作品分享也很方便，本篇將介紹大家註冊並登入學用數學官網，並在官網" target="_blank" href="https://www.bilibili.com/read/cv10387308">Geogebra 官網的註冊登入與介紹</a> (2021-3-20)
1. <a title="米開郎基羅的大衛像到底有多高呢？大衛的身形是否符合黃金比例呢？今天就帶大家利用 Geogebra 的測量工具來估算出大衛像的高度，並了解其" target="_blank" href="https://www.bilibili.com/read/cv10321963">XY05 大衛身高與黃金比</a> (2021-3-16)
1. <a title="電影中的數學最常出現的 4 個元素就是：圓周率、黃金比例、質數、機率。趁著這次 314 Pi Day 的到來，學用數學為大家整理一份與 P" target="_blank" href="https://www.bilibili.com/read/cv10273558">影剧中的圆周率</a> (2021-3-14)
1. <a title="在 Pi Day 的到来，来分享一个与 Pi 有关的逼近式 Basel 问题。这问题谈到的就是连续平方数的倒数之和竟然会等于 pi^2/6" target="_blank" href="https://www.bilibili.com/read/cv10261412">S9G1 圆周率的逼近式 (用 Geogebra 探究 Basel 问题)</a> (2021-3-13)
1. <a title="两线是否平行关键在于利用 同位角相等的判别准则。这次我们将利用 GGB 的直线与角度显示三线八角的关系，让我们可动态观察两条线是否平行。步" target="_blank" href="https://www.bilibili.com/read/cv10120649">S0G7 三线八角与平行线</a> (2021-3-4)
1. <a title="先前完成了蚂蚁爬长方体，关键在于长方体的展开。这次的任务要来挑战蚂蚁爬圆柱，关键也在如何对圆柱作展开。其实在于圆如何展开。利用 Geoge" target="_blank" href="https://www.bilibili.com/read/cv10096855">S8G8 蚂蚁爬圆柱</a> (2021-3-3)
1. <a title="布丰投针 3/14 是国际数学日，又称 Pi Day 。上次我们用 GGB 的随机内点来估计 Pi，这次来模拟这个很神奇的 Pi 的估算法" target="_blank" href="https://www.bilibili.com/read/cv10061408">S8G7 布丰投针估计圆周率 Pi</a> (2021-2-28)
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv10004688"><b>学用数学 Geogebra 教程汇整 (Lv1)（2021版）</b></a> (2021-2-25) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201614_ed0ebe14_5631341.png">
1. <a title="在不少程式语言中，都有支援 Turtle 套件，这个套件可让程式新手通过一些回圈的概念来绘制有规律的基本图形。而 在 Gegoebra 也" target="_blank" href="https://www.bilibili.com/read/cv9947902">S8G6 让乌龟动起来</a> (2021-2-22)
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv9909021"><b>学用数学 Geogebra 教程汇整 (Lv6)</b></a> (2021-2-19) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201546_6449d179_5631341.png">
1. <a title="迎接辛丑年的新年，学用数学来分享个「牛转乾坤」的太极阴阳动态图。本次为一个新手任务，只要利用半圆工具，设置颜色、并利用滑动条控制动点的速度" target="_blank" href="https://www.bilibili.com/read/cv9816469">S0G8 牛轉乾坤</a> (2021-2-14)
1. <a title="https://www.bilibili.com/read/cv12590543https://www.bilibili.com/read" target="_blank" href="https://www.bilibili.com/read/cv9737637"><b>学用数学 Geogebra 教程汇整 (Lv5)</b></a> (2021-2-9) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201505_28845de4_5631341.png">
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv9643129"><b>学用数学 Geogebra 教程汇整 (Lv4)</b></a> (2021-2-4) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201431_9e2f2ba3_5631341.png">
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv9625127"><b>学用数学 Geogebra 教程汇整 (Lv3)</b></a> (2021-2-3) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201400_632a4f6d_5631341.png">
1. <a title="这个错觉图是一个很适合小学讨论面积、中学讨论斜率的问题。而要制作这个课件，利用滑动条搭配平移就可完成。任务一：建立四个多边形说明：先指定多" target="_blank" href="https://www.bilibili.com/read/cv9560519">S8G5 消失的小方格</a> (2021-1-31)
1. <a title="最近在与一位小三的小朋友谈三角形的面积，让他来探究了解同底等高的三角形面积皆相同。就随手带他用 Geogebra 实做了这个探究课件。在让" target="_blank" href="https://www.bilibili.com/read/cv9316562">S0G6 同底等高三角形面积</a> (2021-1-17)
1. <a title="最近与学生讨论到二次方程时，遇到了 x^2=x+1 的方程，同时与学生科普了黄金矩形的相关知识，这时也与学生推荐一个 YouTube 上有" target="_blank" href="https://www.bilibili.com/read/cv9217352">S8G4 费氏数列与黄金矩形 (Geogebra)</a> (2021-1-11)
1. <a title="学函数要数形结合，用描点法画函数图象就三步：列表、描点、连线，在课堂教学中配合Geogebra可以更直观的展示，利用slowplot显示函" target="_blank" href="https://www.bilibili.com/read/cv9147293">S8G3 用描点法绘制函数图象</a> (2021-1-5)
1. <a title="在学圆周角时，都会知道一个重要的性质，在圆上同一条弦所对应的圆周角都相等。而这个其中一个特殊情况就是当弦为直径时，所对应的圆周角为直角。这" target="_blank" href="https://www.bilibili.com/read/cv8809840">S8G1 定角对定边的轨迹</a> (2020-12-14)
1. <a title="在数列这单元常见的图形数就是 1+2+3+..._n 这些三角数。若将这个往三维发展时，就是三角垛的问题。其中堆垛的数量就是 1+(1+2" target="_blank" href="https://www.bilibili.com/read/cv8621998">S7G6 序列与三角垛</a> (2020-12-1)
1. <a title="Markov 矩阵主要用于计算状态的变化过程。这一小节将用 GGB 的点列与圆形指令来将迭代 n 次后的结果以图像显示出其长期的稳定状态。" target="_blank" href="https://www.bilibili.com/read/cv8525528">S7G5 矩阵递回变化的视觉化</a> (2020-11-25)
1. <a title="这次主要认识 Geogebra 的矩阵的基本功能，利用逆反矩阵与矩阵列运算来解多元的联立方程组学习要点使用表格区创建矩阵认识矩阵的基本运算" target="_blank" href="https://www.bilibili.com/read/cv8522084">S0A5用矩阵解联立方程组</a> (2020-11-25)
1. <a title="有一些数列题，比如： a{n+1} = (1-an)/(1+an)，给定 a1= k ，求 a2020。像这一类的数列题，一般都是代个几项" target="_blank" href="https://www.bilibili.com/read/cv8350005">S7G4 用图示法显示分形迭代</a> (2020-11-13)
1. <a title="这次主要介绍坐标轴上的不等式区域的显示，并且利用滑动杆来控制目标函数找到其最值。任务1：显示不等式区域任务1 主要通过直接输入不等式，将其" target="_blank" href="https://www.bilibili.com/read/cv8280646">S0A3线性规划与不等式区域</a> (2020-11-8)
1. <a title="这次基础课搭配自家附近的地图，标准出周边的学校或地铁站，利用条件计数 CountIf 的指令，通过调整区域，可以统计出自己所在区域到底有多" target="_blank" href="https://www.bilibili.com/read/cv8215188">XY04 你家附近有几个学校、地铁站</a> (2020-11-4)
1. <a title="这次进阶课来探究蚂蚁走长方体（点O到点D)的最短路线问题，关键就是把点D所在的平面按三种不同的方式展开，再结合勾股定理,利用两点之间线段最" target="_blank" href="https://www.bilibili.com/read/cv8084853">S7G3 蚂蚁走长方体</a> (2020-10-25)
1. <a title="Voronoi图，它是由一组由连接两邻点直线的垂直平分线组成的连续多边形组成。这讲介绍的是Voronoi Graph，这是GGB一个非常强" target="_blank" href="https://www.bilibili.com/read/cv7953951">XY03 最近距离分割 Voronoi Graph</a> (2020-10-14)
1. <a title="这次的进阶课要带大家来探究正方体的11种展开图，利用geogebra的指令来实现，了解到正方体的展开图如何得到的，通过不同切边得到不同的展" target="_blank" href="https://www.bilibili.com/read/cv7807461">S7G2 正立方體的11種展開圖</a> (2020-10-2)
1. <a title="這次的基礎課要帶大家來探究 Geogebra 的 3D 功能中的『展開圖』。 我們將使用 Geogebra 來建立角錐與角柱，並使用『展開" target="_blank" href="https://www.bilibili.com/read/cv7685696">S0G1 锥体与柱体展开图</a> (2020-9-21)
1. <a title="這次主要模擬光的折射，探究光線在經過不同介質的偏離角度。主要使用滑動秆來調整介質參數，並通過參數計算出折射後的位置。探究折射角與折射率的關" target="_blank" href="https://www.bilibili.com/read/cv7622516">S7G1 模擬光的折射</a> (2020-9-16)
1. <a title="光從空氣進入水面時，會有折射的現象發生，在這段想讓我們要利用 Geogebra 的測量工具來度量光的偏折角度，藉此讓大家感受到用 Geog" target="_blank" href="https://www.bilibili.com/read/cv7581061">XY02 驗證光的折射</a> (2020-9-12)
1. <a title="學用數學已經進入新的第七季，這一季要來開啓一個新的主題：專案型的案例匯總。以兩周為一個週期來匯總一系列的 GGB 課件，也方便教學時可直接" target="_blank" href="https://www.bilibili.com/read/cv7542328">GP01 用 GGB 在圖片上進行測量與驗證</a> (2020-9-9)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv7502098">S3E12 半角模型</a> (2020-9-5)
1. <a title="点击进入查看全文>" target="_blank" href="https://www.bilibili.com/read/cv7452266">VG08 Geogebra 2010 官方学习手册导读</a> (2020-9-2)
1. <a title="欲穷千里目更上一层楼真的就可以实现吗？我们想要看到千里之外，到底要站在多高的地方？今天我们利用geogebra来模拟实际问题的可视范围，具" target="_blank" href="https://www.bilibili.com/read/cv7418945">S6G8 欲穷千里目需上几层楼</a> (2020-8-31)
1. <a title="这次学习的是初中常见几何模形之一的半角模型。这个模型可在洋葱视频中看到几个经典的应用，同时半角模型也备受出题者们的青睐，是中考的常考模型之" target="_blank" href="https://www.bilibili.com/read/cv7416254">S3E12 半角模型</a> (2020-8-31)
1. <a title="A: 世界上有 「10」种人，一种是懂二进制，另一种是不懂二进制。B: 不就两种， 哪来的 10 种人？A：你是不懂二进制的那种。进位转换" target="_blank" href="https://www.bilibili.com/read/cv7374155">S0A3 用表格区将10进位转换为d进位</a> (2020-8-28)
1. <a title="三视图是初中的常见知识点，对于初一的小朋友而言仅靠想象是比较抽象的，使用 Geogebra 就比较方便，但教师要如何方便摆放这些立方格呢？" target="_blank" href="https://www.bilibili.com/read/cv7231569">S6G7用拖拉法做立方块的三视图</a> (2020-8-19)
1. <a title="这是唐大仕 Geogebra 课程介绍的第三讲。前两讲的介绍请参考这两个链接 。在第三讲主要谈 Geogebra 动态的功能，最主要强调动" target="_blank" href="https://www.bilibili.com/read/cv7220813">VG05 唐大仕 Geogebra 课程 第三讲</a> (2020-8-18)
1. <a title="在数学课经常谈到数学史，要了解这些数学大咖的先后次序，就是将其生卒年以时间轴来显示，要制作时间轴，最主要将表格区的讯息转化为绘图区上的讯息" target="_blank" href="https://www.bilibili.com/read/cv7158612">S6G6 用 Geogebra 作時間軸</a> (2020-8-13)
1. <a title="古时候是如何找北极星呢？春夏借助北斗七星，秋冬以仙后座为主，用这次我们利用向量工具来寻找北极星。学习技能1、利用 https://stel" target="_blank" href="https://www.bilibili.com/read/cv7050262">XY01 用向量来找北极星</a> (2020-8-5)
1. <a title="这次主要介绍图文并茂的提示展示模式，让 GGB 可以在课堂交互使用。这次的主题是圆内角与圆外角，在显示时还可以针对不同位置做些提示的动态调" target="_blank" href="https://www.bilibili.com/read/cv7023471">S2B8 圆内角与圆外角</a> (2020-8-3)
1. <a title="这部片改编自数学博士作家 Guillermo Martinez 的同名小说。背景是阿根廷大学生 Martin 到英国牛津大学念书，而主要的" target="_blank" href="https://www.bilibili.com/read/cv7004120">影剧中的数理19: 牛津殺人定律｜深度谜案｜The Oxford Murders (2008)</a> (2020-8-2)
1. <a title="本篇接续前篇 《 VG05 唐大仕 Geogebra 课程第一讲》 来谈第二讲的内容介绍。第二讲主要初步介绍些指令的使用。唐老师在课程开始" target="_blank" href="https://www.bilibili.com/read/cv6968555">VG06 唐大仕 Geogebra 课程 第二讲：使用指令</a> (2020-7-30)
1. <a title="最近与小朋友谈到被苹果砸到的牛顿就问小朋友说苹果为何会落下呢？小朋友得意的说我知道是重力！但再接着问小朋友那月亮为何不会被地球吸下来呢呢？" target="_blank" href="https://www.bilibili.com/read/cv6914441">S6G5 卫星绕地的万有引力模拟</a> (2020-7-26)
1. <a title="这次的 Geogebra 基础课 要来谈 Proof without Words 中，经典的平均不等式。在给定了 a, b 两条线段长，如" target="_blank" href="https://www.bilibili.com/read/cv6858378">S0G5 平均不等式（Geogebra 的无字证明）</a> (2020-7-22)
1. <a title="本次缠绕画任务，再次显示数学的规律之美。这个制作过程，是我在 2017 参加官长寿老师的工作坊时所学习的。这个课程设计的很好，先利用表格迭" target="_blank" href="https://www.bilibili.com/read/cv6826809">S6G4 用表格迭代与Geogebra工具作缠绕画</a> (2020-7-19)
1. <a title="这次 Geogebra 学习资源要介绍的是北京大学唐大仕教授在中国大学MOOC开设的《动态几何画板 Geogebra 教学应用》 这课程。" target="_blank" href="https://www.bilibili.com/read/cv6794381">VG05 唐大仕 Geogebra 课程 第一讲</a> (2020-7-16)
1. <a title="在一些证明勾股定理的面积证明中，我们常用图形的平移来证明。这次将旋转与平移变换套用在这个错觉图形上。将四块有一组对角为直角的图形重新拼贴，" target="_blank" href="https://www.bilibili.com/read/cv6747991">S2I1 GGB错觉：挤出一块小空间</a> (2020-7-13)
1. <a title="这次的 Geogebra 教学要来谈轨迹，说起轨迹问题，最经典的就是到两点距离和为定值的椭圆而将和改为差时就是双曲线，进一步考虑相除为定值" target="_blank" href="https://www.bilibili.com/read/cv6709944">S0G4 到两点距离的运算值为定值</a> (2020-7-9)
1. <a title="在 Geogebra 手机 App 中有个有趣的功能就是增强实境 AR ，这可以让数学理论中的几何形体带到真实世界中，你将可从多个角度来观" target="_blank" href="https://www.bilibili.com/read/cv6665556">S6G3 Geogebra 虚拟实境AR 初体验</a> (2020-7-7)
1. <a title="这三年接触 Geogebra 的人越来越多，网上对于 Gegeobra 的资源也越多，但对初学者来说，这个过多的学习资源可能是个困惑。在这" target="_blank" href="https://www.bilibili.com/read/cv6614899">S6A2 Geogebra 新手入门指引</a> (2020-7-2)
1. <a title="趁 6/28 τ  Day 再来谈个与 Pi 有关的 Geogebra 课件。这次要谈的是用 几率来模拟 Pi ，这次的想法主要是最近看到" target="_blank" href="https://www.bilibili.com/read/cv6572231">S6G2 用随机法来估算圆周率Pi</a> (2020-6-28)
1. <a title="广州刘护灵：「学问，就是边学边问，边问边学。」本期的 Geogebgra 学习资源介绍，就是要介绍广州刘护灵老师的公众号「geogebra" target="_blank" href="https://www.bilibili.com/read/cv6538769">VG04 刘护灵《geogebra与数学深度融合》公众号</a> (2020-6-25)
1. <a title="我从 2018/10/24 发起线上共学编程+数学的计划，后来转为以 Geogebra 为主的学用数学。 在 2019/10/24 伴随第" target="_blank" href="https://www.bilibili.com/read/cv6522986"><b>学用数学 Geogebra 教程汇整 (Lv2)</b></a> (2020-6-24) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201302_4895e5a6_5631341.png">
1. <a title="当科学的普世真理的理念与国家利益有冲突时，该如何抉择呢？这次影剧中的数理，想来介绍的就是在 2008 年 BBC  与 HBO 合作的影片" target="_blank" href="https://www.bilibili.com/read/cv6492355">影剧中数理13 爱因斯坦与爱丁顿(2008)</a> (2020-6-21)
1. <a title="在夏至中午时，若在北回归线上立竿，这时可就看不到「立竿见影」的效果。在 2015 年夏至时，我就想要进一步了解探究，当改变北纬 23.5°" target="_blank" href="https://www.bilibili.com/read/cv6480198">S2E3L6 夏至的日照与杆影变化</a> (2020-6-20)
1. <a title="数学的学习虽然不等同于算术，但计算能力对小学来说还是很重要。不只要让小朋友算得快且正确，更重要的是让小朋友对这些可灵活的应用。今天想推荐的" target="_blank" href="https://www.bilibili.com/read/cv6421774">MR01 聪明格：小学算术与逻辑训练游戏</a> (2020-6-14)
1. <a title="这次的 Geogebra 基础课要来实作这个三角形外角和等于一个周角 360° 的动画。学习指引这节定位为基础课 Level  2 的难度" target="_blank" href="https://www.bilibili.com/read/cv6392561">S0G3 三角形的外角和</a> (2020-6-12)
1. <a title="给定一串点列，如何找到合适的拟合曲线呢？在 Geogebra 中已经内建统计数值分析模型，方便我们直接获取数据的一维与二维的统计资料分析。" target="_blank" href="https://www.bilibili.com/read/cv6359310">S6G1L5 点列的回归曲线</a> (2020-6-8)
1. <a title="后续更新作品链接：https://www.bilibili.com/read/cv10004688我从 2018/10/24 发起线上共学" target="_blank" href="https://www.bilibili.com/read/cv6336651"><b>学用数学 Geogebra 教程汇整 (Lv1)（2020版）</b></a> (2020-6-7) <img height="19px" src="https://images.gitee.com/uploads/images/2022/0307/201044_478df2fc_5631341.png">
1. <a title="很多先接触几何画版再接触 GGB 的人都会抱怨 GGB 没有分页模式。但其实要实现分页模式不难，主要就是通过条件显示就可达成这样的效果。而" target="_blank" href="https://www.bilibili.com/read/cv5486670">S5Q2 GGB的分页模版：以余弦定理为例</a> (2020-4-6)
1. <a title="函数可以说是高中数学的基本语言。在小学我们学会用数字来描述事件，到初中用未知数 x, y, z 来描述未知的情况，但高中就以函数来描述“变" target="_blank" href="https://www.bilibili.com/read/cv485263">高中数学：函数</a> (2018-5-16)

# [《死亡密码pi》观影笔记](https://zhuanlan.zhihu.com/p/105294387) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9051056_link)］

<main class="App-main" role="main"><div class="Post-content" data-za-detail-view-path-module="PostItem" data-za-extra-module='{"card":{"content":{"type":"Post","token":"105294387"}}}' data-zop='{"authorName":"雪狼","itemId":105294387,"title":"《死亡密码pi》观影笔记","type":"article"}' data-zop-usertoken='{"userToken":""}'><div class="TitleImage TitleImage--fullScreen" style="background-image:url(https://pic3.zhimg.com/v2-c8936a6dce480540cee6a598deb17b70_r.jpg?source=172ae18b)"></div><article class="Post-Main Post-NormalMain" tabindex="-1"><header class="Post-Header"><div class="Post-Author"><div class="AuthorInfo" itemprop="author" itemscope="" itemtype="http://schema.org/Person"><div class="AuthorInfo"><meta content="雪狼" itemprop="name"/><meta content="https://pic1.zhimg.com/v2-e72b735e205556e6c34b826b887e90bc_l.jpg?source=172ae18b" itemprop="image"/><meta content="https://www.zhihu.com/people/lin-wen-feng-13-89" itemprop="url"/><meta itemprop="zhihu:followerCount"/><span class="UserLink AuthorInfo-avatarWrapper"><div class="Popover"><div aria-expanded="false" aria-haspopup="true" aria-owns="Popover8-content" id="Popover8-toggle"><a class="UserLink-link" data-za-detail-view-element_name="User" href="//www.zhihu.com/people/lin-wen-feng-13-89" target="_blank">雪狼</a> 应无所住而生其心 @ 靖亚资本（SaaS投资） <a class="UserLink-link" data-za-detail-view-element_name="User" href="//www.zhihu.com/people/lin-wen-feng-13-89" target="_blank"><img alt="雪狼" class="Avatar Avatar--round AuthorInfo-avatar" height="38" src="https://images.gitee.com/uploads/images/2022/0307/133729_ba1bde66_5631341.jpeg" srcset="https://pic1.zhimg.com/v2-e72b735e205556e6c34b826b887e90bc_l.jpg?source=172ae18b 2x" width="38"/></a></div></div></div></div></div></div></div><div class="LabelContainer-wrapper"></div><div role="button" tabindex="0"></div></header>
<p></p>
<p><b>电影简介：</b></p>
<p>
犹太天才数学家马克西·米利安·科恩（希恩·格莱特 Sean  Gullette饰）痴迷沉醉于数字的世界，他认为一切自然界的事物都可以用数字解释。他潜心研究，试图透过数字推演出种种生命迹象背后的奥秘和规律。可越是逼近答案，他越是无法忍受致命的头痛。他发现了过去十年来股票市场的混乱波动，其实是由背后的一套数学模式在操纵。与此同时，他的研究引来了华尔街股票市场的炒家们和犹太神秘教派的注意。在介乎真实与虚拟的世界中，马克西正在走向疯狂。</p>

<div class="Post-RichTextContainer"><div class="css-1yuhvjn"><div class="RichText ztext Post-RichText css-hnrfcf" options="[object Object]"><figure data-size="normal"><img class="origin_image zh-lightbox-thumb lazy" data-actualsrc="https://pic2.zhimg.com/v2-94281e43327fafdd162cd17acbd24e8d_b.jpg" data-caption="" data-lazy-status="ok" data-original="https://pic2.zhimg.com/v2-94281e43327fafdd162cd17acbd24e8d_r.jpg" data-rawheight="588" data-rawwidth="1386" data-size="normal" src="https://images.gitee.com/uploads/images/2022/0307/132043_3af88a78_5631341.jpeg" width="1386"/></figure><p data-pid="Xi328CkF"><b>核心收获：</b></p><ul><li data-pid="LCozvurq">犹太天才数学家马克西·米利安·科恩试图透过数字推演出种种生命迹象背后的奥秘和规律。可越是逼近答案，他越是无法忍受致命的头痛。在介乎真实与虚拟的世界中，马克西正在走向疯狂，最后决定用电钻破坏脑子。</li><li data-pid="KAM8CkcR">试图单纯数学去深刻揭示生命或者金融规律的行为，往往会以失常告终。</li><li data-pid="e06x56ff">数学往往能够和行业知识一起帮助解释过去，但很难去预测未来。</li></ul><figure data-size="normal">

<img src="https://images.gitee.com/uploads/images/2022/0307/132019_0a980e5c_5631341.jpeg" width="49%"/> 
 <img class="origin_image zh-lightbox-thumb lazy" data-actualsrc="https://pic3.zhimg.com/v2-e64ba6253fae9a2d8ed793f1d59bc3aa_b.jpg" data-caption="" data-lazy-status="ok" data-original="https://pic3.zhimg.com/v2-e64ba6253fae9a2d8ed793f1d59bc3aa_r.jpg" data-rawheight="1048" data-rawwidth="1772" data-size="normal" src="https://images.gitee.com/uploads/images/2022/0307/132054_74aa6ec5_5631341.jpeg" width="49.6%"/></figure><p></p></div></div></div><div class="ContentItem-time" role="button" tabindex="0">发布于 2020-02-05 23:09</div><div><div class="Sticky RichContent-actions is-fixed is-bottom" style="width: 690px; bottom: 0px; left: 210px;"></div><div class="Sticky--holder" style="position: static; inset: auto auto 0px 0px; display: block; float: none; margin: 0px 0px 10px; height: 54px;"></div></div></article></div></main>

# [geogebra是什么神仙软件啊！！！](https://zhuanlan.zhihu.com/p/142271439) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067494_link)］

- 罗承成 (​​清华大学 力学博士)

  在我寻找好用的数学画图工具时，结识了geogebra，刚一上手就惊为天人，这是谁做的软件啊，太好用了吧！！！分享几个案例吧，你会发觉画图是如此简单。

  <p><img title="1" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230535_c5b07006_5631341.png"> <img title="2" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230546_fb6e0ca1_5631341.png"> <img title="3" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230556_aca078a1_5631341.png"> <img title="4" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230606_4bbf6da0_5631341.png"> <img title="5" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230616_a1fee281_5631341.png"> <img title="6" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230628_98dc037d_5631341.png"> <img title="7" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230638_bf783ef2_5631341.png"> <img title="8" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230647_fa23ff13_5631341.png"> <img title="9" width="30%" src="https://images.gitee.com/uploads/images/2022/0307/230656_b23c5965_5631341.png"></p>

  1. [画出 $f(x)=x^2$ 与 $g(x)=x+a$ 的交点，并求出围成的面积。](https://video.zhihu.com/video/1246431982651392000)
  2. [$f(x)=x^3$ ，画出其导函数g(x)，并求出g(x)从a到b的积分。](https://video.zhihu.com/video/1246437192484007936)
  3. [过平面上任意一点，做圆锥曲线的切线。](https://video.zhihu.com/video/1246871416743497728)
  4. [演示动滑轮和定滑轮](https://video.zhihu.com/video/1247197291934863360)
  5. [画一个雪花(分形)](https://video.zhihu.com/video/1249088576807186432)
  6. [随机画一棵数](https://video.zhihu.com/video/1250538133101019136)
  7. [作三维图(画出两个曲面的交线)](https://video.zhihu.com/video/1250543127196442624)
  8. [做个桌球游戏](https://video.zhihu.com/video/1251251692869718016)
  9. [导入数据，画折线图](https://video.zhihu.com/video/1251260608835784704)

  颤抖吧，人类！！  
  编辑于 2020-06-02 21:16!

---

# [π|Pi电影](https://gitee.com/yuandj/siger/issues/I4WKNZ) ［笔记］

- Pi (1998) Movie
  https://images.gitee.com/uploads/images/2022/0307/104850_6cfa6ca1_5631341.png

  > Pi Plot Synopsis: In NYC's Chinatown, recluse math genius Max (Sean Gullette) believes "everything can be understood in terms of numbers," and he looks for a pattern in the system as he suffers headaches, plays Go with former teacher Sol Robeson (Mark Margolis), and fools around with an advanced computer system he's built in his apartment. Both a Wall Street company and a Hasidic sect take an interest in his work, but he's distracted by blackout attacks, hallucinations, and paranoid delusions..

  - [海报](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9046929_link)

    [海报素材](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9047340_link)

    <p><img width="16%" src="https://images.gitee.com/uploads/images/2022/0307/104843_87277f00_5631341.png"> <img width="14%" src="https://images.gitee.com/uploads/images/2022/0307/104850_6cfa6ca1_5631341.png"> <img width="34%" src="https://images.gitee.com/uploads/images/2022/0307/104955_42bbd375_5631341.jpeg"> <img width="15.75%" src="https://images.gitee.com/uploads/images/2022/0307/105019_9934eae2_5631341.jpeg"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0307/105009_8d95e7bf_5631341.jpeg"> </p>

    [Pi (1998) Movie](https://hellhorror.com/movies/pi-movie-3652.html)

    > Pi 剧情简介：在纽约的唐人街，隐士数学天才马克斯（肖恩·古莱特饰）认为"一切都可以用数字来理解"，当他头痛时，他在系统中寻找一种模式，与前老师索尔·罗伯逊（马克·马戈利斯饰）一起玩围棋，并用他在公寓里建造的先进计算机系统来愚弄。华尔街公司和哈西德教派都对他的工作感兴趣，但他被停电攻击，幻觉和偏执妄想分散了注意力。

- [《死亡密码pi》观影笔记](https://zhuanlan.zhihu.com/p/105294387) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9048229_link)］

  - [知乎转载](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9048316_link)：
    - sigerot17.py
    - sigerot17.html
    - python3 sigerot17.py > tmp.html
  - [python注释掉一段代码](https://blog.csdn.net/weixin_39623244/article/details/109950695)_python将一段程序无效掉的方 … ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9048419_link)］
  - [soup.body.div.main](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9048525_link)
  - [python 如何 soup findall](segmentfault.com/q/1010000014836577/) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9050471_link)］
  - soup.body.div.main.find_all( "div", class_="Post-Sub Post-NormalSub" )[0].[extract()](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9050524_link)
  - [delete all](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9050557_link)
    - delete 'ColumnPageHeader-Wrapper'
    - delete 'Post-topicsAndReviewer'
    - delete 'ContentItem-actions'
    - delete "span", class_="Voters"
  - [delete 'noscript' 重影的图片](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9050718_link)
  - [uploading replacement imgs from just downloaded.](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9050764_link)
  - [replace the imgs of gitee](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9051028_link)
  - [zhihu released.](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9051056_link) [《死亡密码pi》观影笔记](https://zhuanlan.zhihu.com/p/105294387)

- [影剧中的数理](https://space.bilibili.com/32012983/channel/seriesdetail?sid=1334873) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9051585_link)］

  - [find id='page-channal' & 'li's haveing 敏感词](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9052130_link)
  - [get li title only:](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9052536_link)
  - [format list output to md.](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9053096_link) (删除标题中多余的，影视剧中的数学)
  - [增加 img format 输出](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9053952_link) (非法字符问题，出在 bilibili 随机图像文件名．)
  - [replacing those img just uploading.](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9055554_link)
  - [format to table](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9056254_link)
  - [拼接下一页的两个视频](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9057451_link)

- [欢迎加入打卡学习群acchu0331](https://space.bilibili.com/32012983) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9059860_link)］

  - [学用数学　专题合集](https://space.bilibili.com/32012983/article)

  - [1st version effort.](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9062582_link)

    - 15页文章，共174篇，图文摘要，超出65000字节，同时，敏感词
    - 下载图片，并分页测试，格式不好对齐
    - 缩小摘要字数为 39字，依然有敏感词
    - 分页 69　条，查找敏感词，不显示图片
    - 不显示敏感词，不加 title 注释，通过

  - 2nd effort only title:

    - [只标题显示：](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063365_link)
    - [增加注释，19字](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063411_link)
    - [标题去加粗，注释29字](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063450_link)
    - [测试注释长度，29,39,59,均可; 99不行，改为 69](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063632_link)
    - [改用 index 索引开头：](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063669_link)

  - [加 img 19px 结尾，超文件尺寸](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063771_link)

  - [学用数学](https://space.bilibili.com/32012983) - [专栏文章](https://space.bilibili.com/32012983/article) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9063825_link)］

- [geogebra 谁开发的？](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067056_link)

  GeoGebra软件概述. GeoGebra是2001年由美国数学家Markus·Hohenwarter教授发明的可视化数学软件。. “GeoGebra”是由“Geometry（几何）”的前三个字母与“Algebra（代数）”的后五个字母组合而成的，意味着软件拥有同时处理几何绘图和代数计算的能力。. 然而，随着跨国专家团队的不断开发，GeoGebra的功能已不再局限于几何与代数，还扩展到微积分、概率统计、逻辑运算等领域，几乎可以完成从启蒙教育到大学教育中所有的数学教学，已在欧洲及美国荣获数十项相关领域的大奖。. 目前，GeoGebra在全世界成立了150多所研究院，积极开展GeoGebra的本土化开发、研究及培训工作，软件已被翻译成58种语言，为190多个国家的教育工作者所使用。.

  - [GeoGebra 可视化与微积分教学](https://zhuanlan.zhihu.com/p/135221904) - 知乎 - Zhihu 

    - ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067234_link)］
    - ［[手工编辑](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067342_link)］

  - geogebra是什么神仙软件啊！！！ - 知乎
    https://zhuanlan.zhihu.com/p/142271439
    > 2020-6-2 · geogebra是什么神仙软件啊！. ！. ！. 在我寻找好用的数学画图工具时，结识了geogebra，刚一上手就惊为天人，这是谁做的软件啊，太好用了吧！. ！. ！. 分享几个案例吧，你

    - ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067360_link)］

      > 罗承成 (清华大学 力学博士) ... 你会发觉画图是如此简单。

    - [geogebra是什么神仙软件啊！！！](https://zhuanlan.zhihu.com/p/142271439) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067494_link)］

  - [GeoGebra 是什么?](https://www.geogebra.org/about) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067687_link)］

    [GeoGebra 许可证](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067857_link)

- [影剧中的数理 48 死亡密码 Pi (1998)](https://www.bilibili.com/video/BV1qU4y1p7Dq) ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9067977_link)］ [list](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068458_link)

  1. 影劇中的數理 47 死亡密碼 Pi (1998) 00030-00130 [片頭](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=1) 01:06 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068459_link)］
  2. 影劇中的數理 47 死亡密碼 Pi (1998) 00230-00500 [數學是自然的語言](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=2) 02:13 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068463_link)］
  3. 影劇中的數理 47 死亡密碼 Pi (1998) 01110-01320 [圍棋 圓周率 模式](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=3) 02:18 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068467_link)］
  4. 影劇中的數理 47 死亡密碼 Pi (1998) 01436-01710 [希伯來文、費氏數列](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=4) 02:40 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068474_link)］
  5. 影劇中的數理 47 死亡密碼 Pi (1998) 02150-02450 [阿基米德](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=5) 02:57 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068476_link)］
  6. 影劇中的數理 47 死亡密碼 Pi (1998) 03010-03300 [數字占卜](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=6) 02:53 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068478_link)/[2](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068502_link)］
  7. 影劇中的數理 47 死亡密碼 Pi (1998) 04255-04550 [畢達哥拉斯、黃金比例](https://www.bilibili.com/video/BV1qU4y1p7Dq?p=7) 02:53 ［[笔记](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068479_link)］

  - [2.3026](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068518_link)

    - [CUBISM](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068537_link) ([1908](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068528_link)-[1914](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068535_link)) [立体主义](https://gitee.com/yuandj/siger/issues/I4WKNZ#note_9068536_link)

      <p><img width="199px" src="https://images.gitee.com/uploads/images/2022/0308/073304_cb9d43cf_5631341.jpeg"> <img width="199px" src="https://images.gitee.com/uploads/images/2022/0308/071809_76115a12_5631341.png"></p>
