<p><img src="https://images.gitee.com/uploads/images/2022/0114/032757_950fc5ac_5631341.jpeg" height="999px" title="CANSAT esa"></p>

> 驾轻就熟，又一册还愿帖成刊 :pray: 就在两天前《[I 爱 CAN CHANGE THE WORLD](第2期%20STEM%20in%20ShangHai.md)》的 [STEM](STEM/) 主题成刊，昨天《[CODER DOJO 的开源社区实践](第12期%20开源道场%20DOJO%20CHINA%20的构建.md)》成刊，还是 [STEM](STEM/)，今天就是《<a href="第5期%20CANSAT%20我的航天梦.md" title="第5期 CANSAT 我的航天梦">CANSAT ESA</a>》啦。火焰棋实验室的起点 火焰棋工作室 在《[I爱 CAN](第2期%20STEM%20in%20ShangHai.md)》中提及，今天的主角则是同日见证人 “空天创客” 的耿老师，他也是火焰棋实验室的老朋友。本期主题线索就来自 他发起的 [CANSAT 中国](https://gitee.com/yuandj/siger/issues/I3YEOI)。“航天梦，中国梦” 映入眼帘的时候，一份油然而生的自豪感，这才是 “真星辰，行动派”。

> **[CanSat® 卫星设计创新挑战开始报名啦！](https://gitee.com/yuandj/siger/issues/I3YEOI)** <img src="https://images.gitee.com/uploads/images/2021/0629/203021_6055a736_5631341.png" height="29px"> 
  - 面向亚太地区9-12年级青少年的国际性赛事。
  - 目前全球已有近40个国家的青少年在参与CanSat®活动，每年NASA及ESA均会举办面向青少年的CanSat®挑战。
  - 项目设计严谨，挑战性强：
    - 基础知识学习（电子电路、卫星设计、卫星测控等）
    - 工程实践（制作小卫星）主要子系统（星务、通信、电源、载荷）
    - 测试及发射，科研报告撰写

> 这些都深深地吸引了我，俺们上大学那会儿还没有电脑呢，无线电就是科技的最前沿，计算机通信都是新学科，不像现在的娃娃，生在最好的时代，武装到牙齿的科技装备，全栈赋能的学习环境，各个都能成为超人，只要你想。带娃一起玩起来，就是这期主题的源动力。“ _他是科技迷，打着给孩子买礼物的理由！_ ” —— 这是一位妈妈吐槽自己的老公赞助了火焰棋实验室的第一个作品 “云棋盘” 后告诉我的。如今 它早就是 石榴派的标配啦。

不卖关子了，看向这期封面。它是 ESA CANSAT 2021 年的 A2 海报打底的，原本想直接用，发现信息量太少，按图索骥一通研究，就有了这期的主体，[STEM UK 的 CANSAT 教师培训](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292428_link)。首先，三个老铁齐上阵，在第一期星辰大海 [MARS](第9期：我们的未来是星辰大海！.md) 时就时老面孔（3个LOGO），LAUNCH UK 的 SLOGAN 是原本的主题和大背景。[ESERO CANSAT 官方主页](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292449_link)的[邀请文](https://www.stem.org.uk/sites/default/files/pages/downloads/LaunchUK%20-%20A2%20Poster.pdf)是主题：

- **Would you like to launch your own space project?** 
  > The UK CanSat competition offers students the chance to work on a small-scale space project, creating and launching their own simulation of a satellite. It runs every year for students aged 14+.
  - Find out more information here: **www.stem.org.uk/esero/cansat** 

-  **14+ 模拟卫星 太空项目 感兴趣吗？** 就是我自问自答的标题啦。

- 主图沿用了 ESERO 主页的主图，小彩蛋是其 LOGO 的核心背景方框，两组截图
  - [ESERO 主页的](https://www.stem.org.uk/esero/cansat) [宣传视频](https://fast.wistia.net/embed/iframe/0m27dhi3vl) 16 张截图
  - [一个 Hackaday.io 上的 CanSat 真实项目](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292306_link)，全文档。

最后，遮挡的 LAUNCH UK 的项目地点，只能待到同学们能远征英伦时再用了，先打底学习吧。:pray:

([这是我给耿老师的礼物，也是耿老师给我带来的礼物](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8299645_link) :pray: )

https://www.stem.org.uk/esero/cansat

![输入图片说明](https://images.gitee.com/uploads/images/2022/0114/001943_ca648aa0_5631341.png "屏幕截图.png")

# CanSat

![输入图片说明](https://images.gitee.com/uploads/images/2022/0114/002005_ffc21dcb_5631341.png "屏幕截图.png")

 **Registrations for the 2021-22 UK CanSat Competition have now closed. Registrations for the 2022-23 competition will open in June 2022.** 

The CanSat competition provides students with the opportunity to have practical experience working on a small-scale space project.

Aimed at school and college students over the age of 14, CanSat is a European Space Agency competition.

### What is a CanSat?

A CanSat is a simulation of a real satellite, integrated within the volume and shape of a soft drink can. The challenge for students is to fit all the major subsystems found in a satellite, such as power, sensors and a communication system, into this minimal volume.

<p>
<img src="https://images.gitee.com/uploads/images/2022/0114/113350_d8a0b6de_5631341.jpeg" height="99px" title="cansat01.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113408_14bf7c9b_5631341.jpeg" height="99px" title="cansat02.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113420_2ad666a5_5631341.jpeg" height="99px" title="cansat03.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113431_3f7796f0_5631341.jpeg" height="99px" title="cansat04.jpg"><br><img src="https://images.gitee.com/uploads/images/2022/0114/113441_01811120_5631341.jpeg" height="99px" title="cansat05.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113452_39b0ab9f_5631341.jpeg" height="99px" title="cansat06.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113502_a1c57e23_5631341.jpeg" height="99px" title="cansat07.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113512_3845433d_5631341.jpeg" height="99px" title="cansat08.jpg"></p>

[`<iframe allowtransparency="true" title="Wistia video player" allowFullscreen frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" src="https://fast.wistia.net/embed/iframe/0m27dhi3vl" width="400" height="225"></iframe>`](https://fast.wistia.net/embed/iframe/0m27dhi3vl)

<p><img src="https://images.gitee.com/uploads/images/2022/0114/113522_2d95611b_5631341.jpeg" height="99px" title="cansat09.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113531_2f3e82df_5631341.jpeg" height="99px" title="cansat10.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113540_3bc3cdcf_5631341.jpeg" height="99px" title="cansat11.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113550_239baa79_5631341.jpeg" height="99px" title="cansat12.jpg"><br><img src="https://images.gitee.com/uploads/images/2022/0114/113601_7419e7b0_5631341.jpeg" height="99px" title="cansat13.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113610_155e1a0a_5631341.jpeg" height="99px" title="cansat14.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113620_9637e25e_5631341.jpeg" height="99px" title="cansat15.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/113633_110d2e0f_5631341.jpeg" height="99px" title="cansat16.jpg"></p>

### The CanSat challenge
There are three main challenges for students competing in the CanSat competition:

1. To fit all major subsystems found in a satellite, including power, sensors and communications, into the volume and shape of a soft drink can. 
2. To provide a parachute to ensure the can survives the landing.
3. To carry out scientific experiments and transmit in-flight data to an Earth-based computer. 

### What are the benefits of taking part?
The CanSat competition offers a unique opportunity for students to have their first practical experience of a real space project. They are responsible for all aspects: selecting the mission objectives, designing the CanSat, integrating the components, testing, preparing for launch and then analysing the data. 

ESERO-UK organises an annual UK CanSat competition, the winner of which will be invited to compete in the European CanSat competition. 

### Who can apply?
The competition is open to teams from any school or college in the UK. Team members must be over the age of 14 at the time of the competition. 

### What support will you get?
Each team's teacher will be provided with a kit and an introduction course from ESERO-UK. There are also a number of free online resources that can be used to support teams entering the competition. 

Teams can request a STEM Ambassador, who can support the design and build process. 

When applying, please read our guidelines carefully.  The deadline for expressions of interest for the 2021-22 competition is 01 October 2021.

### Download a free UK spaceflight poster
Help spread the word that, very soon,  payloads will be launching to space from UK spaceports! We worked with CanSat competition funders LaunchUK to create this poster, which publicises the UK’s planned spaceflight programme. 

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0114/002552_3ed1650f_5631341.png "cansat poster image.png")](https://www.stem.org.uk/sites/default/files/pages/downloads/LaunchUK%20-%20A2%20Poster.pdf)

[get involved](https://myscience.onlinesurveys.ac.uk/cansat-2021-22-expression-of-interest)  
CanSat 2021-22 Expression of Interest is closed

![输入图片说明](https://images.gitee.com/uploads/images/2022/0114/001725_5e941721_5631341.png "屏幕截图.png")

# [UK CanSat competition teacher workshop SY200](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292428_link)
UK CanSat Competition teacher workshop. Learn how to construct and code a CanSat kit and take part in this year's competition.

### Summary
Before attending the workshop teachers must have entered a team into the competition here: www.stem.org.uk/esero/cansat

The UK CanSat competition provides students with the opportunity to have practical experience working on a small-scale space project. Aimed at school and college students over the age of 14, CanSat is a European Space Agency competition.

The CanSat competition offers a unique opportunity for students to have their first practical experience of a real space project. They are responsible for all aspects: selecting the mission objectives, designing the CanSat, integrating the components, testing, preparing for launch and then analysing the data. 

ESERO-UK organises an annual UK CanSat competition, the winner of which will be invited to compete in the European CanSat competition. The competition is open to teams from any school or college in the UK. Team members must be over the age of 14 at the time of the competition. 

This workshop is aimed at teachers who have entered a team into the competition. The day will include an overview of the competition and teachers will be able to build and code example CanSat kits.  All teachers new to CanSat must take part in introductory CPD to enable teams to participate. During the workshop teachers will be provided with the basic components of the CanSat to work on.

### Outcomes
Participants will be able to:

- understand the requirements for the competition
- integrate and test electronic components of the CanSat kit
- write basic programs in Arduino or Python to record data and communicate via radio

# [CanSat - Exchangeable Payloads Arduino And Edison](https://hackaday.io/project/7604-cansat-exchangeable-payloads-arduino-and-edison) 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292306_link)】

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000844_536796ac_5631341.jpeg" width="369px" title="8461831441732674694.jpg"></p>

You can Exchange the payload and the fly computer,

### DESCRIPTION
This PicoSatellite is an Open source model thats allows you to exchange the payload without any change in the main program of the fly computer.

### DETAILS
I have two payloads:

PL-01: Measures C02, Humedity and temperature

PL-02: Measures the particles in the air (smog, dust and other solids in PMM).

### COMPONENTS

| numbers | | components |
|---|---|---|
| 1 | × |  **Headers** <br>2.54mm male pin header |
| 2 | × |  **2mm 10pin Xbee Socket** <br>Xbee Socket |
| 1 | × |  **Socket 2.54mm** <br>Standart socket for headers |
| 1 | × |  **103 ceramic capacitor** <br>.01uF ceramic |
| 8 | × |  **7 pin molex conector** <br>Male and female |
| 1 | × |  **MicroSwitch** <br>For PCB |
| 1 | × |  **Jumper** <br>For 2.54mm header |
| 1 | × |  **Arduino pro mini 3.3v 8mhz** <br>An Arduino |
| 1 | × |  **GY-80 IMU** <br>10DOF |
| 1 | × |  **DC-DC Boost** <br>lm2577 |
| 1 | × |  **Regulator 3.3 AMS1117** <br>A linear regulator |
| 1 | × |  **Lipo Battery Charger** <br>TLIP1330 |
| 1 | × |  **Lipo Battery** <br>1200mAh 3.7V 1C |
| 1 | × |  **GPS** <br>GP-635T |
| 1 | × |  **Xbee Pro s2b** <br>wired antenna |
| 1 | × |  **Xbee Xplorer USB** <br>Xbee xplorer |

### PROJECT LOGS  Collapse
- Version 1.2  
Enoc • 09/08/2015 at 17:37 • 0 comments  
In this version the picosatellite allows you to exchange payloads without reprogramming the fly computer.

## BUILD INSTRUCTIONS

### Step 1
Step 1: PCB´s

This Pico-Satellite have 4 subsystems:

1. Electrical Power Subsystem (EPS)
1. Command and data-handling Subsystem (C&DH or Fly Computer)
1. Telemetry and Command Subsytem (T&C)
1. Payload ( PL-01 and PL-02)

Each subsystem have their own PCB board, so we have two options:

1. Manufacture our own PCB´s using "PDF makes files"
1. Pay a professional for do the PCB using the Gerber Files.

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235425_35cbd9ed_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/235450_499b5b19_5631341.jpeg" width="99px"></p>

The PDF makes files: [Here](https://www.dropbox.com/sh/rqfxheem7icruep/AAA7I3a9sc2vJdf3qL2jNEKya?dl=0)

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235522_1e2f13df_5631341.png" height="99px"></p>

The gerber files: [Here](https://www.dropbox.com/sh/09kzi3jzj4mkl26/AADIar7aN9XpzY1J7knxBAL2a?dl=0)

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235553_d867ba3d_5631341.png" height="99px"></p>

### Step 2

Step 2: EPS

Materials (click for image):

1. PCB EPS (1).
1. JST battery receptacle (1).
1. Molex connector 7 pin, male(1). Really important... 7 PIN
1. Molex connector 2 pin, male (1).
1. Switch one pole two ways(2).
1. Jumper ( for use only one switch).
1. Ceramic Capacitor 103 (1)
1. Step Up LM2577 Module (1)
1. Lipo Battery 1200mAh 3.7v 1C JST connector (1)
1. TP4056 Battery charger
1. AMS1117-3-3 DC DC Step Down

This is the view of the EPS welded.

The schematics are available: [here](https://www.dropbox.com/s/33gqzsp2tgeo4mc/EPS.jpg?dl=0)

1.- PCB home made

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235739_6fc4cc6e_5631341.jpeg" height="129px"> <img src="https://images.gitee.com/uploads/images/2022/0113/235750_92c13644_5631341.jpeg" height="129px"></p>

2.- PCB "pro"

Top View

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235855_0e32d9b8_5631341.jpeg" height="129px"> <img src="https://images.gitee.com/uploads/images/2022/0113/235907_f9a21722_5631341.jpeg" height="129px"></p>

Bottom

<p><img src="https://images.gitee.com/uploads/images/2022/0113/235954_8e822797_5631341.jpeg" height="129px"> <img src="https://images.gitee.com/uploads/images/2022/0114/000005_a82a9cdb_5631341.jpeg" height="129px"></p>

Its really important the orientation of each component, even the molex conector (BUSS). The conector of the baterry is JST.

### Step 3
Step 3: C&DH (Fly Computer)

The C&DH subsystem have 2 variants:

1. - The Arduino pro Mini 3.3 8 Mhz Atmel328 based fly computer.

2. - The Intel Edison based fly computer.

The first one is finished and proved, work well but have a really low power of processing so, we cant use all the functions of the on board sensors,

The second one provide us a really powerful processor and access to all on board sensors functions, but i'm still working in the development on the required libraries. Also requires an advanced welding skills, because the components are SMT.

So here is the Arduino based fly computer. (Code & Schematics)

Materials(click for image):

1. Arduino Pro mini 3.3v 8Mhz atmega328 (1)
1. IMU GY-80(1)
1. 10k 1/4watt resistor (1).
1. Push button (1)
1. 2.54mm angle 90 header (6)
1. 2.54mm header(2)
1. Molex connector 7 pin, male(1). Really important... 7 PIN
1. FTDI 3.3v Cable

The schematics are available: here

- Home Made PCB

Top & Bottom

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000128_ab31c80b_5631341.jpeg" height="129px"> <img src="https://images.gitee.com/uploads/images/2022/0114/000157_e622d9d0_5631341.jpeg" height="129px"></p>

- PCB "Pro"

Top View

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000254_cbcc0545_5631341.jpeg" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/000306_aebe45e6_5631341.jpeg" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/000318_92892c20_5631341.jpeg" width="99px"></p>

Bottom View

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000458_26098f8d_5631341.jpeg" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/000510_36953d39_5631341.jpeg" width="99px"></p>

For test the C&DH subsystem, you can Scan I2C directions with [this program](https://www.dropbox.com/sh/zrxb9mwljyq7gz4/AAAlSN635jeSa2W6Mr1Du9Dga?dl=0) using the FTDI cable connected as shown in this picture

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000643_41056ead_5631341.jpeg" width="99px"></p>

And the serial monitor of the Arduino IDE will show this:

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000651_984fba33_5631341.png" height="99px"></p>

### Step 4
Step 4.- Telemetry and Command Subsytem (T&C)

### Step 5
Step 5.- Payload (PL-01)

### Step 6
Step 6.- Payload (PL-02)

### Step 7
Step 7.- Buss (Molex)

### Step 8
Step 8.- Integration And Testing.

### Step 9
Step 9.- Exchanging Payloads.

# CANSAT 彩蛋

### 中国 CANSAT

- [空天创客 “航天梦，中国梦” CanSat®卫星设计创新挑战](https://mp.weixin.qq.com/s?__biz=MzIwNTg0Mzc4MQ==&mid=2247496973&idx=1&sn=59349108fdc3d23a04875580c20645db)【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI)】
- 君学书院：[教培寒冬下，空天创客航天教育助力机构打好翻身仗！](https://mp.weixin.qq.com/s?__biz=MzIwNTg0Mzc4MQ==&mid=2247505373&idx=1&sn=57e864afde1716e9d0d941edc91c395c) 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8291991_link)】
- [2022 “青少年玩卫星” 创新卫星设计冬令营招生啦！](https://mp.weixin.qq.com/s?__biz=MzIwNTg0Mzc4MQ==&mid=2247505373&idx=1&sn=57e864afde1716e9d0d941edc91c395c) 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292053_link)】
- [2019年中国首届CanSat卫星设计大赛——最值得孩子参加的赛事](https://zhuanlan.zhihu.com/p/72364379) 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292171_link)】
- [2021CanSat卫星设计创新挑战圆满落幕](https://www.ahjcg.cn/finance/2021/0720/53204.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8303856_link)】

<p><img src="https://images.gitee.com/uploads/images/2021/0629/203200_7ace466d_5631341.gif" height="99px"> <img src="https://images.gitee.com/uploads/images/2021/0629/211110_b9111cda_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2021/0629/211535_d81000f5_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/160632_b6d49582_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/160541_4b23748d_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225932_c66aad56_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225938_d09dfd2b_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225946_9a6e3667_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225954_4d313cb9_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/230001_f41c5438_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/230010_536045e0_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/230139_d3e1423b_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231909_f85589ad_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/224924_5fa3b837_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225752_0eb1a5a9_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231859_b460812b_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/225031_54c4d692_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231917_e367fc7a_5631341.png" height="99px"></p>

### bing [`CANSAT`](https://cn.bing.com/search?q=CANSAT&form=QBLH&sp=-1&pq=cansat&sc=8-6&qs=n&sk=&cvid=FBA8F8A803C44AC7AAACAD5E96A9D312) [他山之石](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292148_link)

- CanSat  
  https://cansat.esa.int
  > CanSat is an initiative of the European Space Agency that challenges students to build a mini satellite, integrated within the volume and shape of a soft drink can. Thousands of students

    - About CanSat

      > Do you want to know what it is like to take part in a real space project? Work together as a team to imagine, build and launch a CanSat!

      > CanSat offers a unique opportunity for students to have a first practical experience of a real space project. They are responsible for all aspects: imagining the CanSat, selecting its mission, integrating the components, testing, preparing for launch and then analysing the data.

    - Photo gallery

      ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232928_78368fad_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232828_09dd18e4_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232838_0ed560fe_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232846_c71ceafc_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232859_d228876d_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232914_470df597_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232941_5864cb3d_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232950_f65ef8d7_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/232958_54723f87_5631341.png "屏幕截图.png")

- ESA - CanSat 【[笔记](https://gitee.com/yuandj/siger/issues/I3YEOI#note_8292164_link)】  
  https://www.esa.int/Education/CanSat
  > 2021-9-20 · CanSat Calendar 2020-2021 Competition guidelines CADSat website European CanSat Competition website Educational resources Cansat resources Past competitions 2020

    <p><img src="https://images.gitee.com/uploads/images/2022/0113/231519_3946fedc_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231537_d85654ea_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231556_e20f5302_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231611_c06f8bc5_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231638_fe26dd54_5631341.png" width="99px"> <img src="https://images.gitee.com/uploads/images/2022/0113/231652_87576575_5631341.png" width="99px"></p>

- WIKIPEIDA CanSat ![输入图片说明](https://images.gitee.com/uploads/images/2022/0113/233141_f0979303_5631341.png "屏幕截图.png")
  > A CanSat is a type of sounding rocket payload used to teach space technology. It is similar to the technology used in miniaturized satellites. No CanSat has ever left the atmosphere, nor orbited the earth.

<p><img src="https://images.gitee.com/uploads/images/2022/0114/000938_19a82c55_5631341.jpeg" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0114/001200_782b6c2a_5631341.jpeg" height="99px"  title="CanSat_Start.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/001347_38037891_5631341.jpeg" height="99px"  title="Electronics_inside_a_CanSat.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/001702_b606799e_5631341.jpeg" height="99px"  title="Team_GSat_Portugal_track_their_CanSat_after_launch.jpg"> <img src="https://images.gitee.com/uploads/images/2022/0114/001714_17c1c80d_5631341.jpeg" height="99px"  title="Students_preparing_their_CanSat_for_launch_pillars.jpg"></p>