# 石榴派 “一生一芯” 寒假集训，开锣喽！

[!40 RISC-V联盟2021年会观感](https://gitee.com/flame-ai/siger/pulls/40) 来自 @[Lisa Zhang](https://gitee.com/lisazyt) 创建于 2022-01-24 20:56 的 PR 正式拉开了 [石榴派 “一生一芯” 寒假集训](https://gitee.com/xiaoxu-tongxue/siger/issues/I4RDZV) 的大幕，一个旨在向中学生科普科技最前沿的计划，在[前辈们](../../RISC-V/baoyungang%2010%20years%20diffrent.md)的[关怀](https://images.gitee.com/uploads/images/2021/1027/040645_d9fe3307_5631341.png)下，终于迈出了第一步。也恭喜 @[Lisa Zhang](https://gitee.com/lisazyt) 第一个脚印是你的，:D 多少尺码的鞋呢？一个石榴派。:pray: 

今天正式拉起 sig 组，你将和其他同学共同耕耘，以期假期结束时，我们能出一期 “一生一芯” 为主题的学习笔记，为我国科技人才队伍的建设尽一份绵薄之力，也期待同学们中能诞生出投身我国科技基础设施建设的排头兵，未来的科技之星就是你们啦！怀着激动的心情，我将昨晚接受 PR 时的笔记抄录如下：

> @[lisazyt](https://gitee.com/lisazyt) LISA 很高兴，在两个平行空间，我们一同观看了 [RISC-V联盟2021年会](../../RISC-V/Ni%20Guang%20Nan.md#%E4%B8%AD%E5%9B%BD%E5%BC%80%E6%94%BE%E6%8C%87%E4%BB%A4%E7%94%9F%E6%80%81risc-v%E8%81%94%E7%9B%9F2021%E5%B9%B4%E4%BC%9A) 除了《[痴心不改，大国匠“芯”,致敬倪光南院士!](../../RISC-V/Ni%20Guang%20Nan.md#%E7%97%B4%E5%BF%83%E4%B8%8D%E6%94%B9%E5%A4%A7%E5%9B%BD%E5%8C%A0%E8%8A%AF%E8%87%B4%E6%95%AC%E5%80%AA%E5%85%89%E5%8D%97%E9%99%A2%E5%A3%AB)》，[<img src="https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png" height="19px" title="RISC-V联盟2021年会">](https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png) 这幅 “[吉祥数独](https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png)” 也是为年会准备的贺礼，是年会开始前，用石榴派开发板运算出来的。想着送给年会，由包云岗老师代收，既然你来了，就由你代表RV新势力，新青年，收下这份新年礼物吧 :pray: 这是全体火种志愿者聚合的 [99 个祝福](https://gitee.com/blesschess/luckystar/issues/I4D7DW#note_8441412_link)。

> - 你可以抽空，完成这组吉祥数独，可以掐个表，计下时间，这也是社区火种队，志愿者入伙的岗前测试。
> - 用老师给的更多线索，可以写一份学习计划，还可以谈谈吉祥数独和你平日里玩的数独有那些不同？

> 很高兴，我们的点亮星光计划，正是开始了，感谢你的 PR，它就像两个星系的首次交汇，它必将成为 RV 星光岛上摧残的一幕，载入[宇宙](../../%E7%AC%AC8%E6%9C%9F%20%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7II%20-%20%E5%AE%87%E5%AE%99.md)星河的历史，也祝愿你成为其中，最摧残的一颗 [恒星](../../Tools/GitSTAR.md)！:pray:

### 学习笔记 

- @[lisazyt](https://gitee.com/lisazyt) 2022-01-24 20:56 提交 [RISC-V联盟2021年会观感.md](RISC-V联盟2021年会观感.md) 献礼 [RISC-V联盟2021年会](../../RISC-V/Ni%20Guang%20Nan.md#%E4%B8%AD%E5%9B%BD%E5%BC%80%E6%94%BE%E6%8C%87%E4%BB%A4%E7%94%9F%E6%80%81risc-v%E8%81%94%E7%9B%9F2021%E5%B9%B4%E4%BC%9A)
- @[yuandj](https://gitee.com/yuandj) 2022-01-25 17:29 创建 一生一芯 sig 组，[石榴派 “一生一芯” 寒假集训](https://gitee.com/xiaoxu-tongxue/siger/issues/I4RDZV) 正式开启 :pray:
- @[alan段](https://gitee.com/duan-haotian) [2022-02-13 15:08](https://gitee.com/flame-ai/siger/pulls/45) 提交 [插上RISC-V的翅膀，迎上软硬件齐飞的新时代.md](插上RISC-V的翅膀，迎上软硬件齐飞的新时代.md) 迎接 第四期一生一芯 的开启。
- @[yuandj](https://gitee.com/yuandj) 2022-02-18 12:03:01 正式提交 [RISC-V Luckies v0.1.69](./RISC-V%20Luckies%20v0.1.69.md) 给 [一生一芯](https://oscpu.github.io/ysyx/) 项目组。

