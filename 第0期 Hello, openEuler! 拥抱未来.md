#[I35C2C](https://gitee.com/flame-ai/siger/issues/I35C2C) [【创刊】Hello, openEuler! - 拥抱未来](https://gitee.com/flame-ai/siger/issues/I35C2C) 分享了SIGer 第0期的封面创作过程，做为2021新年巨献，借着对openEuler社区的感谢，寄语新年新世纪的开启。引用寄语如下，自此开启SIGer新篇，Hello openEuler将作为SIGer编委会的一个子项长期运行。

> 《[Hello, openEuler!](https://gitee.com/flame-ai/hello-openEuler/wikis/Hello,%20OpenEuler!)》
> 落笔于 2021.2.3 立春之时，发愿感谢：
> 祝愿 openEuler 社区万象更新！祝愿中国开源走向世界，成为百花齐放中最靓的一朵！
> 袁德俊，新春褀福，天下平安！为开放协作的宇宙星河中，贡献一颗凡星 :pray:

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0205/005940_d9a7c2b3_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/hello-openEuler/wikis/Hello,%20OpenEuler!)