[开始构思封面啦](https://gitee.com/yuandj/siger/issues/I3CST6#note_8273242_link)。既然有了 `<img src="" width="px">` 的利器，准备工作就可以全部在线啦。

1. <img src="https://images.gitee.com/uploads/images/2022/0108/220219_e914f71f_5631341.png" height="19px"> 典型的家长志愿者，导师观察团模式，三段式封面设计，由此而来。
1. <img src="https://images.gitee.com/uploads/images/2022/0113/023744_357e5d40_5631341.png" height="19px"> 忍者小屁孩的背影，很有代表性，面向 K12 就是要争当冠军吗？好好找找。
1. <img src="https://images.gitee.com/uploads/images/2022/0109/011155_977803b1_5631341.png" height="19px"> CHAMPIONSHIP HANDBOOK 是学习的宝典，原本要做封面的，大图没有找到。
   - 一看到黄色奖杯，我首先想到的是 SkoleSKAK 的 LEGO 奖杯 <img src="https://images.gitee.com/uploads/images/2022/0113/030725_389976c6_5631341.png" height="19px"> （很久前的图片没找到，找个替代的表示下）
   - LEGO 的 巨 IP 就不用说了，奥斯卡的小金人都成了黄色的 <img src="https://images.gitee.com/uploads/images/2022/0113/030844_c402370b_5631341.png" height="19px">
   - 说到丹麦可是袁老师的火种源头，话匣子先关上，树莓派新一期里有和乐高颗粒结合的案例 <img src="https://images.gitee.com/uploads/images/2022/0113/032503_1b6feb15_5631341.jpeg" height="19px"> :sunglasses: 
   - 玩中学，DK 可是极致的很了，早期的火种队支持者，也是模仿了 LEGO 的颗粒的磁吸的 ARDUINO 这里就不点名了。
1. bing `CODER DOJO CHAMPIONSHIP HANDBOOK` 
   - <img src="https://images.gitee.com/uploads/images/2022/0113/024047_1819f0c6_5631341.png" height="19px"> 小盆友脑袋里想的，HTML 网页肯定是啦，入门案例，前面的教程里有介绍，但要 COOL 起来还是需要些脑洞的。
   - <img src="https://images.gitee.com/uploads/images/2022/0113/024458_6a6ae29f_5631341.png" height="19px"> 我得找个 大大 的 好和 [第0期](第0期%20Hello,%20openEuler!%20拥抱未来.md) <a href="https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md" target="_blank"><img src="https://images.gitee.com/uploads/images/2021/0205/005940_d9a7c2b3_5631341.png" height="19px"></a> 呼应上。
   - <img src="https://images.gitee.com/uploads/images/2022/0113/024842_c2c47cb5_5631341.png" height="19px"> <img src="https://images.gitee.com/uploads/images/2022/0113/024853_707e61b5_5631341.png" height="19px"> 师徒二人是主要的语境啦，这个忍者的 IDEA 还是有点意思的。
   - <img src="https://images.gitee.com/uploads/images/2022/0113/025239_d789a85a_5631341.jpeg" height="19px"> 这样的实景图，家长陪着一群熊孩子，教育出来的是不是就是 元宇宙 一代呢？不好说，电脑反正是用上了，数字装备干啥呢？玩游戏呗。“这个要避免”，游戏有罪论，要辩证地看啦。这个陪娃的场景，就是火种队的起点，也是丹麦陪娃的标准模式，谁叫人家发达呢？咱们的教育还是有太多期待啦。不容许先玩起来。或者说，玩相比学还是容易。
   - <img src="https://images.gitee.com/uploads/images/2022/0113/025724_e2e3c050_5631341.png" height="19px"> 看来 Dojo 这个 IP 是相当丰富的啦。耳熟能详，SIGer 出圈儿是 “当务之急”。可以不用找了先

今天是来感谢的，就不跑题啦。反正就是鲜艳夺目，无主题。三段式封面，估计最后连 SIGer 桌旗都没地方放。硬放吧就。

<p title="#12 开源道场 - DOJO CHINA的构建"><a href="https://gitee.com/yuandj/siger/issues/I3CST6#note_8273242_link" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0113/121557_649f2b3d_5631341.jpeg" height="999px"></p>

> 成稿，又有新的斩获，头条除了 18岁程序员 的故事外，《[轻量级 社交 MOOC 平台 FutureLearn](https://www.sohu.com/a/21039431_115563)》的推介则是另一个重点，我对 2015年 的旧闻做了【[笔记](https://gitee.com/yuandj/siger/issues/I3CST6#note_8277034_link)】这可是最早的 [SIGer 主题 STEM 的缘起](https://gitee.com/flame-ai/siger/issues/I3895J) 啊，因为大量的树莓派公开课程，都是以 FutureLearn 为平台的，CODER DOJO 也不例外，稍微遗憾的是，自从 2019 年以来，这门 DOJO 入门课就鲜有人问津啦。这不影响它作为宝藏的存在。 :pray: （尽管 [STEM](STEM/) 有了更新的内容，树莓派有了自己的 PRESS 部门。）

  - 我非常满意我的这幅新作：

    - [SIGer 星云](Tools/GitSTAR.md#gitstar-%E6%98%9F%E4%BA%91)的创作可以将原料展示出来了，
    - 我喜欢 大大地 18岁，它成年了，它也是[少年](%E5%B0%91%E5%B9%B4/)，这符合 SIGer 的特质，我们就要推荐风华少年
    - Story 和 Stroies 是呼应上了，本期的两大主题，和 SIGer 的特质完全吻合，我们需要故事，这是宇宙的信息
    - [STEM](STEM/) 从未离开过 SIGer ，一周年的基本重量级期刊都无不围绕这个恒古的主题 STEM ，它是教育的主题
    - 自下而上 3 2 1 很有意思，三个家长，托着 2 个 NIJIA 熊孩子，为了一个 冠军奖杯，还是 LEGO 玩具的。
    - 左上角的 [ROCKET](https://gitee.com/yuandj/siger/issues/I3CST6#note_4678815_link) 是奔赴星辰大海用的，也是 [RISC-V](https://gitee.com/RV4Kids) 的愿景，是对 @[秦风岭](https://gitee.com/qinfengling) 老师的敬意 感谢他的分享。
    - 奖杯顶端的 两层 太极图，说明了 [DOJO](https://gitee.com/yuandj/siger/issues/I3CST6) 和 [SIGer 创刊](https://gitee.com/flame-ai/siger/issues/I35C2C)的链接，今天知道了，它是两颗黑洞的逐渐逼近。

    （请允许我偷个懒，SIGer 桌旗上的两个最新期刊就不换了，保留下[周年庆特刊](https://images.gitee.com/uploads/images/2021/1230/154825_ea5b1875_5631341.jpeg)的两幅!）

# [FUTURELEARN](https://gitee.com/yuandj/siger/issues/I3CST6#note_8277034_link): [开始一个 Dojo 课程](https://www.futurelearn.com/courses/start-a-coder-dojo)

 **还有更多问题吗？** 我们为想要结构化的方式来开启 Dojo 团队的人提供免费的在线课程，[向您介绍 CoderDojo](https://www.futurelearn.com/courses/start-a-coder-dojo)。 FUTURELEARN 是树莓派的标准在线课堂平台，我们有B站 :dancer: 

### WEEK1 提纲

  - 第 1 周 向您介绍 CoderDojo

    欢迎来到课程

    - 1.1 欢迎来到第一周视频
    - 1.2 团队介绍 文章 [照片](https://images.gitee.com/uploads/images/2022/0108/234731_a5dc706b_5631341.png) 
      - [Philip Harney](https://www.futurelearn.com/profiles/5679875) | [Vanessa Greene](https://www.futurelearn.com/profiles/10020811) | [Helen Vallance](https://www.futurelearn.com/profiles/11963222) | [Vasu](https://www.futurelearn.com/profiles/9978136) | [Darren](https://www.futurelearn.com/profiles/9664784)
    - 1.3 自我介绍 讨论
    - 1.4 你想学什么？讨论

    什么是编码，为什么它很重要？

    - 1.5 介绍编码 讨论
    - 1.6 什么是 CoderDojo？视频
    - 1.7 为什么年轻人会来道场？讨论 [图](https://images.gitee.com/uploads/images/2022/0109/003019_3001ae7e_5631341.png)
      - 好玩，交朋友，做COOL的事情。
    - 1.8 年轻人在道场中可以创造什么 视频

    社区与精神

    - 1.9 CoderDojo 精神 文章
    - 1.10 全球 CoderDojo 社区 文章

    支持您的道场

    - 1.11 冠军手册 文章
    - 1.12 注册您的道场 文章
    - 1.13 回顾你所学到的东西！测验
    - 1.14 回顾和预览 视频

### [1.1 欢迎来到第一周](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215584_link)

本周我们将研究：

- 什么是编码，以及为什么学习编码很重要
- CoderDojo 的历史
- 为什么年轻人参加CoderDojo俱乐部，以及他们在那里制作的各种很酷的项目
- CoderDojo的基本原则和信念是什么
- 如何与全球 CoderDojo 社区建立联系
- 您可以在哪里为您的俱乐部寻求支持和建议

Close transcriptDownload video: standard or HD
- 0:03 
  您好，欢迎来到课程。我是 Philip Harney <img src="https://images.gitee.com/uploads/images/2022/0108/233930_11402220_5631341.png" height="19px">，CoderDojo 基金会的教育内容负责人，我将是本课程的首席教育者。我编写学习资源来帮助年轻人发展他们的编码技能。我们将在第二周和第三周更多地讨论这些内容。在过去的六年里，我一直是Dojo的创始人，冠军和导师，这对我来说是一次辉煌的经历。我期待着帮助你开始同样的旅程。我是Giustina Mizzoni，CoderDojo基金会的执行董事。我参与 CoderDojo 运动已经超过五年了。我加入基金会，成为第一位与联合创始人詹姆斯·惠尔顿（James Whelton）合作的员工，让它启动并运行起来。

- 0:41 
  我是伊娜·帕纳约托娃 <img src="https://images.gitee.com/uploads/images/2022/0113/162549_f6723805_5631341.png" height="19px">。我是CoderDojo的导师，我是科克大学计算机科学专业的二年级学生。从我记事起，我就一直在和我的弟弟们一起编码。2012年，我很幸运地在13岁时加入了世界上第一个道场。一开始，我只是一个忍者，帮助导师。在不知不觉中，我发现自己在一对一地指导，然后继续教小组，最终整个班级都没有意识到这一点。我参与了科克附近许多新俱乐部的创办，甚至在都柏林的几家俱乐部帮忙。

- 1:13 
  本課程將向您介紹CoderDojo運動，帶您完成建立Dojo，組建團隊，尋找場地，為Dojo拾取項目以及計劃您的第一個課程的過程。简而言之，我们将帮助您成为CoderDojo冠军。本课程适用于任何热衷于帮助社区中的年轻人发展其技能和创造力的人，无论他们的技术水平如何。您不需要知道如何编码，也不需要具有任何技术背景。你只需要对CoderDojo使命的热情，以及让事情发生起来的动力。许多非技术冠军多年来一直在成功运行Dojos。

- 1:51 
  这也是一个分享您的想法的绝佳机会，这样我们都可以从中学习并在此基础上再接再厉。在整个课程中，您会注意到您可以在每个页面上添加评论并加入讨论。我们鼓励您这样做。我们将密切关注评论并回答您的所有问题。CoderDojo 建立在社区多年来不断改进的共同想法和原则之上，开始贡献永远不会太早。

### 1.5 [Introducing coding](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215630_link) 介绍编程

- What is coding? 什么是编程？

  编码或计算机编程是一种技能，即处理一个问题并创建一组指令-我们称之为计算机程序-以便计算机解决问题。计算机程序可以是任何东西，从将两个数字相加的简单程序，一直到复杂的网站，视频游戏或应用程序。

- Why learn to code? 为什么要学编程？

  学习如何编码为年轻人提供了一个新的创造性的出口，一个可以用来表达自己的渠道，同时发展他们的毅力，解决问题的能力和逻辑思维能力。这就是为什么在CoderDojo，我们希望每个年轻人都有机会学习如何在有趣，免费和社交空间中使用技术进行创作。

  - _很高兴看到有人从没有经验到拥有他们手工构建的工作互动网站 - 当他们看到它工作时，他们脸上的表情真是太棒了。_ 
    > _—— Craig Steele, Dojo champion, Glasgow_ 

- Why is coding important? 为什么编程重要？

  了解如何编码会为您提供一个创造性的出口，帮助您开发新的思维方式，打开通往新职业机会的大门，并帮助您更好地了解我们的数字世界。

  - _“是一次改变人生的经历，一个新的整个领域被打开了，我喜欢它！”_  
    > _—— Matei, 10, Ninja at Cluj-Napoca Dojo, Romania_ 

- _**为什么你认为你所在社区的年轻人学习编程很重要，他们将如何从你的道场中受益？请告诉我们您的想法！**_ 

### 1.6 [什么是 CoderDojo？](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215639_link)视频

- 0:02
  太多的结构和僵化的组织会扼杀创造力。大多数道场都有一种有组织的混乱感，这是创造力，实验和学习者主导的技能发展的能量融合。我们一起去了头等舱，

- 0:19
  起初我以为我并不感兴趣：我认为这可能只是你必须做这个和做那个的事情，但实际上这真的很有趣，我第一次编码了一些东西。

- 0:32
  这真的很有趣 CoderDojo，因为，就像，你遇到了很多朋友和所有人。然后，就像，有时你可以做团队合作和所有的事情，但有时你可以做自己的项目，这真的很有趣。在道场，与会者通过在非正式氛围中开展项目来学习。这包括他们之间的交谈和共同努力。鼓励聊天和交朋友。当与会者无法自己或在同龄人的帮助下找到解决方案时，导师会提供帮助。我知道我们正在与技术合作，你可能会觉得你需要成为一个技术奇才来建立一个道场或指导一个道场。事实并非如此。我们几乎都是一样的。

- 1:14
  我们与孩子们的唯一区别是，我们可以指导他们。只要你能开车带孩子们完成这些步骤，教他们如何学习和自给自足，这就是你所需要的。

- comments

  - _CoderDojo是一个面向7至17岁年轻人（忍者）的免费开放编码俱乐部（Dojos）的全球运动，他们可以在忍者同伴和志愿者导师的支持下探索技术。CoderDojo的使命是为世界各地的年轻人提供在社交和安全的环境中学习编码的机会。_ 

  - _我八岁时第一次开始编码，但直到十六岁我才用这些知识做很多事情，因为我认识的其他人都没有对它感兴趣。对于年轻人来说，拥有一个CoderDojo作为聚会并分享他们对技术的热情的地方是很棒的：这是一种通过共同开展他们感兴趣的项目来保持这种激情和发展技能的方式。_ 

  - _Dojo为我们的年轻人提供了一个独特的，非正式的和协作的环境，让他们免费学习编码并与技术专业人士互动。它使孩子，导师和父母受益，并确保年轻人有一个安全，有趣的地方接触编码和技术技能，无论他们是否选择专攻它。这是对社区和我们的年轻人的极好服务，所以我强烈建议任何人建立自己的服务。_ 
    > _—— Garima Singh, Dojo champion, Round Rock, Texas, USA_ 

  - **The story of CoderDojo** 

    The movement was founded by James Whelton and Bill Liao. James was an 18-year-old coder who was running a coding club in his school, and Bill was an entrepreneur and philanthropist who saw the positive impact James was having and wanted to grow his project. Working together, they founded CoderDojo and ran the first Dojo on 23 July 2011 in Co. Cork, Ireland. This Dojo is still running, and it’s actually the one Ina is attending today!

    _该运动由James Whelton和Bill Liao创立。詹姆斯是一名18岁的程序员，在他的学校里经营着一个编码俱乐部，比尔是一位企业家和慈善家，他看到了詹姆斯的积极影响，并希望发展他的项目。他们一起创立了CoderDojo，并于2011年7月23日在爱尔兰科克公司运营了第一个Dojo。这个道场还在运行，其实就是伊娜今天参加的道场！_ 

    Now, ten years later, CoderDojo’s open-source model has helped the movement spread around the world: there are more than 2000 Dojos spread across 110 countries, and people start new Dojos every day!

    _现在，十年后，CoderDojo的开源模型帮助该运动传播到世界各地：有超过2000个Dojos分布在110个国家，人们每天都开始新的Dojos！_ 

    In what way has learning in a group been beneficial to you in the past? What skills has it helped you develop?

    **过去，在小组中学习对你有什么好处？它帮助你发展了哪些技能？** 

    - _[DOJO 就是一个社群，开源文化的阵地，来这里可以编程，也可以做任何事情，比如下棋，还有 SIGer ，因为SIGer 马上就要有新名字啦，SIGer 学习社群。](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215642_link)_  :sunglasses:

    - **[In what way has learning in a group been beneficial to you in the past? What skills has it helped you develop](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215644_link)?** 

      When you're a people to solve problems in a project, the problems can't be found and you could solve all of them at all. But you're in a group the problem be found fast, be solved fast also. That the Dojo and Opensource, the place of Dojo is OpenSource Community for yonth.

### [1.7 Why do young people come to Dojos? 为什么年轻人会来道场？](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215649_link)

参加CoderDojo的年轻人被称为忍者，他们会从你的Dojo中得到很多东西。虽然每个忍者都是独一无二的，但当你问忍者为什么喜欢CoderDojo时，你往往会反复听到一些事情。

- **It’s fun!**  好玩

  _“用代码制作很酷的东西很有趣。忍者可以根据他们感兴趣的东西创造一些很棒的东西，他们可以与朋友分享他们创造的东西。他们还可以学习如何编码，当你第一次工作时，这看起来有点像魔术！”_ 
  >  _—— Lucy, Ninja, Dun Laoghaire, Ireland_ 

- **Making friends** 交朋友

  道场的忍者可以结识与他们有共同兴趣的其他年轻人，并与他们一起学习和提高他们的编码技能。

  _“在这里交朋友很容易，很多人也有同样的想法，所以他们也许能帮助你，他们可能知道该怎么做。这就是我喜欢CoderDojo的原因，你可以得到帮助，而不仅仅是总是问导师。”_ 
  > _—— Joe, Ninja, Dun Laoghaire, Ireland_ 

- **Making cool things they care about** 做COOL的事情

  一旦他们掌握了基础知识，CoderDojo Ninjas就会选择自己的项目并从事他们感兴趣的工作，有时与其他忍者一起工作。他们了解完成他们想要构建的项目所需的内容。这意味着，如果他们对一个项目失去兴趣，他们只需改变它或用让他们兴奋的东西代替它。

  _“在 CoderDojo 中真的很有趣，因为你会遇到很多朋友，有时你可以做团队合作，有时你可以制作自己的项目，这真的很有趣！”_ 
  > _—— Erin, Ninja, Dun Laoghaire, Ireland_ 

- _**你认为为什么年轻人会来你的道场？什么会让他们回来？他们的理由会因年龄，性别或其他因素而异吗？**_ 

  - [I'm a maker also, and did lost of project, and familier with programming, my friends also can be directors for yonth. And Opensource community have lots of resouce. we just got a placement could work together.](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215651_link)

    <p><img src="https://images.gitee.com/uploads/images/2022/0109/003019_3001ae7e_5631341.png" width="599px"></p>

### [1.8 年轻人在道场中可以创造什么 What young people can create at a Dojo](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215656_link) 视频 

- 0:01
  在道场，忍者可能会做任何事情。他们可以使用Scratch来创建他们的第一段代码。甚至是数字艺术作品。或者创建一款受他们最喜欢的独立开发者启发的游戏。他们可能会使用HTML，CSS和JavaScript（网络语言）来创建一个网站来通知人们。或者分享他们热衷的东西。或者只是看看他们是否可以。他们甚至可能迈出迈向计算机科学未来的第一步。

- comment

  在道场课程中，忍者在导师（促进道场学习的志愿者）的支持下，用代码制作一些东西。这可以是来自CoderDojo资源站点的项目，在线编码教程，或者忍者已经想象过并且现在想要实现的东西。

  忍者可以分组工作，也可以单独工作，这取决于他们的项目性质和他们的喜好。

  - **CoderDojo projects** 

    CoderDojo 社区和 CoderDojo 基金会团队创建了一系列教育项目，您可以在 coderdojo.com/resources 找到这些项目。这些是完全免费的，并且存在是为了让您更轻松地运行Dojo！我们将在第2周和第3周详细讨论它们，但这里有一个快速的，您可以立即尝试构建一个简单的网页！


  - **What do you want to make with your Ninjas?** 

    在下面的讨论中，告诉我们您希望在道场与忍者一起创造什么。看看评论，从其他冠军那里获得想法！我们将阅读所有内容，选择我们最喜欢的想法，然后（如果您愿意让我们！）将其变成CoderDojo资源网站的免费项目教程！

    - [We runing a boardgames gerneral AI engines, now making a develop board to running it, in my dojo club, computers match the board games will be a major project. Some boardgames is ancient games, more fun and more historical.](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215659_link)

### 1.9 CoderDojo 精神 The CoderDojo ethos

- comments

  CoderDojo运动的精神是我们如何在Dojo和社区中一起工作，以及我们的目标是如何影响世界的方法。多年来，它已经发展成为所有道场都同意遵循的一套关键原则。在接下来的几周里，我们还将向您展示如何将这些内容实际整合到您的 Dojo 中。

- 准则：

  - 一条规则：要酷！— 善待你的道场成员。
  - 包容和免费 - 道场欢迎每个人，并且可以免费参加。
  - 非正式和有趣 - 道场应该有一个活泼的社交氛围。
  - 开源 - 该运动是开放的，并由其成员的共同努力建立。
  - 协作和团队合作 — 协同工作是 Dojo 体验的核心。
  - 变革 - 鼓励忍者用他们学到的东西来改善他们的社区。


- **One rule: be cool!** 
  最古老的原则应该介绍给所有Dojo与会者。简而言之，这意味着我们应该善待彼此。

  - _“帮助，分享，支持，鼓励，合作和善良都非常酷。欺凌，撒谎，浪费时间和扰乱道场都被认为是不酷的。”_ 
    > _—— Bill Liao，CoderDojo 联合创始人_ 

- **Inclusive and free** 包容且免费
  对于CoderDojo运动的精神来说，参加世界任何地方的任何Dojo都不收取任何费用，这是最基本的。这使得Dojos对任何想要参与的人都是开放和包容的。

  CoderDojo具有完全的包容性，并鼓励多样性。欢迎所有年轻人，无论性别，社会地位，宗教，种族，性取向，社会经济地位或信仰如何，参加当地的CoderDojo。

  - “我喜欢成为一个真正和深刻的利他主义的组织的一部分。CoderDojo真诚地努力接触和激励各种背景和情况的年轻人。”
    > _—— David Welch, Dojo champion, Iowa, USA_ 

- **Informal and fun** 非正式和有趣
  道场是拥有自己社区的俱乐部。道场会议的气氛是有趣和社交的，所以忍者有机会聊天和一起工作。尽量避免让教练在房间的顶部，忍者坐在队列中听，就像在教室里一样。

  承担风险和犯错误对学习至关重要，尤其是在编码方面。失败是学习正确方法的最佳方式之一。重要的是要确保忍者知道这一点，并且知道即使是最好的编码人员有时也会失败。

  作为道场导师，当忍者要求我帮助他们构建一些我不知道如何创建的东西时，我总是让他们看到我在网上查找。我告诉他们我是如何搜索的，我是如何自学的，以及我如何识别和修复我的错误。

- **Open source** 
  自成立以来，CoderDojo一直基于开源模型。任何地方的任何人都可以建立一个Dojo，如果他们在CoderDojo的精神和价值观中运作。所有 CoderDojo 志愿者都是全球开源 CoderDojo 运动和网络的一部分，我们鼓励所有人与社区分享他们的见解和改进。

- **Collaboration and teamwork** 协作和团队合作
  不同的人有不同的长处，互相学习是团队合作的一大好处。我们建议您鼓励忍者自组织成团队。这可以帮助他们更好地了解自己的个人优势，学习如何与他人合作，并帮助他们的同龄人解决问题。

  - “看着孩子们享受我们教给他们的东西，看到他们互动并相互学习，真是太令人满意了。”
    > _—— Vicky Hogan, Dojo mentor, Wexford, Ireland_ 

  团队成员不必具有相同的年龄或能力：将其混合在一起，让每个人都有机会通过小组协作学习和成长！如果可能的话，尽量确保群体不限于一种性别，以便年轻人可以跨性别工作。团队内部的多样性是创新思维和创造力的已知催化剂。此外，女孩和妇女在STEM课程，课程和工作领域的代表性不足，鼓励女孩成为导师是更多女孩参与CoderDojo 的强大催化剂。如果他们有女性榜样，女孩更有可能探索她们对技术和编码的兴趣，并考虑STEM职业道路。

- **Changemaking** 做出改变
  应该鼓励忍者从事他们直接感兴趣的主题和项目。可以引入具有积极社会，环境或社区影响的项目，以帮助忍者确定影响和造福周围世界的机会。除了学习团队合作和协作技能外，还鼓励忍者展示他们正在从事的项目，以发展他们的沟通和领导技能。

  Share this article:
  - [I have a good idea FORK DOJO. encourage any body any group to share opensourse ETHOS. THANKs dojo, Thanks people meet with opensource. ](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215673_link):pray:

### [1.10 全球 CoderDojo 社区 The worldwide CoderDojo community](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215684_link)

- comments
  CoderDojo运动是一个国际社区，人们致力于分享他们对技术的知识和热情。您现在是该社区的成员，您有很多机会与您的其他成员交谈，甚至见面！

- **Join the community online** 在线加入社区
  您可以在线与CoderDojo社区联系，提出问题，并通过加入您的志愿者同事分享想法和灵感：

  CoderDojo 全球 Slack 频道中的实时聊天。您也可以在那里找到我们所有的版主！

  - **Contact a specific Dojo** 联系特定的道场
    如果您想联系特定的道场，也许要为您所在地区的道场设置活动，您可以在他们的道场列表页面上找到他们的联系电子邮件地址和社交媒体帐户，您可以在此处搜索。

- **Coolest Projects** 最酷的项目
  最酷的项目是忍者的年度展示活动，他们可以带来他们在过去一年中在道场中建造的最酷的东西，并炫耀出来。对于他们来说，这是一个绝佳的机会，可以结识社区成员，与其他忍者分享想法，并根据最酷的项目活动的规模，可能会结识软件和游戏行业的人。

  - _“我喜欢与所有其他与会者会面并分享项目。在美国，我只在我的道场和孩子们一起工作过，所以看到国际道场聚集在一个集体活动中是独一无二的。”_ 
    > _—— Kavi，15岁，Ninja at Tiburon CoderDojo @ BelTib Library，加利福尼亚州，美国_ 

  对于道场的导师和冠军来说，这也可能是一年中最愉快和最有趣的一天（至少对我来说是这样！他们可以一起来，看到道场的忍者玩得很开心，并看到他们共同工作的结果。

  我们的数字展示"最酷的项目"在线注册将于2021年2月1日开放。

  在 [最酷的项目网站](http://coolestprojects.org/) 上了解更多信息。（http://coolestprojects.org/）

  - [It was so much fun to see the two children wearing ninja culture shirts. I'd love to join them](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215689_link)  
    快看那[两个小孩穿着忍者的文化衫](https://images.gitee.com/uploads/images/2022/0109/005927_243a6a0a_5631341.png)，太好玩啦。我好想加入他们

    <p><img src="https://images.gitee.com/uploads/images/2022/0109/005927_243a6a0a_5631341.png" width="599px"></p>

### [1.11 冠军手册 The champion's handbook](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215694_link)

- **comments** : In the CoderDojo champion’s handbook, we’ve collected information a CoderDojo champion will need to know to run a Dojo. You can [download it here](http://dojo.soy/chb) for free! 在 CoderDojo champion 的手册中，我们收集了 CoderDojo 冠军运行 Dojo 需要知道的信息。您可以在这里[免费下载](http://dojo.soy/chb)！

  本课程中的大多数主题都包含在其中，有时甚至更深入。它包括：

  - 编码和 CoderDojo 简介
  - 道场社区的成员
  - 开始道场
  - 运行道场
  - 规划您的道场
  - 与年轻人一起工作
  - CoderDojo 精神
  - CoderDojo 社区
  - CoderDojo 基金会

  该手册是参考指南，而不是您需要阅读的内容。如果您想了解有关特定 CoderDojo 相关主题的更多信息，或者想要找到特定问题的答案，本指南是您的起点。

  - [The CoderDojo Champions' Handbook](https://help.coderdojo.com/cdkb/s/article/The-CoderDojo-Champions-Handbook) 【[笔记](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215697_link)】 <img src="https://images.gitee.com/uploads/images/2022/0109/011155_977803b1_5631341.png" height="39px">

### [1.12 注册您的道场 Register your Dojo](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215704_link)

<p><a href="http://dojo.soy/register" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0109/011321_8f33314b_5631341.png" width="599px"></p>

成为 CoderDojo 冠军的第一步是在 CoderDojo 网站上注册您的 Dojo。你现在应该这样做，即使你的道场只是一个想法。然后，您将能够获得我们的帮助，使这个想法成为现实！

前往 [dojo.soy/register](http://dojo.soy/register)。如果您已经在 CoderDojo 网站上拥有帐户，请登录。如果没有，请注册一个。

登录后，您将进入网站的"开始 Dojo"部分。开始在那里填写表格，尽可能多地填写关于你的道场和你的团队的信息，如果你现在不是很多，请不要担心。该表单将保存信息，您可以回来填写更多部分，因为我们在接下来的两周内讨论各自的主题！

如果您在那里遇到任何问题或疑问，请寻找屏幕底部带有语音气泡的蓝色小圆圈。单击它可与 CoderDojo 基金会团队聊天，他们将帮助你完成整个过程。

通过点击上述链接，您将被带到第三方网站，该网站的使用已由CoderDojo安排并由CoderDojo负责。在此网站上，您将被要求提交一些关于您自己的信息。在提交您的信息之前，请确保您熟悉第三方网站的条款和条件以及隐私政策。CoderDojo 只会将您的个人信息用于上述目的，并按照第三方网站的条款使用。

无论您是否点击上述链接并提交您的个人信息，您的课程进度都不会受到影响。您的课程标记或FutureLearn个人资料也不会。出于通过第三方网站提交的任何个人信息的目的，CoderDojo将是数据控制者，而不是FutureLearn。

### [1.13 回顾你所学到的东西！Recap and preview](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215719_link) 测验

- 0:00
  感谢您本周加入我们，了解 CoderDojo 运动。我们希望你觉得这很有趣，并且你参与了整个讨论。下周，我们将了解道场的日常运行，寻找场地和组建团队。再见。

  第1周就到此为止！让我们来看看我们报道的内容，并期待在第2周为您准备的内容。

  本周你了解到：

  - 什么是编码，以及为什么学习编码很重要
  - CoderDojo的历史
  - 为什么忍者参加道场，以及他们在那里制作的各种很酷的项目
  - CoderDojo的基本原则和信念是什么
  - 如何参与全球CoderDojo社区
  - 您可以去哪里为您的道场寻求支持和建议

  下周你会得到计划，正如我们所看到的：

  - 谁将在你的道场
  - 建立志愿者团队
  - 为您的道场寻找场地
  - 寻找忍者参加你的道场
  - 与年轻人一起工作
  - 构建您的第一个电脑游戏！

  分享此视频：

  - _[thanks your course, see you next week.](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215720_link)_ 

  - **[Question 1](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215722_link)** 

    **What is coding?** 
    - Creating a set of instructions in a language that a computer can understand. *
    - Writing in binary.
    - Complex maths that you need a degree to do.

    **Correct** 
    > Philip Harney (Lead Educator) <img src="https://images.gitee.com/uploads/images/2022/0113/162259_9f1d3449_5631341.png" height="19px">

    Correct! Coding, or computer programming, means creating instructions for a computer in a language it can interpret.

  - **Question 2** 
    What do we mean when we say “One rule: be cool” in CoderDojo?
    - Wearing sunglasses is mandatory at all CoderDojo events.
    - Behave well towards your fellow Dojo members. *
    - Dojos should keep up with the latest trends.

  - **Question 3** 
    What do we call the young people who attend Dojos?
    - Students
    - Learners
    - Ninjas *

### WEEK2 提纲

  - 第2周 整理道场

    准备

    - 2.1 欢迎来到第二周视频
    - 2.2 道场成员文章
    - 2.3 建立团队文章
    - 2.4 查找场地文章
    - 2.5 道场资源 讨论

    教育资源

    - 2.6 试用寿司卡文章

    忍者

    - 2.7 招募忍者 讨论
    - 2.8 与年轻人一起工作文章
    - 2.9 道场文章中的保护
    - 2.10 回顾和预览视频

### WEEK3 提纲

  - 第3周 你在CoderDojo的一年

    规划您的道场

    - 3.1 欢迎来到第三周视频
    - 3.2 道场会话的结构 讨论
    - 3.3 在道场文章中使用学习资源
    - 3.4 计划你的道场年讨论

    开发你的道场

    - 3.5 与忍者一起成长 讨论
    - 3.6 表彰成就讨论
    - 3.7 介绍基于文本的编码讨论
    - 3.8 特别活动 讨论
    - 3.9 展示案例研究：三谷道场，美国文章

    下一步是什么？

    - 3.10 第一年后去哪里
    - 3.11 课程结束视频
    - 3.12 反馈和想法练习

### [在你的学校建立一个代码俱乐部](https://codeclub.org/en/start-a-code-club/)【[笔记](https://gitee.com/yuandj/siger/issues/I3CST6#note_7964809_link)】

任何老师都可以建立和运行 Code Club，无论他们是熟悉编码还是完全陌生——因为他们将与学生一起学习。我们的免费项目将会议计划的工作排除在外，不需要特殊设备。

[立即注册您的俱乐部！](https://codeclub.org/en/club_applications/new)

<p><img src="https://images.gitee.com/uploads/images/2021/1221/230703_11eefa98_5631341.png" width="599px"></p>

 **开始使用代码俱乐部** 

1. 在我们的网站上创建和帐户。
2. 在我们的网站上注册您的学校。您不需要在您的俱乐部为孩子们注册 - 只需输入学校的详细信息，您就可以开始了。
3. 如果您想自己经营俱乐部，那么您已准备就绪！
或者，您可以在我们的网站上为志愿者做广告。您当地的志愿者可以搜索您的俱乐部，然后通过您的俱乐部页面与您取得联系。
4. 会见您的志愿者以组织俱乐部并检查他们的 DBS 声明（或同等声明）。
5. 使用他们的电子邮件地址将您的志愿者添加到您的俱乐部。
6. 使用免费的 Code Club 项目（这些项目是孩子们可以遵循的分步指南）来举办俱乐部课程。
7. 在仪表板中查看活跃俱乐部的额外资源。

[注册并注册您的俱乐部！](https://my.raspberrypi.org/signup?brand=codeclub&returnTo=https://codeclub.org/auth/rpi?returnTo=Mw==)

- [面向 9-13 岁儿童的由志愿者主导的免费编程俱乐部的全球网络](https://gitee.com/yuandj/siger/issues/I3CST6#note_7964887_link)  
  https://international.codeclub.org/

  [查看下面的列表，了解您所在的国家/地区是否有 Code Club 国家合作伙伴。](https://gitee.com/yuandj/siger/issues/I3CST6#note_7964889_link)
  - 澳大利亚	1600码俱乐部
  - 巴西	365代码俱乐部
  - 加拿大	1210 密码俱乐部
  - 克罗地亚	185 代码俱乐部
  - 法国	215 代码俱乐部
  - 印度	545 代码俱乐部
  - 新西兰	610密码俱乐部
  - 韩国	140个代码俱乐部
  - 乌克兰	395 代码俱乐部
  - 英国	10345 代码俱乐部
  - 美国	984密码俱乐部

- [去发现一个身边的道场！](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215367_link) <img src="https://images.gitee.com/uploads/images/2022/0108/214211_63b94ca2_5631341.png" height="39px"> [详情](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215387_link)

  > 重温 道场搜索！曾经就是这样找到了 @秦风岭 老师，如今 SIGer 1周年，欠他一本期刊！以 DOJO 的方式运行火种队，就是最好的致敬了。！

    [重温了接头暗号，我们需要一个道场一个道场的孵化吗？按下不表](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215423_link)

    > 2021-3-22  
    >
    > 袁先生：  
    > 您好，非常感谢您的关注，由于疫情的影响，我们暂时还没有恢复线下活动。  
    > 我们的活动组织都是志愿者形式的，用的设备是micro:bit，软件是Scratch 3.0。  
    > 如果您也熟悉编程相关的基础知识，也可以随时下载我们之前公开的教程。  
    > 如果您有时间加入这个志愿者活动，也可以随时和我保持联系。
    > 
    > 谢谢  
    > Fengling

- [DOJO 公开课](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215424_link)

  - CoderDojo系列课程：石头剪刀布（micro bit），也看看火焰棋实验室的 [猜东里猜](https://gitee.com/flame-ai/YCGCIT) 吧
  <p><img src="https://images.gitee.com/uploads/images/2022/0108/220219_e914f71f_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/221252_9ceff005_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/221422_cc218c64_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/221538_a1ab72bb_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/221654_e8150925_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/221806_6447c248_5631341.gif" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/222927_859238a0_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/223151_d6ca7e7f_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0108/223441_635cce56_5631341.png" height="99px"></p>

- [什么是拥护者？](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215488_link)

  > [核心是开源的项目，可以示范给志愿者，并能成功带娃。我们的火种队早就具备这样的能力啦。这也是 SIGer 的平台作用，开源和分享带娃经验。](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215493_link)

- [还有更多问题吗？](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215498_link) 

  - [开始一个 Dojo 课程](https://www.futurelearn.com/courses/start-a-coder-dojo) 请重温开篇 WEEK1
  - 帮助台
  - 为您订制的200+编程课程
  - 找到并参观您附近的一个Dojo活动
  - Start a Dojo: Webinar， **[bing](https://gitee.com/yuandj/siger/issues/I3CST6#note_8215557_link)**  `Webinar`

- [开始构思封面啦](https://gitee.com/yuandj/siger/issues/I3CST6#note_8273242_link)。
  - [本节](https://gitee.com/yuandj/siger/issues/I3CST6#note_8283530_link)：[封面](https://images.gitee.com/uploads/images/2022/0113/121557_649f2b3d_5631341.jpeg) 并开始撰稿

- [FutureLearn：更轻量、更社交的英国 MOOC 平台](https://gitee.com/yuandj/siger/issues/I3CST6#note_8277034_link)

# [FutureLearn：更轻量、更社交的英国 MOOC 平台](https://www.sohu.com/a/21039431_115563)

2015-07-02 19:26

讲到在线学习，多数人会想到的词大概是“MOOC”（慕课），更具体的话，则是Coursera、Edx这两大MOOC代表平台。事实上，人们对“MOOC”一词的印象，基本是被Coursera和Edx两大平台所决定的。诚然，MOOC平台基本采用的都是“视频教程+评测&认证”这种学习模式，最主要的差异化竞争力，则是体现在提供的课程内容和质量上。但是，除了这两大巨头以外，其他的MOOC有什么其独到之处？

在Coursera、Edx诞生的同年2012年年底，英国第一个MOOC平台FutureLearn也正式成立了，并于次年9月推出第一门在线课程。创立和赞助FutureLearn的是英国公开大学(The Open University，简称OU)。 英国公开大学算是目前世界上发展最成熟的在线大学，于1969年建立，学生以英国人为主，也有欧洲各地甚至非洲及亚洲的学生，是全欧洲规模最大的学术单位，创校以来已招收超过300万学生。

FutureLearn虽然不像Coursera和Edx那么备受瞩目， **但近200万的用户量和高达15%的课程完成率，无疑体现了它巨大的发展潜力。那么以它为例，在MOOC发展越驱成熟的今天，MOOC还存在着什么被人们忽略的可能性？** 

### 1. 不只是美式教育

在FutureLearn推出前，在Coursera合作授课的英国大学只有两所：爱丁堡大学和伦敦大学。这不仅是因为英国及欧洲区域的在线教育发展较为缓慢，也是因为较为传统刻板的英式教育受到在线学习者的反响寥寥。比起遍地开花的美国大学课程，英国大学极为少数的MOOC让在线学习者对英国教育的了解很浅薄。直到FutureLearn的出现，才改变了这个格局。现在，与FutureLearn合作的总共有45所以英国大学为主的世界一流大学。

### 2. 不只是大学主导

除了和大学合作以外，FutureLearn的合作授课伙伴还有大英博物馆、英国文化协会、大英图书馆、国家影视学院(National Film and Television School)等权威机构，这无疑丰富了课程的内容和学习的角度。



### 3. 轻量化设计

以往的公开课程，学习者可以随时下载观看视频，却因为缺少约束力，导致学习效果极差。Coursera和Edx推出后，学习者需要遵循固定的上课时间、完成规定的测验、小论文、期中期末考试等等，学习者收获的内容是更多更扎实了，然而，这为学习者设置的重重关卡导致了MOOC平台基本破不了十位数百分比的课程完成率。

FutureLearn轻量化的设计在这两个极端之间达到了很好的平衡。"Keep it simple（尽量简洁）"便是FutureLearn的十大理念之一。除了UI设计简洁大方以外，FutureLearn把Coursera上繁琐多变的课程页面分类诸如”课程大纲、论坛、公告、评分规则“等等都删减了，只剩统一的“To do（待办）”、“Activity（活动）”、“Replies（回复）”、“Progress（进展）”四大项。



### 4. 学习区和讨论区结合促进社交

在课程主界面“To do”区域进入某一课后，课程页面就变得像一个帖子一样，帖子的内容是视频教程和文字介绍，点击”comments“评论按钮后视图分为左右两块，展开了右侧的评论区。这种设计将Coursera中的”视频教程“和”讨论区“结合起来，使得交流更方便，无形间促进了学生老师之间的互动。

秉着"Create connections（建立学习者之间的联系）"的理念，除了把讨论区和视频学习区结合起来，为了进一步强化社交功能，FutureLearn参考了Facebook的"follow（关注）"、"like（赞）"等概念。用户在评论区可以专门去找自己关注的人的回复，也可以看到最热门的评论。每个用户还有自己的独立档案，里面记录了上过的课程、论坛的活动、粉丝和关注的人，这相当于去粗取精后一个轻量级社交产品的用户界面。



### 5. 学习资料不再零碎不堪

"Tell stories（讲故事）"作为FutureLearn十大理念的其中之一，代表着FutureLearn希望学习者上的每一门课程，都可以是一次精彩纷呈的旅行、一本扣人心弦的小说。因此，除了将论坛和视频学习区域结合起来，"To do（待办）"分类下还将阅读文本、测试、讨论课结合了起来，使课程中不同形式的学习资料有机结合了起来。这无疑将过去Coursera里视频、文本、讨论区、推荐阅读等等信息都整合在了一起，帮助学习者理清知识脉络，对话题有了更系统性的把握。

遗憾的是，FutureLearn目前还在研发手机客户端，因此用户只能在电脑端浏览课程内容。但是相信结合其轻量化、社交化的核心理念设计和特色、优质的课程内容，FutureLearn推出App之后一定会斩获大批量的忠实用户。

FutureLearn的十大理念也很值得我们借鉴：

1. Open（开放课程内容）
2. Listen to our learners（倾听学习者的需求）
3. Tell stories（讲故事）
4. Provoke conversation（鼓励对话）
5. Embrace massive（扩大规模）
6. Create connection（建立学习者之间的联系）
7. Keep it simple（尽量简洁）
8. Learn from others（向他人学习）
9. Celebrate progress（表扬学习者的进步）
10. Embrace FutureLearners（欢迎终身学习者）

作者：芥末堆-Silvia俞

原文链接：http://www.jmdedu.com/viewpoint/detail/1537 返回搜狐，查看更多

声明：该文观点仅代表作者本人，搜狐号系信息发布平台，搜狐仅提供信息存储空间服务。

### [STEM UK](https://gitee.com/yuandj/siger/issues/I3CST6#note_8155352_link)

STEM 在英国是方兴未艾的，这也是石榴派最初学习的榜样，但一直从未实践过，dojo则是例外，实践在先。因此下面的三篇选题，可以作为本期 DOJO 的素材被收录。分别是科学俱乐部，网络科学俱乐部，还有开源产业在英国。STEM一直都是以产业为先导，在英国展开的，更像是职业教育的前导。STEM大使制度就是为此设计的。而少儿编程，则是数字鸿沟弥合的项目，催生乐树莓派的发展，如今的工业级别应用已经让树莓派不再是树莓派，而后来者也从来没有真正贯彻树莓派的教育原点，石榴派会吗？太多诱惑在影响着我们。我们需要反省。

- [STEM: Paul's ZOOM Science Club (PZSC)](https://gitee.com/yuandj/siger/issues/I3CNQZ)

  > 从去年四月开始，我一直为KS2学生(4、5、6年级)运行一个基于ZOOM的科学俱乐部。每周我们做15到45分钟的实践活动。去年11月，我参加了一个网络研讨会(请参阅什么是STEM俱乐部以及我如何在大流行期间管理这个俱乐部)。在www.stem.org.uk STEM-club-webinars从大约20分钟开始)，在那里我承诺与社区分享我制作的活动。

  > 如果你是一名中学老师，你可能仍然对我开发的一些材料感兴趣，但有时可能需要调整或不需要。我使用KS3的经验是有限的!

  > 在谷歌的驱动器上还有俱乐部背后的概念和它是如何运行的全部细节。

  > 不幸的是，我一次只能接待十几个学生，但我现在有一两个名额。如果您想让您的小学学生参加，请使用上面的电子邮件联系我，我们可以交谈。

  > Paul Treble <img src="https://images.gitee.com/uploads/images/2021/0323/153937_b9e61591_5631341.png" height="19px">  
  > STEM Ambassador  
  > Cheltenham

    - STEM [Club网络研讨会](https://www.stem.org.uk/STEM-club-webinars) <img src="https://images.gitee.com/uploads/images/2021/0307/222051_672b6890_5631341.png" height="19px">  
      https://gitee.com/blesschess/shiliupi/wikis/?sort_id=3666296

- [科学教育协会：一个专业社区，致力于支持卓越的科学教学。](https://gitee.com/yuandj/siger/issues/I3C88B)

  <p><img src="https://images.gitee.com/uploads/images/2021/0319/195350_2d2f70b0_5631341.png" height="69px">  《小学科学》第 166 期， <b>提高知名度</b> </p>

  > 本期的重点是“提高科学的知名度”，在2020年底宣布取消对教师的评估的要求之后，这似乎非常及时，小学的科学地位再次受到了威胁。文章说明了如何在学校中提高科学素养并继续分享优秀实践。

  > 你好！我是小学科学专家Jo Montgomery。我是一名合格的教师和研究科学家，拥有二十多年的经验，在学校提供有趣且动手的科学研讨会，并支持教师的专业发展。

  > 我是ASE和CLEAPSS的成员，ASE和CLEAPSS是STEM学习CPD质量标志的认可协理者，PSQM中心负责人，Great Science Shares for Schools区域冠军和Explorify冠军。

    - [运营STEM俱乐部的方式，原因，方式，地点和时间](https://www.ase.org.uk/resources/primary-science/issue-166/what-why-how-where-and-when-of-running-stem-clubs)  

      > _—— 乔·蒙哥马利（Jo Montgomery）_ 第166期 第11页 2021年2月发布

  关于我们  **科学教育协会（ASE）** 

  > _富有启发性和知识渊博的教师和技术人员发展了青年人对科学的兴趣。从地方活动到国家政策，我们都是一个专业社区，致力于支持卓越的科学教学。_ 

  > _科学教育协会（ASE）是一个活跃的会员组织，为从学龄前到高等教育的所有科学教育参与者提供了超过100年的支持；成员包括教师，技术员，导师和顾问。我们是一家拥有皇家宪章的注册慈善机构，由我们的会员拥有并且独立于政府。我们力求为科学教育专业人士树立强有力的声音，以期对整个英国乃至更远的地方的科学教学产生积极而有影响的影响。_ 

- [报告：英国开源欧洲领先，贡献经济增长达 430 亿英镑](https://gitee.com/yuandj/siger/issues/I3BIE8)

  - **[OpenUK Report](https://gitee.com/yuandj/siger/issues/I3BIE8#note_7831663_link)** Download

    - **[OpenUK Report Phase Three](https://openuk.uk/wp-content/uploads/2021/10/openuk-state-of-open_final-version.pdf)** ：[State of Open: The UK in 2021](https://openuk.uk/stateofopen/)

    - **[OpenUK Report Phase Two](https://openuk.uk/wp-content/uploads/2021/07/State-of-Open-Phase-Two.pdf)** ：97% of UK companies surveyed use open source and 89% are running open source software.

    - **[OpenUK Report Phase One](https://openuk.uk/wp-content/uploads/2021/03/openuk_stateofope2021_report_FINALCHANGES_08.pdf)** ：UK is Europe’s number one contributor to open source software and open source contributes up to £43.1bn pa to GDP

    <p><img src="https://images.gitee.com/uploads/images/2021/1213/224328_b81e4aee_5631341.jpeg" height="239px" title="Phase-3-Cover.jpg"> <img src="https://images.gitee.com/uploads/images/2021/1213/224342_75c7beba_5631341.png" height="239px" title="State-of-Open-Phase-Two-Front-Cover-Image-e1625661054588.png"> <img src="https://images.gitee.com/uploads/images/2021/1213/224413_d223ec97_5631341.jpeg" height="239px" title="State-of-Open-Phase-One-Cover-e1625659750159.jpg""></p>

  - 【[OSCHINA周报](https://www.oschina.net/news/133028/openuk-state-of-open?from=20210314)】如题 2021-03-14 全文转载 (编辑: 白开水不加糖)

  > 开源非营利组织 OpenUK 的于近日发布了其三阶段报告的第一部分，评估了英国开源的现状以及英国在全球开源领域的地位。其主要发现为：英国是开源软件的卓越中心；开源软件为英国经济贡献了数百亿英镑；以及英国是世界上开源软件的最大贡献者之一。

  > 报告指出，开源技术为英国贡献了高达 430 亿英镑（602.2 亿美元）的经济增长，这表明英国在开源开发方面领先于欧洲。并表示，英国仍是开源技术的领导者，其国内预计有 12.6 万名贡献者参与了创建、开发和维护开源的工作；这一数字将近欧盟 26 万名开源开发者中的一半。

  > OpenUK 的估算基于欧盟的[开源影响研究](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/first-results-study-impact-open-source)，该研究发表于 2 月份的 2021 年欧盟开源政策峰会。欧盟的报告指出，开源为欧洲经济贡献了 609 亿英镑至 841.5 亿英镑的增长。使用相同的计算模型，OpenUK 则认为开源为英国带来了 295 亿英镑至 432 亿英镑的经济增长。

  > OpenUK 报告的第二部分将包括对企业的调查，以了解开源在英国产业中的地位，计划于今年 6 月发布；第三部分则旨在提供开源对英国数字经济的总价值，计划于 9 月发布。OpenUK 希望该报告将使商业、工业和公共部门更好地了解英国开源的规模和采用情况，并围绕它计划新的数字计划。

  > OpenUK 首席执行官 Amanda Brock 表示，该报告展示了英国开源价值的规模，称其为“揭示了英国在大多数欧洲国家中的领先程度，也揭示了其作为全球顶级玩家之一的地位。”

  > [数据表明](https://www.techrepublic.com/article/is-the-uk-becoming-the-open-source-capital-of-europe/)，英国开源软件的使用量同比增长 28.6％，成为了美国以外的五大用户之一。2020 年，英国的贡献者在 GitHub 上修改了约 8070 万个开源文件，同比增加了 33.76%。

  > 英国贸易大臣 Francis Maude 称，英国在开源的未来中将扮演重要的领导角色。“通过支持参与开源的公司，并帮助他们发展和支持英国公共部门，我们可以实现强大的循环经济，并创建可以在全球范围内扩展的世界领先项目。” 

  > 完整报告地址：https://openuk.uk/wp-content/uploads/2021/03/StateOfOpen-TheUKin2021-PhaseOneReport-March2021.pdf 

  > 本站新闻禁止未经授权转载，违者依法追究相关法律责任。授权请联系：oscbianji#oschina.cn  
  > 本文标题：报告：英国开源欧洲领先，贡献经济增长达 430 亿英镑  
  > 本文地址：https://www.oschina.net/news/133028/openuk-state-of-open

  - 摘要信息

    > STEM UK 是 SIGer 重点报道的内容源。  
    > 其STEM大使计划是否和开源有关尚不可知。  
    > 但同样的文化土壤一定有可以借鉴和学习之处。

    - 循环经济
    - 全球视野

  - 相关引用

    - [发布](https://openuk.uk/stateofopen/)
    - [开源影响研究](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/first-results-study-impact-open-source)
    - [数据表明](https://www.techrepublic.com/article/is-the-uk-becoming-the-open-source-capital-of-europe/)

# ROCKET 彩蛋：[以DOJO为题，开源社群的中国实践，](https://gitee.com/yuandj/siger/issues/I3CST6)

DOJO 作为树莓派社区的延伸，是开源社区的重要成果，也是我们学习的榜样。  
但在本土化的过程中，一定有很多工作要做，社群建设也相当的不容易。  
好在一切都会实现的，慢慢来，志同道合的朋友都会遇到。

### 道友

- Qin Fengling is an Open Source Software Engineer working on embedded hardware systems and the Blockchain.
- I love FOSS and I am the software firmware architect for Canaan Creative Avalon Bitcoin Miner for the Avalon 3, 4, 6, and 7 series computers.
- I am a contributor to OpenWRT, CGMiner, and all Canaan Creative Avalon Open Source projects.
- I speak internationally about Open Source, Embedded Systems, and the Blockchain.
- For fun, I write code for FPGA, and other hardware projects.
- For now, I am contributing to the Proof of Existence project.

### 摘要

- 树莓派社区的两个姊妹
  1. DOJO
  2. CODE

- 开源硬件篇
  1. 树莓派
  2. PICO
  3. MICRO-BIT
  4. ARDUINO

### ROCKET 彩蛋 <img src="https://images.gitee.com/uploads/images/2021/0329/021607_203240bb_5631341.png" height="69px"> 

- [OpenWRT系统特点: 优劣势](https://gitee.com/yuandj/siger/issues/I3CST6#note_4611640_link) https://openwrt.org/ 为何使用OpenWrt?

  > OpenWRT是一个高度模块化、高度自动化的嵌入式Linux系统，拥有强大的网络组件和扩展性，常常被用于工控设备、电话、小型机器人、智能家居、路由器以及VOIP设备中。 同时，它还提供了100多个已编译好的软件，而且数量还在不断增加，而OpenWrt SDK 更简化了开发软件的工序。

- [研学的方向](https://gitee.com/yuandj/siger/issues/I3CST6#note_4643739_link) 有仨？

  学无止境，老师的每一次回复都为我展开又一个崭新的世界，我希望我的同学们和我一起，畅游在知识的海洋，求知若渴。今天发出邀请，也是给自己的作业。
  > No.12 青少年教育社区 DOJO  
  > No.13 区块链科普

  @qinfengling 这也会是同学们研学的方向啦。

  > 基于 FPGA 的 RISC-V 树莓派电脑，作为一个研究项目。就是第三期 SIGER RISCV 专题的题目  
  > 编号暂不申请，等待时机成熟。 :pray: 

  [基于 FPGA 的 RISC-V 树莓派电脑，作为一个研究项目。就是第三期 SIGER RISCV 专题的题目](https://gitee.com/yuandj/siger/issues/I3CST6#note_4678815_link)

  > [How to make RISC-V Microcomputer using FPGA for programmer](https://images.gitee.com/uploads/images/2021/0329/021607_203240bb_5631341.png)  
  > 2020年3月2日， 如何使用FPGA编写用于程序员的RISC-V微型计算机 平装本-2020年3月2日  
  > https://www.amazon.com/make-RISC-V-Microcomputer-using-programmer/dp/4802098294

- [based fpga risc-v raspberry pi micro computer](https://gitee.com/yuandj/siger/issues/I3CST6#note_4678818_link)

  - [PolarBerry is a Compact, Linux-capable RISC-V FPGA SBC ...www.cnx-software.com › 2020/10/30 › polar...](https://www.cnx-software.com/2020/10/30/polarberry-compact-linux-capable-risc-v-fpga-sbc-som/)  【[笔记](https://gitee.com/yuandj/siger/issues/I3CST6#note_4678818_link)】

    > 2020年10月30日 — Sundance PolarBerry is a RISC-V FPGA single board computer and system-on-​module with Gigabit Ethernet, 40-pin Raspberry Pi header. ... SiFive may just have announced a mini-ITX motherboard for RISC-V PCs, but if you'd like a RISC​-V ... Maybe not long until microchip comes with riscv based PICs? 1.

      <p><img src="https://images.gitee.com/uploads/images/2021/0329/023645_b998ad35_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2021/0329/024036_1ec3deb9_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2021/0329/024101_ba89992b_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2021/0329/024312_ff7c4104_5631341.png" height="99px"> 

（之所以称为彩蛋，是 [石榴派](http://shiliupi.cn) 的 [RISC-V](https://gitee.com/RV4Kids) 核心 回答了本节标题！:pray: 一逗号结尾是它才刚刚开始...）